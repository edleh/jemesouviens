/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    SCRIPT LOADING SYSTEM NEEDED
    BASED ON FILE PARAMETERS
    --------------------------------------------------------------
    SYSTÈME DE CHARGEMENT DES SCRIPTS NÉCESSAIRES
    EN FONCTION DES PARAMÈTRES PASSÉS AU FICHIER
    --------------------------------------------------------------
*/



let PARAMS = {};
let LANG = 'en';



function saveParams() {
    if (localStorage) {
        const json = JSON.stringify(PARAMS);
        localStorage.setItem('JMS-users', json);
        }
    }





function main() {

    function include(urls, callback) {
        /*
        http://sametmax.com/include-require-import-en-javascript
        */
        for (i in urls) {
            let url = urls[i];
            /* on crée une balise<script type="text/javascript"></script> */
            let script = document.createElement('script');
            script.type = 'text/javascript';
            /* On fait pointer la balise sur le script qu'on veut charger
            avec en prime un timestamp pour éviter les problèmes de cache
            */
            script.src = url + '?' + (new Date().getTime());
            /* On dit d'exécuter cette fonction une fois que le dernier script est chargé */
            if (i == urls.length - 1)
                if (callback) {
                    script.onreadystatechange = callback;
                    script.onload = script.onreadystatechange;
                    }
            /* On rajoute la balise script dans le head, ce qui démarre le téléchargement */
            document.getElementsByTagName('head')[0].appendChild(script);
            }
        };


    let INCLUDES = [];
    const LANGUAGES = ['en', 'fr', ];
    LANG = window.location.search.substring(1).split('=')[1];
    if (LANGUAGES.indexOf(LANG) < 0)
        LANG = LANGUAGES[0];
    //alert(LANG);
    INCLUDES.push('assets/translations/' + LANG + '.js');

    if (localStorage) {
        const json = localStorage.getItem('JMS-users');
        if (json != null)
            PARAMS = JSON.parse(json);
        }
    if (PARAMS.logins == undefined)
        PARAMS.logins = 0;
    if (PARAMS.date == undefined)
        PARAMS.date = Date.now();
    //console.log('logins: ' + PARAMS.logins + ', date: ' + PARAMS.date);

    include(
        INCLUDES, 
        function() {
            include(['assets/users.js']);}
        )
    }

main();
