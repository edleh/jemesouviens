
    <!-- Page content
    ================================================== -->
    <div id="container" class="container" what="list">



        <!-- LOGO ET TITRE -->
        <div class="row">
            <div class="col-sm-4">
                <img src="assets/logo.png" class="img-responsive">
            </div>
            <div class="col-sm-7">
                <h1 class="text-center" id="title">User management<br/><br/></h1>
            </div>
            <div class="col-sm-1"></div>
        </div>



        <!-- BOUTONS GÉNÉRAUX (NOUVEAU, ADMIN, QUITTER) -->
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 text-center"">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-lg" id="new" onclick="newUser()">
                        <span class="oi oi-plus" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default btn-lg" id="admin" onclick="editAdmin()">
                        <span class="oi oi-cog" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default btn-lg" id="logOut" onclick="logOut()">
                        <span class="oi oi-account-logout" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>



        <!-- ZONE D'ÉDITION -->
        <div class="row hide" id="edit-zone">
            <br />
            <div class="col-sm-2"></div>
            <div class="col-sm-8">

                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="input-group">
                            <span class="input-group-addon"><span class="oi oi-person"></span></span>
                            <input type="text" class="form-control" id="login" value="">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="oi oi-lock-locked"></span></span>
                            <input type="text" class="form-control" id="password">
                        </div>
                        <div class="input-group" id="subfolders-input">
                            <span class="input-group-addon"><span class="oi oi-folder"></span></span>
                            <input type="text" class="form-control" id="subfolders">
                        </div>
                        <div class="input-group"><p></p></div>
                        <div class="btn-group float-right" role="group">
                            <button type="button" class="btn btn-default btn-sm" id="cancel" onclick="cancelUser()">
                                <span class="oi oi-account-logout" aria-hidden="true"></span>
                            </button>
                        </div>
                        <div class="btn-group float-right" role="group">
                            <button type="button" class="btn btn-default btn-sm" id="save" onclick="saveUser()">
                                <span class="oi oi-data-transfer-download" aria-hidden="true"></span>
                            </button>
                        </div>
                        <div class="input-group">
                            <p id="comment"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>



        <!-- LA LISTE DES UTILISATEURS -->
        <div class="row">
        <br />
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr class="success">
                                <th id="userId">id</th>
                                <th id="user">USER</th>
                                <th id="folders">sub-folders</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
<?php
$table = '';
$users = listUsers();
foreach ($users as $user) {
    $table .= spaces(7).'<tr>'."\n";
    $table .= spaces(8).'<td id="id-'.$user[1].'">'.$user[1].'</td>'."\n";
    $table .= spaces(8).'<td id="user-'.$user[1].'">'.$user[0].'</td>'."\n";
    $table .= spaces(8).'<td id="subfolders-'.$user[1].'">'.$user[2].'</td>'."\n";
    $table .= spaces(8).'<td>'."\n";
    $table .= spaces(9).'<div class="btn-group float-right" role="group">'."\n";
    $table .= spaces(9).'<button type="button" class="btn btn-default btn-sm" id="edit" onclick="editUser('.$user[1].')">'."\n";
    $table .= spaces(10).'<span class="oi oi-pencil" aria-hidden="true"></span>'."\n";
    $table .= spaces(9).'</button>'."\n";
    $table .= spaces(9).'<button type="button" class="btn btn-default btn-sm" id="delete" onclick="deleteUser('.$user[1].')">'."\n";
    $table .= spaces(10).'<span class="oi oi-x" aria-hidden="true"></span>'."\n";
    $table .= spaces(9).'</button>'."\n";
    $table .= spaces(9).'</div>'."\n";
    $table .= spaces(8).'</td>'."\n";
    $table .= spaces(7).'</tr>'."\n";
    }
echo $table;
?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>



    </div><!--container-->

