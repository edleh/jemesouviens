
    <!-- Page content
    ================================================== -->
    <div id="container" class="container" what="login">


        <div class="row">
            <!-- LOGO -->
            <div class="col-sm-4">
                <img src="assets/logo.png" class="img-responsive">
            </div>
            <div class="col-sm-7">
                <!-- TITRE -->
                <h1 class="text-center" id="title">User management<br/><br/></h1>

                <!-- CONNEXION -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="login">
                            <form class="form-login">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="oi oi-person"></span></span>
                                    <label class="sr-only" for="login"></label>
                                    <input type="text" class="form-control" id="login" value="" placeholder="User Name">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="oi oi-lock-locked"></span></span>
                                    <label class="sr-only" for="password"></label>
                                    <input type="password" class="form-control" id="password" placeholder="Password">
                                </div>
                                <div class="input-group">
                                    <label></label>
                                </div>
                                <button 
                                    class="btn btn-primary btn-block" 
                                    type="button" 
                                    id="go" 
                                    data-loading-text="Connecting..." 
                                    onclick="connect()">
                                    Connect &raquo
                                </button>
                                <div class="input-group">
                                    <label></label>
                                </div>
                            </form>
                        </div>
                        <!-- POUR LES MESSAGES AFFICHÉS VIA JAVASCRIPT -->
                        <div class="message">
                            <div class="hide" id="alert_msg"></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-1"></div>
        </div>


    </div><!--container-->

