/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


const TRANSLATIONS = {
    'User management': '', 
    'User Name': '', 
    'Password': '', 
    'Connect': '', 
    'Connecting...': '', 
    'Enter a username.': '', 
    'Enter a password.': '', 
    'OK': '', 
    'The user name is incorrect.': '', 
    'The password is incorrect.': '', 
    'You have exceeded the number of allowed errors.': '', 
    'Your IP address has been recorded and is temporarily blocked.': '', 
    'Reconnect in 15 minutes.': '', 
    'The server returned an error!': '', 
    'The connection failed!': '', 


    'Log out': '', 
    'id': '', 
    'USER': '', 
    'sub-folders': '', 
    'Edit': '', 
    'Delete': '', 
    'Create a new user': '', 
    'Change the administrator account': '', 
    'Save': '', 
    'Cancel': '', 
    'Leave the password field blank if you do not want to change it.': '', 
    'Leave a field blank if you do not want to change it.': '', 
    'Delete this user?': '', 


    '': '', 
    };


function tr(text) {
    let result = TRANSLATIONS[text.trim()];
    if (result === undefined)
        result = text;
    else if (result == '')
        result = text;
    return result;
    }

