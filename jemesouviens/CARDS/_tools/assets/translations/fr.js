/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


const TRANSLATIONS = {
    'User management': 'Gestion des utilisateurs', 
    'User Name': "Nom d'utilisateur", 
    'Password': 'Mot de passe', 
    'Connect': 'Se connecter', 
    'Connecting...': 'Connexion...', 
    'Enter a username.': "Saisissez un nom d'utilisateur.", 
    'Enter a password.': 'Saisissez un mot de passe.', 
    'OK': 'OK', 
    'The user name is incorrect.': "Le nom d'utilisateur est incorrect.", 
    'The password is incorrect.': 'Le mot de passe est incorrect.', 
    'You have exceeded the number of allowed errors.': 
        "Vous avez dépassé le nombre d'erreurs autorisées.", 
    'Your IP address has been recorded and is temporarily blocked.': 
        'Votre adresse IP a été enregistrée et est bloquée temporairement.', 
    'Reconnect in 15 minutes.': 'Reconnectez-vous dans 15 minutes.', 
    'The server returned an error!': 'Le serveur a renvoyé une erreur !', 
    'The connection failed!': 'La connexion a échoué !', 


    'Log out': 'Se déconnecter', 
    'id': 'id', 
    'USER': 'UTILISATEUR', 
    'sub-folders': 'sous-dossiers', 
    'Edit': 'Modifier', 
    'Delete': 'Supprimer', 
    'Create a new user': 'Créer un nouvel utilisateur', 
    'Change the administrator account': 'Modifier le compte administrateur', 
    'Save': 'Enregistrer', 
    'Cancel': 'Annuler', 
    'Leave the password field blank if you do not want to change it.': 
        'Laissez le champ mot de passe vide si vous ne voulez pas le modifier.', 
    'Leave a field blank if you do not want to change it.': 
        'Laissez un champ vide si vous ne voulez pas le modifier.', 
    'Delete this user?': 'Supprimer cet utilisateur ?', 


    '': '', 
    };


function tr(text) {
    let result = TRANSLATIONS[text.trim()];
    if (result === undefined)
        result = text;
    else if (result == '')
        result = text;
    return result;
    }

