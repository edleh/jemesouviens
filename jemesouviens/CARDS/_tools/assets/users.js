/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/********************************************
 ********************************************
 ********************************************
    GESTION DES UTILISATEURS
 ********************************************
 ********************************************
 ********************************************/

let container, editZone, what, action;





function initInterface() {
    /*
    mise en place de l'interface.
    Essentiellement les traductions
    */
    container = document.getElementById('container');
    what = container.getAttribute('what');
    //alert(what + ' - ' + LANG);
    if (what == 'login') {
        PARAMS.logins += 1;
        document.getElementById('title').innerHTML = tr('User management') + '<br/><br/>';
        document.getElementById('login').setAttribute('placeholder', tr('User Name'));
        document.getElementById('password').setAttribute('placeholder', tr('Password'));
        document.getElementById('go').innerHTML = tr('Connect') + ' &raquo';
        document.getElementById('go').setAttribute('data-loading-text', tr('Connecting...'));
        }
    else if (what == 'list') {
        editZone = document.getElementById('edit-zone');
        document.getElementById('title').innerHTML = tr('User management') + '<br/><br/>';
        document.getElementById('logOut').setAttribute('title', tr('Log out'));
        document.getElementById('user').innerHTML = tr('USER');
        document.getElementById('userId').innerHTML = tr('id');
        document.getElementById('folders').innerHTML = tr('sub-folders');
        document.getElementById('edit').setAttribute('title', tr('Edit'));
        document.getElementById('delete').setAttribute('title', tr('Delete'));
        document.getElementById('new').setAttribute('title', tr('Create a new user'));
        document.getElementById('admin').setAttribute('title', tr('Change the administrator account'));
        document.getElementById('save').setAttribute('title', tr('Save'));
        document.getElementById('cancel').setAttribute('title', tr('Cancel'));
        }
    }





/********************************************
    PAGE LOGIN
********************************************/

function connect() {
    /*
    bouton de connexion (seule action de la page)
    */
    let login = document.getElementById('login').value;
    let password = document.getElementById('password').value;
    let alert_msg = document.getElementById('alert_msg');
    document.getElementById('go').innerHTML = tr('Connecting...');
    alert_msg.setAttribute('class', 'hide');
    if (login == '') {
        alert_msg.innerHTML = '<strong>' + tr('Enter a username.') + '</strong>';
        alert_msg.setAttribute('class', 'alert alert-danger text-center');
        }
    else if (password == '') {
        alert_msg.innerHTML = '<strong>' + tr('Enter a password.') + '</strong>';
        alert_msg.setAttribute('class', 'alert alert-danger text-center');
        }

    else if (PARAMS.logins > 4) {
        //console.log('logins: ' + PARAMS.logins + ', date: ' + PARAMS.date);
        if (PARAMS.logins == 5)
            PARAMS.date = Date.now();
        let d = Date.now() - PARAMS.date;
        //console.log(d);
        if (d > 1000 * 60 * 15)
            PARAMS.logins = 0;
        else
            PARAMS.logins += 1;
        saveParams();
        alert_msg.innerHTML = '<strong>'
            + tr('You have exceeded the number of allowed errors.') + '<br />'
            + tr('Reconnect in 15 minutes.') + '</strong>';
        alert_msg.setAttribute('class', 'alert alert-danger text-center');
        }

    else {
        let request = new XMLHttpRequest();
        request.open('POST', 'users.php', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                let response = request.responseText;
                //console.log('logins: ' + PARAMS.logins + ', date: ' + PARAMS.date);
                //console.log(response);
                if (response.indexOf('OK') >= 0) {
                    PARAMS.logins = 0;
                    saveParams();
                    alert_msg.innerHTML = '<strong>' + tr('OK') + '</strong>';
                    alert_msg.setAttribute('class', 'alert alert-success text-center');
                    window.location = 'users.php?lang=' + LANG;
                    }
                else if (response.indexOf('USER') >= 0) {
                    PARAMS.logins += 1;
                    saveParams();
                    alert_msg.innerHTML = '<strong>' + tr('The user name is incorrect.') + '</strong>';
                    alert_msg.setAttribute('class', 'alert alert-danger text-center');
                    }
                else if (response.indexOf('PASSWORD') >= 0) {
                    PARAMS.logins += 1;
                    saveParams();
                    alert_msg.innerHTML = '<strong>' + tr('The password is incorrect.') + '</strong>';
                    alert_msg.setAttribute('class', 'alert alert-danger text-center');
                    }
                else {
                    alert_msg.innerHTML = '<strong>' + tr('The server returned an error!') + '</strong>';
                    alert_msg.setAttribute('class', 'alert alert-danger text-center');
                    }
                }
            else {
                alert_msg.innerHTML = '<strong>' + tr('The server returned an error!') + '</strong>';
                alert_msg.setAttribute('class', 'alert alert-danger text-center');
                }
            };

        request.onerror = function() {
            alert_msg.innerHTML = '<strong>' + tr('The connection failed!') + '</strong>';
            alert_msg.setAttribute('class', 'alert alert-danger text-center');
            };

        request.send('action=connect&login=' + login + '&password=' + password);
        }
    document.getElementById('go').innerHTML = tr('Connect') + ' &raquo';
    }





/********************************************
    PAGE LIST
********************************************/

function logOut() {
    /*
    on retourne à la page login
    */
    let request = new XMLHttpRequest();
    request.open('POST', 'users.php', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = function() {
        if (request.status >= 200 && request.status < 400)
            window.location = 'users.php?lang=' + LANG;
        };
    request.send('action=logOut');
    }



function newUser() {
    /*
    demande de nouvel utilisateur.
    On met à jour l'action et on affiche la zone de saisie
    */
    action = 'new';
    document.getElementById('login').value = '';
    document.getElementById('password').value = '';
    document.getElementById('subfolders').value = '';
    document.getElementById('subfolders-input').setAttribute('class', 'input-group');
    document.getElementById('comment').innerHTML = '';
    editZone.setAttribute('class', 'row');
    }

function editAdmin() {
    /*
    modification du compte administrateur.
    On met à jour l'action et on affiche la zone de saisie.
    Pas de champ subfolders pour l'admin
    */
    action = 'admin';
    document.getElementById('login').value = '';
    document.getElementById('password').value = '';
    document.getElementById('subfolders-input').setAttribute('class', 'input-group hide');
    document.getElementById('comment').innerHTML = '';
    document.getElementById('comment').innerHTML = 
        '<small>' 
        + tr('Leave a field blank if you do not want to change it.') 
        + '</small>';
    editZone.setAttribute('class', 'row');
    }

function editUser(who) {
    /*
    modification d'un utilisateur existant.
    On met à jour l'action et on affiche la zone de saisie.
    Elle est pré-remplie (sauf le mot de passe)
    */
    action = who;
    //alert(who);
    let user = document.getElementById('user-' + who).innerHTML;
    let subfolders = document.getElementById('subfolders-' + who).innerHTML;
    document.getElementById('login').value = user;
    document.getElementById('password').value = '';
    document.getElementById('subfolders').value = subfolders;
    document.getElementById('subfolders-input').setAttribute('class', 'input-group');
    document.getElementById('comment').innerHTML = 
        '<small>' 
        + tr('Leave the password field blank if you do not want to change it.') 
        + '</small>';
    editZone.setAttribute('class', 'row');
    }

function cancelUser() {
    /*
    annulation demandée.
    Il suffit de masquer la zone de saisie
    */
    editZone.setAttribute('class', 'row hide');
    }



function saveUser() {
    /*
    enregistrement des modifications.
    On envoie les données à la page users.php
    et on referme la zone de saisie
    */
    let login = document.getElementById('login').value;
    let password = document.getElementById('password').value;
    let subfolders = document.getElementById('subfolders').value;
    //alert(action);

    let request = new XMLHttpRequest();
    request.open('POST', 'users.php', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            let response = request.responseText;
            //alert(response);
            if (response.indexOf('OK') >= 0) {
                // on recharge la page (mise à jour de la liste des utilisateurs) :
                window.location = 'users.php?lang=' + LANG;
                }
            }
        };
    request.send(
        'action=user&what=' + action 
        + '&login=' + login 
        + '&password=' + password 
        + '&subfolders=' + subfolders);

    editZone.setAttribute('class', 'row hide');
    }

function deleteUser(who) {
    /*
    suppression d'un utilisateur.
    ICI : On demande confirmation, 
    puis on passe le message à la page users.php
    */
    if (confirm(tr('Delete this user?')) == true) {
        //alert(who);
        let request = new XMLHttpRequest();
        request.open('POST', 'users.php', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                let response = request.responseText;
                //alert(response);
                if (response.indexOf('OK') >= 0) {
                    // on recharge la page (mise à jour de la liste des utilisateurs) :
                    window.location = 'users.php?lang=' + LANG;
                    }
                }
            };
        request.send('action=delete&who=' + who);
        }
    }















function init(event) {
    initInterface();
    }

window.addEventListener('load', init, false);

