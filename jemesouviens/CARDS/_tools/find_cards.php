<?php
/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


header("Content-Type: text/plain");
header("Access-Control-Allow-Origin: *");



function rsearch($folder, $pattern) {
    $dir = new RecursiveDirectoryIterator($folder);
    $iterator = new RecursiveIteratorIterator($dir);
    $filesList = new RegexIterator($iterator, $pattern, RegexIterator::GET_MATCH);
    $files = array();
    foreach($filesList as $file)
        $files = array_merge($files, $file);
    return $files;
    }



function listCards($ACTUAL_CARDS_DIR) {
    $CARDS_IN_DIR = array('CARDS' => array(), 'SUBDIRS' => array(), 'EXISTS' => true);
    if (! file_exists($ACTUAL_CARDS_DIR)) {
        $CARDS_IN_DIR['EXISTS'] = false;
        $ACTUAL_CARDS_DIR = '.';
        }
    $files = rsearch($ACTUAL_CARDS_DIR, '/^.+\-r.*$/i');
    foreach($files as $file) {
        //echo $file."\n";
        if ($ACTUAL_CARDS_DIR == '.')
            $file = substr($file, 2);
        $question = str_replace('-r.', '-q.', $file);
        if (file_exists($question)) {
            if ($ACTUAL_CARDS_DIR != '.')
                $question = str_replace($ACTUAL_CARDS_DIR, '', $question);
            $temp = explode('-q.', $question);
            $ext = '.'.$temp[1];
            $temp = explode('/', $temp[0]);
            if ($ACTUAL_CARDS_DIR == '.')
                $filePath = '';
            else
                $filePath = '/'.$ACTUAL_CARDS_DIR;
            for ($i=0; $i < count($temp) - 1; $i++)
                if ($temp[$i] != '')
                    $filePath = $filePath.'/'.$temp[$i];
            $fileName = $temp[count($temp) - 1];
            $filePath = str_replace('/../.', '', $filePath);
            
            if (stripos($filePath, '/_tools') === FALSE) {
                $question = array($filePath, $fileName, $ext);
                $CARDS_IN_DIR['CARDS'][] = $question;
                
                $temp = str_replace($ACTUAL_CARDS_DIR, '', $filePath);
                $temp = str_replace('//', '/', $temp);
                //echo $temp."\n";
                $temp = explode('/', $temp);
                if (count($temp) > 1) {
                    $temp = $temp[1];
                    if ($temp != '_tools')
                        if (!in_array($temp, $CARDS_IN_DIR['SUBDIRS']))
                            $CARDS_IN_DIR['SUBDIRS'][] = $temp;
                    }
                }
            }
        }
    sort($CARDS_IN_DIR['SUBDIRS'], SORT_NATURAL);
    return $CARDS_IN_DIR;
    }



$path = '.';
if (! empty($_GET)) {
    $path = ($_GET['path']) ? $_GET['path'] : '.';
    if ($path == '')
        $path = '.';
    }
$cards = listCards('../'.$path);
//print_r($cards);



echo json_encode($cards);

?>

