<?php
/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


header("Content-Type: text/plain");
header("Access-Control-Allow-Origin: *");



function rsearch($folder) {
    $dir = new RecursiveDirectoryIterator($folder);
    $iterator = new RecursiveIteratorIterator($dir);
    $files = array();
    foreach ($iterator as $info)
        $files[] = $info->getPathname();
    return $files;
    }   



function listFiles($ACTUAL_FILES_DIR) {
    $FILES_IN_DIR = array('FILES' => array());
    if (! file_exists($ACTUAL_FILES_DIR))
        $ACTUAL_FILES_DIR = '.';
    $files = rsearch($ACTUAL_FILES_DIR);
    foreach($files as $file) {
        if (! is_dir($file)) {
            //echo $file."\n";
            if ($ACTUAL_FILES_DIR == '.') {
                $file = substr($file, 2);
                $filePath = '';
                }
            else {
                $fileName = str_replace($ACTUAL_FILES_DIR, '', $file);
                $filePath = '/'.$ACTUAL_FILES_DIR;
                }
            $fileSize = filesize($file);
            //$fileDate = date('YmdHis', filemtime($file));
            $fileDate = date('YmdHi', filemtime($file));
            $filePath = str_replace('/..', '', $filePath);
            $FILES_IN_DIR['FILES'][] = array($filePath, $fileName, $fileSize, $fileDate);
            }
        }
    return $FILES_IN_DIR;
    }



//date_default_timezone_set('Europe/Paris');
date_default_timezone_set('UTC');
if (! empty($_POST)) {
    $path = ($_POST['path']) ? $_POST['path'] : '.';
    if ($path != '')
        $files = listFiles('../'.$path);
    }
else {
    $path = '.';
    $files = listFiles('../'.$path);
    }



$filesJsonFile = fopen('files.json', 'w');
fwrite($filesJsonFile, json_encode($files));
fclose($filesJsonFile);

//echo $path;

?>

