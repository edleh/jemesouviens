<?php
/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


header('Content-type: text/html; charset=UTF-8');



$userId = -1;
$user = '';
$password = '';
$subDirs = '';



if (! empty($_POST)) {
    $user = ($_POST['user']) ? $_POST['user'] : ''; 
    $password = ($_POST['password']) ? $_POST['password'] : '';
    }

if ($user != '') {
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/database.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

    $SQL = 'SELECT * FROM users ';
    $SQL .= 'WHERE user= :user';
    $OPT = array(':user' => $user);
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $query = ($STMT != '') ? $STMT -> fetch() : array('userId' => -1);
    if ($query['userId'] > -1) {
        $userId = $query['userId'];
        if (hash('sha256', $password) != $query['password'])
            $userId = -2;
        else
            $subDirs = $query['subDirs'];
        }
    }



$result = '||'.$userId.'||'.$subDirs.'||';
echo $result;

?>

