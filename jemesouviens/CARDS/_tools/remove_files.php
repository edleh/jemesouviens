<?php
/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


header("Content-Type: text/plain");
header("Access-Control-Allow-Origin: *");



$result = '';
if (! empty($_POST)) {
    // on commence par vérifier l'utilisateur :
    $user = ($_POST['user']) ? $_POST['user'] : ''; 
    $password = ($_POST['password']) ? $_POST['password'] : '';
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/database.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT * FROM users ';
    $SQL .= 'WHERE user= :user ';
    $SQL .= 'AND password= :password ';
    $OPT = array(':user' => $user, ':password' => hash('sha256', $password));
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $query = ($STMT != '') ? $STMT -> fetch() : array('userId' => -1);
    if ($query['userId'] > -1) {
        // on y va :
        $what = ($_POST['what']) ? $_POST['what'] : '.';
        $files = explode('#####', $what);
        foreach ($files as $fileName)
            if ($fileName != '')
                unlink('../'.$fileName);
        $result = 'OK';
        }
    }



echo $result;

?>

