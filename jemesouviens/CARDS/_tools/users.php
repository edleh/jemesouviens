<?php
/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/****************************************************
    FONCTIONS UTILES
****************************************************/

function spaces($n=0) {
    /*
    pour la mise en forme des pages calculées en php.
    Permet d'indenter la page html
    */
    $result = '';
    for ($i=0; $i<$n; $i++)
        $result = $result.'    ';
    return $result;
    }


function debugToConsole($data) {
    error_log(print_r($data, True));
    }



function createSession($why='') {
    /*
    Création d'une nouvelle session.
    La variable $why indique la raison de l'appel à la fonction
    (LOGIN, FIRST_TIME, INACTIVE_TIME, ).
    */
    if ($why == 'FIRST_TIME')
        session_destroy();
    session_name('JMS-users');
    session_start();
    $_SESSION = array();
    $_SESSION['PAGE'] = 'login';
    }

function updateSession() {
    /*
    Mise à jour de l'état de la session.
    Fonction appelée systématiquement depuis index.php.
    */
    session_name('JMS-users');
    session_start();
    if (! $_SESSION)
        createSession('FIRST_TIME');
    }



function connect($login, $password) {
    /*
    test de connexion de l'administrateur.
    ÉTATS DE SORTIE :
        9 : valeur initiale
        8 : trop de tests avec erreur (on endort 15 min)
        5 : login incorrect
        3 : password incorrect
        0 : OK (connexion réussie)
    */
    $result = 9;
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/database.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT * FROM admin ';
    $SQL .= 'WHERE user= :user';
    $OPT = array(':user' => hash('sha256', $login));
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $query = ($STMT != '') ? $STMT -> fetch() : array('user' => '');
    if ($query['user'] !== '') {
        if ($query['user'] != hash('sha256', $login))
            return 5;
        elseif ($query['password'] != hash('sha256', $password))
            return 3;
        else
            return 0;
        }
    else
        return 5;
    // au cas où :
    return $result;
    }

function listUsers() {
    /*
    retourne la liste des utilisateurs.
    Pour affichage dans la page list.php
    */
    $result = array();
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/database.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT * FROM users';
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $query = ($STMT != '') ? $STMT -> fetchAll() : array();
    //$query = ($STMT != '') ? $STMT -> fetch() : array();
    foreach ($query as $ROW)
        $result[] = array($ROW['user'], $ROW['userId'], $ROW['subDirs']);
    return $result;
    }

function updateAdmin($login, $password) {
    /*
    mise à jour de la connexion de l'administrateur.
    La table admin ne contient qu'un seul enregistrement
    */
    $result = '';
    if (($login == '') && ($password == ''))
        return $result;
    $encodedLogin = hash('sha256', $login);
    $encodedPassword = hash('sha256', $password);

    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/database.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

    try {
        $pdo -> beginTransaction();
        if ($login != '') {
            $SQL = 'UPDATE admin SET user="'.$encodedLogin.'"';
            //debugToConsole($SQL);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute();
            }
        if ($password != '') {
            $SQL = 'UPDATE admin SET password="'.$encodedPassword.'"';
            //debugToConsole($SQL);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute();
            }
        $pdo -> commit();
        $result = 'OK';
        }
    catch (PDOException $e) {
        $pdo -> rollBack();
        }
    return $result;
    }

function createUser($login, $password, $subfolders) {
    /*
    création d'un nouvel utilisateur.
    On cherche un userId puis on insère dans la table
    */
    $result = '';
    // on commence par chercher son userId :
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/database.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT * FROM users ';
    $SQL .= 'ORDER BY userId DESC';
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $query = ($STMT != '') ? $STMT -> fetchAll() : array();
    $userId = $query[0]['userId'] + 1;
    if ($userId < 0)
        $userId = 0;
    // on y va :
    $SQL = 'INSERT INTO users ';
    $SQL.= 'VALUES(:userId, :user, :password, :subDirs)';
    $OPT = array(
        ':userId' => $userId, 
        ':user' => $login, 
        ':password' => hash('sha256', $password), 
        ':subDirs' => $subfolders);
    $STMT = $pdo -> prepare($SQL);
    try {
        $pdo -> beginTransaction();
        $STMT -> execute($OPT);
        $pdo -> commit();
        $result = 'OK';
        }
    catch (PDOException $e) {
        $pdo -> rollBack();
        }
    return $result;
    }

function updateUser($what, $login, $password, $subfolders) {
    /*
    mise à jour d'un utilisateur existant
    */
    $result = '';
    if (($login == '') && ($password == '') && ($subfolders == ''))
        return $result;
    $encodedPassword = hash('sha256', $password);

    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/database.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

    try {
        $pdo -> beginTransaction();
        if ($login != '') {
            $SQL = 'UPDATE users SET user="'.$login.'" WHERE userId='.$what;
            //debugToConsole($SQL);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute();
            }
        if ($password != '') {
            $SQL = 'UPDATE users SET password="'.$encodedPassword.'" WHERE userId='.$what;
            //debugToConsole($SQL);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute();
            }
        if ($subfolders != '') {
            $SQL = 'UPDATE users SET subDirs="'.$subfolders.'" WHERE userId='.$what;
            //debugToConsole($SQL);
            $STMT = $pdo -> prepare($SQL);
            $STMT -> execute();
            }
        $pdo -> commit();
        $result = 'OK';
        }
    catch (PDOException $e) {
        $pdo -> rollBack();
        }
    return $result;
    }

function deleteUser($who) {
    /*
    on supprime un utilisateur de la table
    */
    $result = '';
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/database.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'DELETE FROM users ';
    $SQL .= 'WHERE userId=:userId';
    $OPT = array(':userId' => $who);
    $STMT = $pdo -> prepare($SQL);
    try {
        $pdo -> beginTransaction();
        $STMT -> execute($OPT);
        $pdo -> commit();
        $result = 'OK';
        }
    catch (PDOException $e) {
        $pdo -> rollBack();
        }
    return $result;
    }





/****************************************************
    INITIALISATION
****************************************************/
updateSession();





/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($_SESSION['PAGE'] == 'login') {
    $ACTION = (isset($_POST['action'])) ? $_POST['action'] : '';
    $login = isset($_POST['login']) ? $_POST['login'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] :  '';
    if ($ACTION == 'connect') {
        $message = '';
        $test = 3;
        switch(connect($login, $password)):
            case 0:
                $message = 'OK';
                $_SESSION['PAGE'] = 'list';
                break;
            case 3:
                $message = 'PASSWORD';
                break;
            case 5:
                $message = 'USER';
                break;
            case 8:
                $message = 'IP';
                break;
        endswitch;
        exit($message);
        }
    }
else if ($_SESSION['PAGE'] == 'list') {
    $ACTION = (isset($_POST['action'])) ? $_POST['action'] : '';
    if ($ACTION == 'logOut') {
        $_SESSION['PAGE'] = 'login';
        exit();
        }
    elseif ($ACTION == 'user') {
        $message = 'OK';
        $what = isset($_POST['what']) ? $_POST['what'] : '';
        $login = isset($_POST['login']) ? $_POST['login'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] :  '';
        $subfolders = isset($_POST['subfolders']) ? $_POST['subfolders'] : '';
        if ($what == 'admin')
            updateAdmin($login, $password);
        elseif ($what == 'new')
            createUser($login, $password, $subfolders);
        elseif ($what == '')
            $message = '';
        else
            updateUser($what, $login, $password, $subfolders);
        exit($message);
        }
    elseif ($ACTION == 'delete') {
        $message = 'OK';
        $who = isset($_POST['who']) ? $_POST['who'] : -1;
        deleteUser($who);
        exit($message);
        }
    }

//print_r($_SESSION);





/****************************************************
    CALCUL DE LA PAGE
****************************************************/
include('assets/header.php');

if ($_SESSION['PAGE'] == 'list')
    include('assets/list.php');
else
    include('assets/login.php');

include('assets/footer.php');

?>

