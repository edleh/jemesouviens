# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Contient l'interface graphique.
    La plupart des actions renvoient à des fonctions qui sont dans les modules :
        prof, admin et les utils_aaa
"""

# modules utiles :
import json

# modules perso :
import utils, utils_functions, utils_webengine, utils_filesdirs
import utils_softs, utils_pdf, utils_markdown, utils_web

# modules PyQt :
from PyQt5 import QtCore, QtWidgets, QtGui





class MainWindow(QtWidgets.QMainWindow):
    """
    LA FENÊTRE PRINCIPALE
    """
    def __init__(self, locale, translator, parent=None):
        """
        mise en place de l'interface
        plus des variables utiles
        """
        super(MainWindow, self).__init__(parent)
        self.setWindowTitle(utils.PROGNAME)

        # i18n :
        self.translator = translator
        self.locale = locale

        # initialisation :
        self.initSettings()

        # mise en place de l'interface :
        utils.loadSupportedImageFormats()
        self.createInterface()

        # redimensionnement de la fenêtre :
        rect = QtWidgets.QApplication.desktop().availableGeometry()
        w, h = int(rect.width()), int(rect.height())
        self.resize(int(0.75 * w), int(0.9 * h))

    def initSettings(self):
        """
        blablabla
        """
        message = utils_softs.testSofts()
        print(message)
        # le dossier des fichiers etc :
        self.cardsDir = '{0}/../CARDS'.format(utils.HERE)
        self.absoluteCardsDir = QtCore.QFileInfo(
            self.cardsDir).absoluteFilePath()
        # (re)création du dossier perso :
        myCards = QtWidgets.QApplication.translate(
            'main', 
            'MyCards')
        self.myCardsDir = '{0}/{1}'.format(
            self.cardsDir, myCards)
        if not(QtCore.QDir(self.myCardsDir).exists()):
            QtCore.QDir(self.cardsDir).mkdir(myCards)
        self.absoluteMyCardsDir = QtCore.QFileInfo(
            self.myCardsDir).absoluteFilePath()

        # (re)création du dossier temporaire :
        self.tempPath = utils_filesdirs.createTempAppDir(utils.PROGNAME + '-temp')

        # (re)création du dossier config dans le home :
        self.configDict = {}
        self.configDir, first = utils_filesdirs.createConfigAppDir(utils.PROGNAME)
        # on commence par lire le fichier de config :
        configFile = '{0}/config.json'.format(
            self.configDir.canonicalPath())
        try:
            if QtCore.QFile(configFile).exists():
                theFile = open(configFile, newline='', encoding='utf-8')
                self.configDict = json.load(theFile)
                theFile.close()
        except:
            self.configDict = {}
        # récupération ou initialisation des valeurs :
        for key in utils.DEFAULTCONFIG:
            if not(key in self.configDict):
                self.configDict[key] = utils.DEFAULTCONFIG[key]

        # on lit le fichier cards.js (pour tester les modifications) :
        self.readCardsJs()

    def closeEvent(self, event):
        self.closing = True
        # on écrit le fichier de config :
        configFile = '{0}/config.json'.format(
            self.configDir.canonicalPath())
        theFile = open(configFile, 'w', encoding='utf-8')
        json.dump(self.configDict, theFile, indent=4)
        theFile.close()
        # suppression du dossier temporaire :
        utils_filesdirs.emptyDir(self.tempPath)
        tempDir = QtCore.QDir.temp()
        tempDir.rmdir(utils.PROGNAME)



    ###########################################
    # Actions :
    ###########################################

    def testGame(self):
        """
        blablabla
        """
        self.createCardsJs()
        outFileName = '{0}/../jmsweb/index.html'.format(utils.HERE)
        url = QtCore.QUrl().fromLocalFile(outFileName)
        self.gameWebView.load(url)
        self.stackedWidget.setCurrentIndex(
            self.containers['game'])
        self.gameWebView.setFocus()

    def openCardsDir(self):
        """
        blablabla
        """
        utils_filesdirs.openDir(self.cardsDir)

    def pdf2Images(self):
        """
        blablabla
        """
        self.stackedWidget.setCurrentIndex(
            self.containers['pdf'])

    def markdownEdit(self):
        """
        blablabla
        """
        self.stackedWidget.setCurrentIndex(
            self.containers['markdown'])

    def printCards(self):
        """
        On fabrique un fichier PDF des cartes contenues dans un dossier.
        Sur chaque couple de 2 pages il y a 9 cartes (3 rangées de 3 cartes)
            * page impaire : les questions
            * page paire : les réponses.
        Pour chaque rangée les réponses sont dans l'ordre inverse des questions
        afin de se retrouver bien placées derrière.
        Les étapes :
            1. sélection du dossier
            2. recherche récursive des cartes et 
                remplissage du contenu html (htmlContent)
            3. création du fichier html d'après le modèle (print-cards.html)
            4. impression en PDF
        
        """

        # des modèles pour fabriquer le html :
        questionTemplate = (
            '<td class="question">\n'
            '    <img src="{0}/files/md/JMS.png"><br /><br /><br /><br />\n'
            '    <img src="{1}-q{2}">\n'
            '</td>')
        responseTemplate = (
            '<td class="response">\n'
            '    <img src="{0}-r{1}">\n'
            '</td>')
        rowTemplate = (
            '<tr>\n'
            '{0}\n{1}\n{2}\n'
            '</tr>')
        pageTemplate = (
            '<table>\n'
            '{0}\n{1}\n{2}\n'
            '</table>\n'
            '<hr />\n'
            '<table>\n'
            '{3}\n{4}\n{5}\n'
            '</table>')

        # sélection du dossier à traiter :
        title = QtWidgets.QApplication.translate(
            'main', 
            'Choose the folder containing the cards to be printed')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            title,
            self.cardsDir,
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return

        # sélection du fichier PDF à créer :
        proposedName = ''#self.markdownActualFile['fileName']
        if len(proposedName) < 1:
            proposedName = QtWidgets.QApplication.translate(
                'main', 'No name')
            proposedName = '{0}/{1}.pdf'.format(
                self.myCardsDir, proposedName)
        pdfFileName = QtWidgets.QFileDialog.getSaveFileName(
            self, 
            QtWidgets.QApplication.translate('main', 'Save as...'), 
            proposedName, 
            QtWidgets.QApplication.translate('main', 'PDF Files (*.pdf)'))
        if isinstance(pdfFileName, tuple):
            pdfFileName = pdfFileName[0]
        if len(pdfFileName) < 1:
            return

        utils_functions.doWaitCursor()
        try:

            # Récupération de la liste des cartes :
            localCardsList = []
            dirIterator = QtCore.QDirIterator(
                directory, 
                QtCore.QDir.Files, 
                QtCore.QDirIterator.Subdirectories)
            while dirIterator.hasNext():
                fileName = dirIterator.next()
                fileInfo = QtCore.QFileInfo(fileName)
                absolutePath = fileInfo.absolutePath()
                baseName = fileInfo.baseName()
                extension = fileInfo.suffix()
                if baseName[-2:] == '-q':
                    response = '{0}/{1}-r.{2}'.format(
                        absolutePath, baseName[:-2], extension)
                    if QtCore.QFileInfo(response).exists():
                        cardText = '{0}/{1} ||| {2}'.format(
                            absolutePath,
                            baseName[:-2], 
                            '.{0}'.format(extension))
                        localCardsList.append(cardText)
            localCardsList.sort()

            # fabrication du contenu html :
            htmlContent = ''
            nbCards = len(localCardsList)
            nbPages = nbCards // 9
            reste = nbCards % 9
            # on crée les couples de pages complètes :
            nbPage = -1
            for nbPage in range(nbPages):
                pageContent = ''
                questionRowContent = ['', '', '']
                responseRowContent = ['', '', '']
                for row in range(3):
                    questionContent = ['', '', '']
                    responseContent = ['', '', '']
                    for column in range(3):
                        index = nbPage * 9 + row * 3 + column
                        cardText = localCardsList[index]
                        fileData = cardText.split(' ||| ')
                        questionContent[column] = questionTemplate.format(
                            utils.HERE, fileData[0], fileData[1])
                        responseContent[column] = responseTemplate.format(
                            fileData[0], fileData[1])
                    questionRowContent[row] = rowTemplate.format(
                        questionContent[0], questionContent[1], questionContent[2])
                    responseRowContent[row] = rowTemplate.format(
                        responseContent[2], responseContent[1], responseContent[0])
                pageContent = pageTemplate.format(
                    questionRowContent[0], questionRowContent[1], questionRowContent[2], 
                    responseRowContent[0], responseRowContent[1], responseRowContent[2])
                if htmlContent == '':
                    htmlContent = '{0}\n{1}\n'.format(
                        htmlContent, pageContent)
                else:
                    htmlContent = '{0}\n<hr />\n{1}\n'.format(
                        htmlContent, pageContent)
            # s'il reste des cartes, il faut encore un couple de pages :
            if reste > 0:
                nbPage += 1
                pageContent = ''
                questionRowContent = ['', '', '']
                responseRowContent = ['', '', '']
                for row in range(3):
                    questionContent = ['', '', '']
                    responseContent = ['', '', '']
                    for column in range(3):
                        index = nbPage * 9 + row * 3 + column
                        if index < nbCards:
                            cardText = localCardsList[index]
                            fileData = cardText.split(' ||| ')
                            questionContent[column] = questionTemplate.format(
                                utils.HERE, fileData[0], fileData[1])
                            responseContent[column] = responseTemplate.format(
                                fileData[0], fileData[1])
                        else:
                            # on insère une carte vide :
                            questionContent[column] = '<td class="vide"></td>'
                            responseContent[column] = '<td class="vide"></td>'
                    questionRowContent[row] = rowTemplate.format(
                        questionContent[0], questionContent[1], questionContent[2])
                    responseRowContent[row] = rowTemplate.format(
                        responseContent[2], responseContent[1], responseContent[0])
                pageContent = pageTemplate.format(
                    questionRowContent[0], questionRowContent[1], questionRowContent[2], 
                    responseRowContent[0], responseRowContent[1], responseRowContent[2])
                if htmlContent == '':
                    htmlContent = '{0}\n{1}\n'.format(
                        htmlContent, pageContent)
                else:
                    htmlContent = '{0}\n<hr />\n{1}\n'.format(
                        htmlContent, pageContent)
            #print(htmlContent)

            # récupération du contenu du modèle html (print-cards.html) :
            htmlLines = ''
            template = '{0}/files/md/{1}.html'.format(utils.HERE, 'print-cards')
            htmlFileName = '{0}/md/{1}.html'.format(self.tempPath, 'print-cards')
            utils_filesdirs.removeAndCopy(template, htmlFileName)
            inFile = QtCore.QFile(htmlFileName)
            if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(inFile)
                stream.setCodec('UTF-8')
                htmlLines = stream.readAll()
                inFile.close()
            # on met en forme et on remplace le repère du modèle :
            #htmlContent = htmlContent.replace('\n', '\\n').replace("'", "\\'")
            htmlLines = htmlLines.replace('%CONTENT%', htmlContent)

            # on enregistre le nouveau fichier html :
            outFile = QtCore.QFile(htmlFileName)
            if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(outFile)
                stream.setCodec('UTF-8')
                stream << htmlLines
                outFile.close()

            # on imprime en pdf (dans temp) :
            tempPdfFileName = '{0}.pdf'.format(htmlFileName)
            utils_pdf.htmlToPdf(
                self, htmlFileName, tempPdfFileName, orientation='Portrait')
            # enfin on recopie le fichier pdf :
            utils_filesdirs.removeAndCopy(tempPdfFileName, pdfFileName)
            # et on l'ouvre :
            utils_filesdirs.openFile(pdfFileName)

        finally:
            utils_functions.restoreCursor()

    def uploadCards(self):
        """
        blablabla
        """
        self.createCardsJs()
        self.stackedWidget.setCurrentIndex(
            self.containers['upload'])

    def gestUsers(self):
        """
        blablabla
        """
        lang = self.locale.split('_')[0]
        theUrl = '{0}/CARDS/_tools/users.php?lang={1}'.format(
            self.configDict['CONFIG']['webUrl'], 
            lang)
        """
        theUrl = '{0}/users.php?lang={1}'.format(
            'http://127.0.0.1:8000', 
            lang)
        """
        self.usersWebView.load(QtCore.QUrl(theUrl))
        self.stackedWidget.setCurrentIndex(
            self.containers['users'])

    def doCreateLinuxDesktopFile(self):
        """
        crée un lanceur (fichier .desktop).
        """
        title = QtWidgets.QApplication.translate(
            'main', 
            'Choose the directory where the desktop file will be created')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            title,
            QtCore.QDir.homePath(),
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory != '':
            utils_functions.doWaitCursor()
            try:
                result = utils_filesdirs.createLinuxDesktopFile(
                    self, utils.HERE, directory, utils.PROGNAME, utils.PROGNAME)
            finally:
                utils_functions.restoreCursor()

    def doCreateWinShortcutFile(self):
        """
        crée un raccourci (fichier .lnk).
        """
        try:
            import pythoncom
        except ImportError:
            message = QtWidgets.QApplication.translate(
                'main', 'You must install pywin32 to do this action.')
            allMessage = (
                '<p align="center">__________________________</p>'
                '<p align="center"><b>{0}</b></p>'
                '<p></p>').format(message)
            QtWidgets.QMessageBox.warning(
                self, utils.PROGNAME, allMessage)
            return
        utils_functions.doWaitCursor()
        try:
            title = QtWidgets.QApplication.translate(
                'main', 'Shortcut to JeMeSouviens')
            utils_filesdirs.createWinShortcutFile(
                self, utils.PROGNAME, title)
        finally:
            utils_functions.restoreCursor()

    def readCardsJs(self):
        utils_functions.doWaitCursor()
        try:
            self.cards = {
                'CARDS': [], 
                'SUBDIRS': [], 
                'EXISTS': True, 
                }

            # on récupère le contenu du fichier cards.js :
            jsFileName = '{0}/_tools/cards.js'.format(self.cardsDir)
            jsFile = QtCore.QFile(jsFileName)
            if not(jsFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text)):
                return False
            stream = QtCore.QTextStream(jsFile)
            stream.setCodec('UTF-8')
            lines = stream.readAll()
            jsFile.close()
            # on vire le début et la fin :
            begin = 'var cards_database = '
            end = '};'
            lines = lines.replace(begin, '')
            lines = lines.replace(end, '}')

            # on enregistre le fichier json dans temp :
            jsonFileName = '{0}/cards.json'.format(self.tempPath)
            QtCore.QFile(jsonFileName).remove()
            jsonFile = QtCore.QFile(jsonFileName)
            if jsonFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(jsonFile)
                stream.setCodec('UTF-8')
                stream << lines
                jsonFile.close()

            # on peut enfin lire le fichier json :
            try:
                if QtCore.QFile(jsonFileName).exists():
                    jsonFile = open(jsonFileName, newline='', encoding='utf-8')
                    self.cards = json.load(jsonFile)
                    jsonFile.close()
            except:
                pass

        finally:
            utils_functions.restoreCursor()

    def createCardsJs(self):
        utils_functions.doWaitCursor()
        try:
            cards = {
                'CARDS': [], 
                'SUBDIRS': [], 
                'EXISTS': True, 
                }

            # récupération de la liste des fichiers :
            cardsDirLength = len(self.absoluteCardsDir)
            dirIterator = QtCore.QDirIterator(
                self.cardsDir, 
                QtCore.QDir.Files, 
                QtCore.QDirIterator.Subdirectories)
            while dirIterator.hasNext():
                fileName = dirIterator.next()
                absolutePath = QtCore.QFileInfo(fileName).absolutePath()
                if '_tools' in absolutePath:
                    continue
                baseName = QtCore.QFileInfo(fileName).baseName()
                extension = QtCore.QFileInfo(fileName).suffix()
                if baseName[-2:] == '-q':
                    response = '{0}/{1}-r.{2}'.format(
                        absolutePath, baseName[:-2], extension)
                    if QtCore.QFileInfo(response).exists():
                        cards['CARDS'].append([
                            absolutePath[cardsDirLength:], 
                            baseName[:-2], 
                            '.{0}'.format(extension)])
                        filePaths = absolutePath[cardsDirLength:].split('/')
                        if len(filePaths) > 1:
                            firstSubDir = filePaths[1]
                            if not(firstSubDir in cards['SUBDIRS']):
                                cards['SUBDIRS'].append(firstSubDir)
            cards['SUBDIRS'].sort()

            # on compare cards avec self.cards :
            mustDo = False
            if len(cards['SUBDIRS']) != len(self.cards['SUBDIRS']):
                mustDo = True
            elif len(cards['CARDS']) != len(self.cards['CARDS']):
                mustDo = True
            else:
                temp = [x for x in cards['SUBDIRS'] if x in self.cards['SUBDIRS']]
                if len(temp) != len(cards['SUBDIRS']):
                    mustDo = True
                else:
                    temp = [x for x in cards['CARDS'] if x in self.cards['CARDS']]
                    if len(temp) != len(cards['CARDS']):
                        mustDo = True

            if mustDo:
                self.cards['SUBDIRS'] = [x for x in cards['SUBDIRS']]
                self.cards['CARDS'] = [x for x in cards['CARDS']]
                jsFileName = '{0}/_tools/cards.js'.format(
                    self.absoluteCardsDir)
                jsonFile = open(jsFileName, 'w', encoding='utf-8')
                begin = 'var cards_database = '
                end = ';'
                jsonFile.write(begin)
                jsonFile.write(json.dumps(cards, indent=4))
                jsonFile.write(end)
                jsonFile.close()
        finally:
            utils_functions.restoreCursor()



    ###########################################
    # aide etc :
    ###########################################

    def about(self):
        """
        affiche la fenêtre À propos
        """
        import utils_about
        dialog = utils_about.AboutDlg(
            self, self.locale, icon='./images/logo.png')
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)

    def showHelp(self):
        """
        blablabla
        """
        self.stackedWidget.setCurrentIndex(
            self.containers['help'])
        self.helpWebView.setFocus()

    def helpProjetPage(self):
        """
        blablabla
        """
        utils_functions.webHelpInBrowser(utils.HELPPAGE)

    def test(self):
        # ICI POUR FAIRE DES TESTS :
        print('test')
        self.markdownContainer.pdfViewer.page().setHtml('aaa')
        #self.markdownContainer.pdfViewer.reloadAndBypassCache()



    ###########################################
    # création de l'interface, des actions et des menus :
    ###########################################

    def createInterface(self):
        """
        blablabla
        """

        # un helpWebView pour afficher le fichier :
        self.helpWebView = utils_webengine.MyWebEngineView(self)
        mdFile = utils_functions.doLocale(
            self.locale, 'translations/help', '.md')
        helpFile = utils_markdown.md2html(self, mdFile)
        helpFile = QtCore.QUrl().fromLocalFile(helpFile)
        self.helpWebView.load(helpFile)
        # bouton helpInBrowser :
        helpInBrowserButton = QtWidgets.QPushButton(
            QtWidgets.QApplication.translate('main', 'Project page'))
        helpInBrowserButton.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Opens the project page in your browser.'))
        # mise en place :
        helpContainer = QtWidgets.QWidget()
        vBoxLayout = QtWidgets.QVBoxLayout()
        vBoxLayout.addWidget(self.helpWebView)
        vBoxLayout.addWidget(helpInBrowserButton)
        helpContainer.setLayout(vBoxLayout)

        # affichage du jeu :
        self.gameWebView = utils_webengine.MyWebEngineView(self)
        # mise en place :
        gameContainer = QtWidgets.QWidget()
        vBoxLayout = QtWidgets.QVBoxLayout()
        vBoxLayout.addWidget(self.gameWebView)
        gameContainer.setLayout(vBoxLayout)

        # conversion pdf -> images :
        self.pdfContainer = utils_pdf.Pdf2ImagesDlg(parent=self)

        # éditeur Markdown :
        self.markdownContainer = utils_markdown.MarkdownCreationDlg(parent=self)

        # envoi des fichiers :
        uploadContainer = utils_web.UploadCardsDlg(parent=self)

        # gestion des utilisateurs :
        self.usersWebView = utils_webengine.MyWebEngineView(self)
        # mise en place :
        usersContainer = QtWidgets.QWidget()
        vBoxLayout = QtWidgets.QVBoxLayout()
        vBoxLayout.addWidget(self.usersWebView)
        usersContainer.setLayout(vBoxLayout)

        # le stackedWidget est le widget central :
        self.stackedWidget = QtWidgets.QStackedWidget()
        self.stackedWidget.addWidget(helpContainer)
        self.stackedWidget.addWidget(gameContainer)
        self.stackedWidget.addWidget(self.pdfContainer)
        self.stackedWidget.addWidget(self.markdownContainer)
        self.stackedWidget.addWidget(uploadContainer)
        self.stackedWidget.addWidget(usersContainer)
        self.setCentralWidget(self.stackedWidget)
        self.containers = {
            'help': 0, 
            'game': 1, 
            'pdf': 2, 
            'markdown': 3, 
            'upload': 4, 
            'users': 5, 
            }
        self.closing = False

        # le reste de l'interface :
        self.statusbar = QtWidgets.QStatusBar()
        self.setStatusBar(self.statusbar)
        self.toolBar = QtWidgets.QToolBar(
            QtWidgets.QApplication.translate('main', 'Base Bar'))
        iconSize = int(utils.STYLE['PM_LargeIconSize'] * 2)
        self.toolBar.setIconSize(QtCore.QSize(iconSize, iconSize))
        self.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolBar)

        self.createActions()
        self.createMenusAndButtons()

        if utils.SOFTS.get('pdftoppm', '') == '':
            self.actionPdf2X.setEnabled(False)

        helpInBrowserButton.clicked.connect(self.helpProjetPage)

        self.stackedWidget.setCurrentIndex(
            self.containers['help'])

    def disableInterface(self, dialog):
        """
        cache les barres d'outils lors de l'appel d'un dialog
        affiché dans le stackedWidget.
        On retourne aussi :
            lastIndex : le widget affiché précédemment dans le stackedWidget
            lastTitle : le titre de la fenêtre
            lastMessage : le texte du statusBar
        """
        lastIndex = self.stackedWidget.currentIndex()
        lastTitle = self.windowTitle()
        lastMessage = self.statusBar().currentMessage()
        for widget in (self.menuBar(), self.toolBar):
            widget.setVisible(False)
        self.setWindowTitle(dialog.windowTitle())
        self.statusBar().clearMessage()
        newIndex = self.stackedWidget.addWidget(dialog)
        self.stackedWidget.setCurrentIndex(newIndex)
        return (lastIndex, lastTitle, lastMessage)

    def enableInterface(self, dialog, lastState=(0, '', '')):
        """
        replace les barres d'outils après l'appel d'un dialog
        affiché dans le stackedWidget.
        On remet aussi l'état précédent via lastState :
            widget affiché dans le stackedWidget
            titre de la fenêtre
            texte du statusBar
        """
        if self.closing:
            return
        self.stackedWidget.removeWidget(dialog)
        self.stackedWidget.setCurrentIndex(lastState[0])
        self.setWindowTitle(lastState[1])
        self.statusBar().showMessage(lastState[2])
        for widget in (self.menuBar(), self.toolBar):
            widget.setVisible(True)

    def createActions(self):
        """
        blablabla
        """
        self.actionQuit = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Exit'), 
            self, 
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Quit), 
            icon=utils.doIcon('application-exit'))
        self.actionQuit.triggered.connect(self.close)

        self.actionOpenCardsDir = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Open CARDS Dir'), 
            self, 
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Open), 
            icon=utils.doIcon('cards-dir-open'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'Open the directory containing cards.'))
        self.actionOpenCardsDir.triggered.connect(self.openCardsDir)


        self.actionTestGame = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Test the game'), 
            self, 
            icon=utils.doIcon('roll'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To test the game directly in the interface.'))
        self.actionTestGame.triggered.connect(self.testGame)


        self.actionPdf2X = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'PDF file'), self,
            icon=utils.doIcon('pdf'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To create cards from a PDF file.'))
        self.actionPdf2X.triggered.connect(self.pdf2Images)

        self.actionMarkdown = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Markdown editor'), self,
            icon=utils.doIcon('markdown'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To create cards in Markdown syntax.'))
        self.actionMarkdown.triggered.connect(self.markdownEdit)

        self.actionPrintCards = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Print cards'), self,
            icon=utils.doIcon('document-print'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To create printable versions of the cards.'))
        self.actionPrintCards.triggered.connect(self.printCards)


        self.actionUploadCards = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Download or upload cards'), self,
            icon=utils.doIcon('cards-upload'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To download cards of a site or send your own.'))
        self.actionUploadCards.triggered.connect(self.uploadCards)

        self.actionGestUsers = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'User management'), self,
            icon=utils.doIcon('users'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'You must be a site administrator to use this action.'))
        self.actionGestUsers.triggered.connect(self.gestUsers)



        self.actionCreateLinuxDesktopFile = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Create a launcher'), self,
            icon=utils.doIcon('logo_linux'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To create a launcher (*.desktop file) in the folder of your choice.'))
        self.actionCreateLinuxDesktopFile.triggered.connect(self.doCreateLinuxDesktopFile)

        self.actionCreateWinShortcutFile = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Create a shortcut'), self,
            icon=utils.doIcon('logo_windows'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To create a shortcut (*.lnk file) on your Desktop.'))
        self.actionCreateWinShortcutFile.triggered.connect(self.doCreateWinShortcutFile)

        self.actionHelp = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Help'), self,
            icon=utils.doIcon('help'), 
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.HelpContents))
        self.actionHelp.triggered.connect(self.showHelp)

        self.actionHelpInBrowser = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Project page'), self,
            icon=utils.doIcon('help'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'Opens the project page in your browser.'))
        self.actionHelpInBrowser.triggered.connect(self.helpProjetPage)

        self.actionAbout = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'About'), self,
            icon=utils.doIcon('help-about'))
        self.actionAbout.triggered.connect(self.about)

        # ICI POUR FAIRE DES TESTS :
        self.actionTest = QtWidgets.QAction(
            'Test', self,
            icon=utils.doIcon('help-about'))
        self.actionTest.triggered.connect(self.test)

    def createMenusAndButtons(self):
        """
        blablabla
        """
        self.fileMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'File'), self)
        self.fileMenu.addAction(self.actionTestGame)
        self.fileMenu.addAction(self.actionOpenCardsDir)
        if utils.OS_NAME[0] == 'linux':
            self.fileMenu.addSeparator()
            self.fileMenu.addAction(self.actionCreateLinuxDesktopFile)
        elif utils.OS_NAME[0] == 'win':
            self.fileMenu.addSeparator()
            self.fileMenu.addAction(self.actionCreateWinShortcutFile)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.actionQuit)

        self.utilsMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'Tools'), self)
        self.utilsMenu.addAction(self.actionPdf2X)
        self.utilsMenu.addAction(self.actionMarkdown)
        self.utilsMenu.addAction(self.actionPrintCards)
        self.utilsMenu.addSeparator()
        self.utilsMenu.addAction(self.actionUploadCards)
        self.utilsMenu.addSeparator()
        self.utilsMenu.addAction(self.actionGestUsers)

        self.helpMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'Help'), self)
        self.helpMenu.addAction(self.actionHelp)
        self.helpMenu.addAction(self.actionHelpInBrowser)
        self.helpMenu.addAction(self.actionAbout)
        # ICI POUR FAIRE DES TESTS :
        #self.helpMenu.addSeparator()
        #self.helpMenu.addAction(self.actionTest)

        self.menuBar().addMenu(self.fileMenu)
        self.menuBar().addMenu(self.utilsMenu)
        self.menuBar().addMenu(self.helpMenu)

        self.toolBar.addAction(self.actionQuit)
        self.toolBar.addAction(self.actionHelp)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionTestGame)
        self.toolBar.addAction(self.actionOpenCardsDir)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionPdf2X)
        self.toolBar.addAction(self.actionMarkdown)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionUploadCards)


