# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Ce module contient les variables et fonctions utiles au programme.
"""

# modules utiles :
import sys
import os


"""
****************************************************
    VERSIONS DE PYTHON, QT, ETC
****************************************************
"""

# version de Python :
PYTHON_VERSION = sys.version_info[0] * 10 + sys.version_info[1]
if PYTHON_VERSION < 30:
    print('****************************')
    print('  YOU MUST USE PYTHON 3!')
    print('****************************')

# test de PyQt5 :
try:
    # on teste d'abord PyQt5 :
    from PyQt5 import QtCore, QtWidgets, QtGui
except:
    print('****************************')
    print('  YOU MUST INSTALL PYQT5!')
    print('****************************')

# version de Qt :
QT_VERSION = QtCore.qVersion()

# détection du système (nom et 32 ou 64) :
OS_NAME = ['', '']
def detectPlatform():
    global OS_NAME
    # 32 ou 64 bits :
    if sys.maxsize > 2**32:
        bits = 64
    else:
        bits = 32
    # platform et osName :
    platform = sys.platform
    osName = ''
    if platform.startswith('linux'):
        osName = 'linux'
    elif platform.startswith('win'):
        osName = 'win'
    elif platform.startswith('freebsd'):
        osName = 'freebsd'
    elif platform.startswith('darwin'):
        import platform
        if 'powerpc' in platform.uname():
            osName = 'powerpc'
        else:
            osName = 'mac'
    OS_NAME = [osName, bits]
detectPlatform()

print('PYTHON_VERSION:', PYTHON_VERSION, 'QT_VERSION:', QT_VERSION)



"""
****************************************************
    VARIABLES LIÉES AU LOGICIEL
****************************************************
"""

PROGNAME = 'JeMeSouviens'
PROGVERSION = '0.8'
HELPPAGE = 'http://pascal.peter.free.fr/jemesouviens.html'



"""
****************************************************
    DIVERS
****************************************************
"""

HERE = ''

def changeHere(newValue):
    global HERE
    HERE = newValue




# présence des logiciels tiers :
SOFTS = {}

def changeSofts(newDic):
    global SOFTS
    for what in newDic:
        SOFTS[what] = newDic[what]







EXTENSIONS = ('jpg', 'jpeg', 'png', 'svg',)

DEFAULTCONFIG = {
    'CONFIG': {
        'webUrl': 'http://clgdrouyn.fr/jemesouviens', 
        'user': '', 
        'password': '', 
        'webSubDirs': '', 
        'userId': -1, 
        },
    'LASTDIRS': [],
    'TEMP': {},
    }




STYLE = {}
def loadStyle():
    global STYLE
    style = QtWidgets.QApplication.style()
    STYLE = {
        'PM_ToolBarIconSize': style.pixelMetric(QtWidgets.QStyle.PM_ToolBarIconSize), 
        'PM_LargeIconSize': style.pixelMetric(QtWidgets.QStyle.PM_LargeIconSize), 
        #'PM_SmallIconSize': style.pixelMetric(QtWidgets.QStyle.PM_SmallIconSize), 
        #'PM_IconViewIconSize': style.pixelMetric(QtWidgets.QStyle.PM_IconViewIconSize), 
        #'PM_ListViewIconSize': style.pixelMetric(QtWidgets.QStyle.PM_ListViewIconSize), 
        #'PM_TabBarIconSize': style.pixelMetric(QtWidgets.QStyle.PM_TabBarIconSize), 
        #'PM_MessageBoxIconSize': style.pixelMetric(QtWidgets.QStyle.PM_MessageBoxIconSize), 
        #'PM_ButtonIconSize': style.pixelMetric(QtWidgets.QStyle.PM_ButtonIconSize), 
        }


SUPPORTED_IMAGE_FORMATS = ('png',)

def loadSupportedImageFormats():
    global SUPPORTED_IMAGE_FORMATS
    SUPPORTED_IMAGE_FORMATS = QtGui.QImageReader.supportedImageFormats()

def doIcon(fileName='', ext='svgz', what='ICON'):
    if ext in SUPPORTED_IMAGE_FORMATS:
        allFileName = 'images/{0}.{1}'.format(fileName, ext)
    else:
        allFileName = 'images/png/{0}.png'.format(fileName)
    if not(QtCore.QFile(allFileName).exists()):
        print('doIcon:', allFileName)
        allFileName = 'images/png/{0}.png'.format(fileName)
    if what == 'ICON':
        return QtGui.QIcon(allFileName)
    else:
        return QtGui.QPixmap(allFileName)

