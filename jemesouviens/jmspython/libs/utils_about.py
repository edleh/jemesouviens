# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    La fenêtre "À propos"
"""

# modules perso :
import utils, utils_functions, utils_webengine, utils_markdown

# modules PyQt :
from PyQt5 import QtCore, QtWidgets, QtGui



"""
****************************************************
    LA FENÊTRE À PROPOS
****************************************************
"""

class AboutDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, locale='', icon='./images/logo.png'):
        super(AboutDlg, self).__init__(parent)
        self.main = parent

        # En-tête de la fenêtre :
        size = 128
        logoLabel = QtWidgets.QLabel()
        logoLabel.setMaximumSize(size, size)
        logoLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        logoLabel.setPixmap(
            QtGui.QPixmap(icon).scaled(
                size, 
                size, 
                QtCore.Qt.KeepAspectRatio,
                QtCore.Qt.SmoothTransformation))
        translatedText0 = QtWidgets.QApplication.translate(
            'main', 'About {0}')
        translatedText0 = translatedText0.format(utils.PROGNAME)
        translatedText1 = QtWidgets.QApplication.translate(
            'main', '(version {0})')
        translatedText1 = translatedText1.format(utils.PROGVERSION)
        translatedText = (
            '<h1>{0}</h1>'
            '<h4>{1}</h4>').format(
                translatedText0, translatedText1)
        titleLabel = QtWidgets.QLabel(translatedText)
        titleGroupBox = QtWidgets.QGroupBox()
        titleLayout = QtWidgets.QHBoxLayout()
        titleLayout.addWidget(logoLabel)
        titleLayout.addWidget(titleLabel)
        titleGroupBox.setLayout(titleLayout)

        # Zone d'affichage :
        tabWidget = QtWidgets.QTabWidget()
        mdFile = utils_functions.doLocale(
            locale, 'translations/README', '.md')
        tabWidget.addTab(
            FileViewTab(parent=self.main, fileName=mdFile, fileType='MD'),
            QtWidgets.QApplication.translate('main', 'About'))
        tabWidget.addTab(
            FileViewTab(parent=self.main, fileName='COPYING'),
            QtWidgets.QApplication.translate('main', 'License'))

        # Les boutons :
        okButton = QtWidgets.QPushButton(
            QtWidgets.QApplication.translate('main', 'Close'))
        okButton.clicked.connect(self.accept)
        buttonLayout = QtWidgets.QHBoxLayout()
        buttonLayout.addStretch(1)
        buttonLayout.addWidget(okButton)

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleGroupBox)
        mainLayout.addWidget(tabWidget)
        mainLayout.addLayout(buttonLayout)
        self.setLayout(mainLayout)
        self.setWindowTitle(
            QtWidgets.QApplication.translate(
                'main', 'About {0}').format(utils.PROGNAME))


class FileViewTab(QtWidgets.QWidget):
    """
    pour afficher un fichier txt, html ou md.
    """
    def __init__(self, parent=None, fileName='', fileType='TXT'):
        super(FileViewTab, self).__init__(parent)
        self.main = parent
        self.waiting = True
        # mise en place du conteneur :
        if fileType in ('HTML', 'MD'):
            widget = utils_webengine.MyWebEngineView(
                self, linksInBrowser=True)
            container = QtWidgets.QAbstractScrollArea()
            vBoxLayout = QtWidgets.QVBoxLayout()
            vBoxLayout.addWidget(widget)
            container.setLayout(vBoxLayout)
        else:
            widget = container = QtWidgets.QTextEdit()
            widget.setReadOnly(True)
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addWidget(container)
        self.setLayout(mainLayout)
        # ouverture du fichier :
        if fileType == 'HTML':
            fileName = '{0}/{1}'.format(utils.HERE, fileName)
            url = QtCore.QUrl().fromLocalFile(fileName)
            widget.load(url)
        elif fileType == 'MD':
            outFileName = utils_markdown.md2html(self.main, fileName)
            url = QtCore.QUrl().fromLocalFile(outFileName)
            widget.load(url)
        else:
            inFile = QtCore.QFile(fileName)
            if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(inFile)
                stream.setCodec('UTF-8')
                widget.setPlainText(stream.readAll())
                inFile.close()
        self.waiting = False



