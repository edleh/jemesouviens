# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Un label pour faire des drag-drop de fichiers.
"""

# modules PyQt :
from PyQt5 import QtCore, QtWidgets, QtGui



class DropArea(QtWidgets.QLabel):

    def __init__(self, parent=None, extension='', requiredDir=''):
        super(DropArea, self).__init__(parent)
        self.parent = parent

        self.extension = extension
        self.requiredDir = requiredDir
        self.texts = {
            'last' : '', 
            'nothing' : QtWidgets.QApplication.translate('main', '<drop here>'), 
            'bad file' : QtWidgets.QApplication.translate('main', 'This file is not valid.'), 
            'bad folder' : QtWidgets.QApplication.translate('main', 'The file must be in the <b>MyCards</b> folder.'), 
            }

        self.setFrameStyle(QtWidgets.QFrame.Sunken | QtWidgets.QFrame.StyledPanel)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.setAcceptDrops(True)
        self.setAutoFillBackground(True)
        self.clear()

    def dragEnterEvent(self, event):
        self.saveLast()
        self.setText(self.texts['nothing'])
        self.setBackgroundRole(QtGui.QPalette.Highlight)
        event.acceptProposedAction()

    def dragMoveEvent(self, event):
        event.acceptProposedAction()

    def dropEvent(self, event):
        ok = False
        text = self.texts['bad file']
        mimeData = event.mimeData()
        if mimeData.hasUrls():
            #print('hasUrls:', mimeData.urls())
            if len(mimeData.urls()) == 1:
                fileName = mimeData.urls()[0].toLocalFile()
                #print('fileName:', fileName)
                if self.extension == '':
                    text = fileName
                else:
                    # on vérifie l'extension :
                    extension = QtCore.QFileInfo(fileName).suffix()
                    if extension.lower() == self.extension:
                        text = fileName
                    else:
                        text = self.texts['bad file']
                # on vérifie aussi le chemin :
                if self.requiredDir != '':
                    absoluteFilePath = QtCore.QFileInfo(fileName).absoluteFilePath()
                    if absoluteFilePath[:len(self.requiredDir)] != self.requiredDir:
                        text = self.texts['bad folder']
                    elif text == fileName:
                        ok = True
                        text = absoluteFilePath[len(self.requiredDir):]

        self.setText(text)
        self.setBackgroundRole(QtGui.QPalette.Dark)
        event.acceptProposedAction()
        if ok:
            self.doAfterDrop()

    def doAfterDrop(self):
        """
        si self.parent a une fonction doAfterDrop 
        pour traiter l'après drag-drop, on l'appelle.
        """
        try:
            self.parent.doAfterDrop()
        except:
            pass

    def dragLeaveEvent(self, event):
        self.clear()
        event.accept()

    def clear(self):
        self.setText(self.texts['nothing'])
        self.setBackgroundRole(QtGui.QPalette.Dark)

    def saveLast(self):
        self.texts['last'] = self.text()
        self.setBackgroundRole(QtGui.QPalette.Dark)

    def restoreLast(self):
        self.setText(self.texts['last'])
        self.setBackgroundRole(QtGui.QPalette.Dark)


