# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Fonctions de manipulation de fichiers ou dossiers :
    copier, ...
"""

# modules utiles :
import sys
import os

# modules perso :
import utils, utils_functions

# modules PyQt :
from PyQt5 import QtCore, QtGui



"""
****************************************************
    CONFIGURATION
****************************************************
"""

def createTempAppDir(PROGLINK, mustReCreate=True):
    """
    (re)création du dossier temporaire
    on cree un sous-dossier /tmp/PROGLINK
    """
    tempDir = QtCore.QDir.temp()
    tempAppPath = '{0}/{1}'.format(
        QtCore.QDir.tempPath(), PROGLINK)
    if mustReCreate:
        emptyDir(tempAppPath)
        tempDir.rmdir(PROGLINK)
    tempAppDir = QtCore.QDir(tempAppPath)
    if tempAppDir.exists():
        return tempAppPath
    elif tempDir.mkdir(PROGLINK):
        return tempAppPath
    else:
        return QtCore.QDir.tempPath()

def createConfigAppDir(PROGLINK):
    """
    Crée un sous dossier ".PROGLINK" dans le home (dossier utilisateur)
    (ou un dossier PROGLINK dans home/.config)
    Ce sera le dossier de configuration du logiciel
    """
    first = False
    progLink = '.' + PROGLINK
    # récupération du dossier home:
    if utils.OS_NAME[0] == 'win':
        # home on win32 is broken
        if 'APPDATA' in os.environ:
            winAppDataDir = os.environ['APPDATA']
        else:
            winAppDataDir = QtCore.QDir.homePath()
        homeDir = QtCore.QDir(winAppDataDir)
        homeDirPath = QtCore.QDir(winAppDataDir).path()
    else:
        homeDir = QtCore.QDir.home()
        homeDirPath = QtCore.QDir.home().path()
        # test de la présence d'un dossier .config :
        pConfigPath = homeDirPath + '/.config/'
        if QtCore.QDir(pConfigPath).exists():
            # s'il y a un home/.PROGLINK, on le déplace dans .config (en supprimant le .) :
            pProgLinkPath = homeDirPath + '/.' + PROGLINK + '/'
            if QtCore.QDir(pProgLinkPath).exists():
                if not(QtCore.QDir(pConfigPath + PROGLINK).exists()):
                    QtCore.QDir(pConfigPath).mkdir(PROGLINK)
                copyDir(pProgLinkPath, pConfigPath + PROGLINK)
                emptyDir(pProgLinkPath)
            progLink = PROGLINK
            homeDirPath = homeDirPath + '/.config'

    # création du dossier config dans le home:
    configAppDir = QtCore.QDir(homeDirPath + '/' + progLink + '/')
    if not(configAppDir.exists()):
        first = True
        QtCore.QDir(homeDirPath).mkdir(progLink)
        configAppDir = QtCore.QDir(homeDirPath + '/' + progLink + '/')
    return configAppDir, first



"""
****************************************************
    FICHIERS ET DOSSIERS (COPIE, ETC)
****************************************************
"""

def openFile(fileName):
    localefileFile = QtCore.QFileInfo(fileName)
    localefileFile.makeAbsolute()
    thefile = localefileFile.filePath()
    url = QtCore.QUrl.fromLocalFile(thefile)
    QtGui.QDesktopServices.openUrl(url)

def openDir(dirName):
    """
    Sous Windobs, ça ne marche qu'avec des ~1 partout
    Comme quoi Dos existe encore...
    Pas sùr que ça marche à tous les coups.
    """
    try:
        url = QtCore.QUrl().fromLocalFile(dirName)
        QtGui.QDesktopServices.openUrl(url)
    except:
        if utils.OS_NAME[0] == 'win':
            dirName = os.sep.join(dirName.split('/'))
            commandLine = 'explorer "{0}"'.format(dirName)
            os.system(commandLine)

def copyDir(src, dst, ignore=()):
    """
    copie récursive d'un dossier dans un autre
    """
    src = utils_functions.addSlash(src)
    dst = utils_functions.addSlash(dst)
    has_err = False
    srcDir = QtCore.QDir(src)
    dstDir = QtCore.QDir(dst)
    if srcDir.exists():
        entries = srcDir.entryInfoList(
            QtCore.QDir.NoDotAndDotDot | QtCore.QDir.Dirs | QtCore.QDir.Files | QtCore.QDir.Hidden)
        for entryInfo in entries:
            name = entryInfo.fileName()
            path = entryInfo.absoluteFilePath()
            if entryInfo.isDir():
                if not(name in ignore):
                    dstDir.mkdir(name)
                    # on fait suivre :
                    has_err = copyDir(
                        '{0}{1}'.format(src, name), 
                        '{0}{1}'.format(dst, name), 
                        ignore=ignore)
            elif entryInfo.isFile():
                if not(name in ignore):
                    srcFileName = '{0}{1}'.format(src, name)
                    dstFileName = '{0}{1}'.format(dst, name)
                    if QtCore.QFile(dstFileName).exists():
                        QtCore.QFile(dstFileName).remove()
                    QtCore.QFile(srcFileName).copy(dstFileName)
    return has_err

def removeAndCopy(sourceFile, destFile, testSize=-1):
    if testSize > 0:
        sourceSize = QtCore.QFileInfo(sourceFile).size()
        if sourceSize < testSize:
            print(
                'TEST SIZE : ', sourceFile, sourceSize)
            return False
    if QtCore.QFile(destFile).exists():
        removeOK = QtCore.QFile(destFile).remove()
        if not(removeOK):
            print('REMOVE ERROR : ', destFile)
    copyOK = QtCore.QFile(sourceFile).copy(destFile)
    if not(copyOK):
        print('COPY ERROR : ', sourceFile, destFile)
    return copyOK

def emptyDir(dirName, deleteThisDir=True, filesToKeep=[]):
    """
    Vidage récursif d'un dossier.
    Si deleteThisDir est mis à False, le dossier lui-même n'est pas supprimé.
    filesToKeep est une liste de noms de fichiers à ne pas effacer 
        (filesToKeep=['.htaccess'] par exemple).
    """
    has_err = False
    aDir = QtCore.QDir(dirName)
    if aDir.exists():
        entries = aDir.entryInfoList(
            QtCore.QDir.NoDotAndDotDot | QtCore.QDir.Dirs | QtCore.QDir.Files | QtCore.QDir.Hidden)
        for entryInfo in entries:
            path = entryInfo.absoluteFilePath()
            if entryInfo.isDir():
                # on fait suivre filesToKeep, mais deleteThisDir sera True :
                has_err = emptyDir(path, filesToKeep=filesToKeep)
            elif entryInfo.isFile():
                if not(entryInfo.fileName() in filesToKeep):
                    f = QtCore.QFile(path)
                    if f.exists():
                        if not(f.remove()):
                            print('PB: ', path)
                            has_err = True
        if deleteThisDir:
            if not(aDir.rmdir(aDir.absolutePath())):
                print('Erreur de suppression de : ' + aDir.absolutePath())
                has_err = True
    return has_err

def createDirs(inPath, newPaths):
    """
    création de sous-dossiers (newPaths) dans un dossier (inPath)
    newPaths peut contenir plusieurs dossiers à créer
    Par exemple, createDirs(inPath, 'abé/ééc')
    """
    if newPaths.split('/')[0] != '':
        a, b = newPaths.split('/')[0], '/'.join(newPaths.split('/')[1:])
    else:
        a, b = newPaths.split('/')[1], '/'.join(newPaths.split('/')[2:])
    if a != '':
        inDir = QtCore.QDir(inPath)
        newDir = QtCore.QDir('{0}/{1}'.format(inPath, a))
        if not(newDir.exists()):
            inDir.mkdir(a)
        if b != '':
            createDirs('{0}/{1}'.format(inPath, a), b)



"""
****************************************************
    ARCHIVAGE
****************************************************
"""

def md5Sum(fileName):
    """
    calcule la somme md5 d'un fichier
    """
    import hashlib
    md5 = hashlib.md5()
    with open(fileName, 'rb') as f: 
        for chunk in iter(lambda: f.read(128 * md5.block_size), b''): 
             md5.update(chunk)
    return md5.hexdigest()

def tarDirectory(main, tarFileName, directory, beginDir):
    result = False
    try:
        import tarfile
        QtCore.QDir.setCurrent(directory + '/..')
        tar = tarfile.open(tarFileName + '.tar.gz', 'w:gz')
        tar.add(tarFileName, tarFileName)
        tar.close()
        QtCore.QDir.setCurrent(beginDir)
        result = True
    finally:
        return result

def untarDirectory(tarFileName, directory):
    result = False
    try:
        import tarfile
        tar = tarfile.open(tarFileName, 'r:gz')
        tar.extractall(directory)
        tar.close()
        result = True
    finally:
        return result



"""
****************************************************
    DIVERS
****************************************************
"""

def readTextFile(fileName):
    """
    retourne le contenu d'un fichier texte.
    fileContent = utils_filesdirs.readTextFile(fileName)
    """
    result = ''
    inFile = QtCore.QFile(fileName)
    if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(inFile)
        stream.setCodec('UTF-8')
        result = stream.readAll()
        inFile.close()
    return result

def writeTextFile(fileName, lines):
    """
    retourne le contenu d'un fichier texte.
    utils_filesdirs.writeTextFile(fileName, lines)
    """
    result = False
    QtCore.QFile(fileName).remove()
    outFile = QtCore.QFile(fileName)
    if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(outFile)
        stream.setCodec('UTF-8')
        stream << lines
        outFile.close()
        result = True
    return result

def createLinuxDesktopFile(main, beginDir, directory, fileName, progName):
    """
    création du fichier progName.desktop
    """
    # on ouvre le fichier livré avec l'archive :
    desktopFileName = '{0}/files/{1}.desktop'.format(
        beginDir, fileName)
    desktopFile = QtCore.QFile(desktopFileName)
    if not(desktopFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text)):
        return False
    stream = QtCore.QTextStream(desktopFile)
    stream.setCodec('UTF-8')
    lines = stream.readAll()
    # on remplace les variables :
    lines = lines.replace('PYTHON', sys.executable)
    lines = lines.replace('CHEMIN', beginDir)
    lines = lines.replace('PROGNAME', progName)
    desktopFile.close()
    # on crée le fichier final :
    desktopFileName = '{0}/{1}.desktop'.format(
        directory, fileName)
    QtCore.QFile(desktopFileName).remove()
    desktopFile = QtCore.QFile(desktopFileName)
    if desktopFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(desktopFile)
        stream.setCodec('UTF-8')
        stream << lines
        desktopFile.close()
    # avec les bons attributs :
    QtCore.QFile(desktopFileName).setPermissions(\
        QtCore.QFile(desktopFileName).permissions() | \
        QtCore.QFile.ExeOwner | \
        QtCore.QFile.ExeUser | \
        QtCore.QFile.ExeGroup | \
        QtCore.QFile.ExeOther)
    # on le copie aussi dans ~/.local/share/applications :
    applicationsPath = QtCore.QDir.homePath() + '/.local/share/applications'
    if QtCore.QDir(applicationsPath).exists():
        desktopFileName2 = '{0}/{1}.desktop'.format(
            applicationsPath, fileName)
        QtCore.QFile(desktopFileName2).remove()
        QtCore.QFile(desktopFileName).copy(desktopFileName2)
    return True

def createWinShortcutFile(main, progName, title, icon='icon'):
    """
    création du fichier progName.lnk sur le bureau windows.
    """
    import os, sys
    import pythoncom
    from win32com.shell import shell, shellcon
    shortcut = pythoncom.CoCreateInstance(
        shell.CLSID_ShellLink, 
        None, 
        pythoncom.CLSCTX_INPROC_SERVER, 
        shell.IID_IShellLink)
    program_location = sys.executable.replace('python.exe', 'pythonw.exe')
    shortcut.SetPath(program_location)
    shortcut.SetDescription(title)
    shortcut.SetArguments('"{0}{1}{2}.pyw"'.format(utils.HERE, os.sep, progName))
    shortcut.SetIconLocation('{0}{1}images{1}{2}.ico'.format(utils.HERE, os.sep, icon), 0)
    desktop_path = shell.SHGetFolderPath(0, shellcon.CSIDL_DESKTOP, 0, 0)
    persist_file = shortcut.QueryInterface(pythoncom.IID_IPersistFile)
    persist_file.Save(os.path.join(desktop_path, '{0}.lnk'.format(progName)), 0)
















