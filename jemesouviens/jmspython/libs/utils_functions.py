# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Ce module contient des fonctions utiles au programme.
"""

# modules perso :
import utils

# modules PyQt :
from PyQt5 import QtCore, QtWidgets, QtGui



"""
****************************************************
    MESSAGES, BOUTONS, ...
****************************************************
"""

NOWAITCURSOR = False
def changeNoWaitCursor(newNoWaitCursor):
    global NOWAITCURSOR
    NOWAITCURSOR = newNoWaitCursor

def doWaitCursor():
    if not(NOWAITCURSOR):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)

def restoreCursor():
    if not(NOWAITCURSOR):
        QtWidgets.QApplication.restoreOverrideCursor()

def messageBox(main, level='information', title='', message='',
               detailedText='',
               buttons=['Ok'], defaultButton=QtWidgets.QMessageBox.NoButton):
    """
    """
    # on teste l'aspect du curseur (doit être normal) :
    try:
        waitCursor = (QtWidgets.QApplication.overrideCursor().shape() == QtCore.Qt.WaitCursor)
    except:
        waitCursor = False
    if waitCursor:
        QtWidgets.QApplication.restoreOverrideCursor()
    # gestion des boutons (standards ou persos) :
    buttonsDic = {
        'Ok': QtWidgets.QMessageBox.Ok,
        'Yes': QtWidgets.QMessageBox.Yes,
        'No': QtWidgets.QMessageBox.No,
        'NoToAll': QtWidgets.QMessageBox.NoToAll,
        'Cancel': QtWidgets.QMessageBox.Cancel,
        'Open': QtWidgets.QMessageBox.Open,
        'Save': QtWidgets.QMessageBox.Save,
        'Discard': QtWidgets.QMessageBox.Discard,
        'Abort': QtWidgets.QMessageBox.Abort,
        'Close': QtWidgets.QMessageBox.Close,
        'Help': QtWidgets.QMessageBox.Help,
        }
    buttonsToAdd = []
    standardButtons = QtWidgets.QMessageBox.NoButton
    for button in buttons:
        if button in buttonsDic:
            standardButtons = standardButtons | buttonsDic[button]
        else:
            # les boutons persos seront ajoutés plus tard :
            buttonsToAdd.append(button)
    # titre de la fenêtre :
    titlesDic = {
        'information': QtWidgets.QApplication.translate('main', 'information message'),
        'question': QtWidgets.QApplication.translate('main', 'question message'),
        'warning': QtWidgets.QApplication.translate('main', 'warning message'),
        'critical': QtWidgets.QApplication.translate('main', 'critical message'),
        }
    if title == '':
        title = '{0} ({1})'.format(utils.PROGNAME, titlesDic[level])
    # icône :
    iconsDic = {
        'information': QtWidgets.QMessageBox.Information,
        'question': QtWidgets.QMessageBox.Question,
        'warning': QtWidgets.QMessageBox.Warning,
        'critical': QtWidgets.QMessageBox.Critical,
        }
    icon = iconsDic[level]
    # on peut créer la boîte de dialogue :
    messageBox = QtWidgets.QMessageBox(icon, title, message, standardButtons, main)
    # on ajoute les boutons persos :
    for button in buttonsToAdd:
        if isinstance(button, tuple):
            theButton = QtWidgets.QPushButton(QtGui.QIcon(button[0]), button[1])
            messageBox.addButton(theButton, QtWidgets.QMessageBox.NoRole)
        else:
            messageBox.addButton(button, QtWidgets.QMessageBox.NoRole)
    # le texte détaillé s'il existe :
    if detailedText != '':
        messageBox.setDetailedText(detailedText)
    # on affiche la boîte :
    result = messageBox.exec_()
    # on remet le curseur wait si besoin :
    if waitCursor:
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
    return result

def afficheStatusBar(main, message=''):
    # pour afficher message dans la StatusBar de la fenêtre principale
    try:
        main.statusBar().showMessage(message)
    except:
        pass

def afficheMsgFin(main, message=''):
    restoreCursor()
    afficheStatusBar(main)
    endMessage = QtWidgets.QApplication.translate('main', 'END !')
    if message != '':
        message = '<p>{0}</p>'.format(message)
    allMessage = (
        '<p align="center">__________________________</p>'
        '<p align="center"><b>{0}</b></p>'
        '{1}<p></p>').format(endMessage, message)
    QtWidgets.QMessageBox.information(main, utils.PROGNAME, allMessage)



"""
****************************************************
    GESTION D'UN PROGRESSDIALOG
****************************************************
"""

VALUEPROGRESS = 0
TOTALPROGRESS = 1

def initProgressDialog(main, title='', totalProgress=1):
    # création du progressDialog :
    global VALUEPROGRESS, TOTALPROGRESS
    VALUEPROGRESS = 0
    TOTALPROGRESS = totalProgress
    message = '{0}/{1} ({2})'.format(VALUEPROGRESS, TOTALPROGRESS, title)
    progressDialog = QtWidgets.QProgressDialog(main)
    progressDialog.setCancelButton(None)
    progressDialog.setWindowTitle(title)
    progressDialog.setMinimumWidth(500)
    progressDialog.setMaximum(TOTALPROGRESS)
    progressDialog.setValue(VALUEPROGRESS)
    progressDialog.setLabelText(message)
    progressDialog.show()
    QtWidgets.QApplication.processEvents()
    return progressDialog

def incrementeProgressDialog(main, progressDialog, message=''):
    global VALUEPROGRESS
    VALUEPROGRESS += 1
    message = '{0}/{1} ({2})'.format(VALUEPROGRESS, TOTALPROGRESS, message)
    if progressDialog == None:
        print(message)
    else:
        progressDialog.setValue(VALUEPROGRESS)
        progressDialog.setLabelText(message)
        QtWidgets.QApplication.processEvents()

def endProgressDialog(main, progressDialog):
    if progressDialog != None:
        progressDialog.hide()
        del progressDialog



"""
****************************************************
    DIVERS
****************************************************
"""

def doLocale(locale, beginFileName, endFileName, defaultFileName=''):
    """
    Teste l'existence d'un fichier localisé.
    Par exemple, insère _fr_FR ou _fr entre beginFileName et endFileName.
    Renvoie le fichier par défaut sinon.
    """
    # on teste d'abord avec locale (par exemple fr_FR) :
    localeFileName = '{0}_{1}{2}'.format(beginFileName, locale, endFileName)
    if QtCore.QFileInfo(localeFileName).exists():
        return localeFileName
    # ensuite avec lang (par exemple fr) :
    lang = locale.split('_')[0]
    localeFileName = '{0}_{1}{2}'.format(beginFileName, lang, endFileName)
    if QtCore.QFileInfo(localeFileName).exists():
        return localeFileName
    # si defaultFileName est spécifié :
    if defaultFileName != '':
        return defaultFileName
    # sinon on renvoie le fichier de départ :
    localeFileName = '{0}{1}'.format(beginFileName, endFileName)
    return localeFileName

def addSlash(aDir):
    """
    pour ajouter un / à la fin d'un nom de dossier si besoin
    aDir = utils_functions.addSlash(aDir)
    """
    if aDir[-1] != '/':
        aDir = '{0}/'.format(aDir)
    return aDir

def removeSlash(aDir):
    """
    pour supprimer l'éventuel / à la fin d'un nom de dossier
    aDir = utils_functions.removeSlash(aDir)
    """
    if len(aDir) > 0:
        if aDir[-1] == '/':
            aDir = aDir[:-1]
    return aDir

def webHelpInBrowser(weburl):
    """
    Ouvre un fichier html dans le navigateur.
    """
    url = QtCore.QUrl(weburl)
    QtGui.QDesktopServices.openUrl(url)






def doEncode(main, inText, algo='sha256'):
    """
    retourne une version encodée de inText.
    Utilise Sha256, prévoir plus complexe ?
    http://pythoncentral.io/hashing-strings-with-python
    """
    import hashlib
    if algo == 'sha256':
        outText = hashlib.sha256(inText.encode('utf-8')).hexdigest()
    else:
        outText = hashlib.sha1(inText.encode('utf-8')).hexdigest()
    #print('>>>>>>>>> doEncode:', inText, '->', outText)
    return outText

def verifyLibs_toInt(what):
    """
    utilisé surtout lors d'une réponse php.
    Pour s'assurer qu'on a bien un entier
    """
    result = -1
    try:
        result = int(what)
    except:
        try:
            result = what.toInt()[0]
        except:
            result = -1
    return result










