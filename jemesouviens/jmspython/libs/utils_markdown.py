# -*- coding: utf8 -*-

# -----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    un QSyntaxHighlighter adapté de celui-ci :
    https://github.com/rupeshk/MarkdownHighlighter
"""

# modules utiles :
import re

# modules perso :
import utils, utils_functions, utils_filesdirs, utils_droparea

# modules PyQt :
from PyQt5 import QtCore, QtWidgets, QtGui
try:
    from PyQt5.QtWebKitWidgets import QWebView as QWebEngineView
except ImportError:
    from PyQt5.QtWebEngineWidgets import QWebEngineView





def md2html(main, mdFile, template='default', replace=False):
    """
    retourne un fichier html d'après un fichier md (markdown).
    Les dossiers utils.HERE et main.tempPath
    doivent être définis.
    """
    # fichier final :
    outFileName = '{0}/md/{1}.html'.format(
        main.tempPath, 
        QtCore.QFileInfo(mdFile).baseName())
    # s'il existe déjà, rien à faire de plus :
    if QtCore.QFileInfo(outFileName).exists():
        if replace:
            QtCore.QFile(outFileName).remove()
        else:
            return outFileName
    # fichier du modèle html à utiliser :
    templateFile = '{0}/md/{1}.html'.format(
        main.tempPath, template)
    # si on ne l'a pas trouvé, c'est qu'on lance pour la première fois.
    # on recopie alors le dossier md dans temp :
    if not(QtCore.QFileInfo(templateFile).exists()):
        utils_filesdirs.createDirs(main.tempPath, 'md')
        scrDir = '{0}/files/md'.format(utils.HERE)
        destDir = '{0}/md'.format(main.tempPath)
        utils_filesdirs.copyDir(scrDir, destDir)
    # récupération du contenu du modèle html :
    htmlLines = ''
    inFile = QtCore.QFile(templateFile)
    if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(inFile)
        stream.setCodec('UTF-8')
        htmlLines = stream.readAll()
        inFile.close()
    # récupération du fichier Markdown :
    mdLines = ''
    inFile = QtCore.QFile(mdFile)
    if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(inFile)
        stream.setCodec('UTF-8')
        mdLines = stream.readAll()
        inFile.close()
    # on met en forme et on remplace le repère du modèle :
    mdLines = mdLines.replace('\n', '\\n').replace("'", "\\'")
    htmlLines = htmlLines.replace('# USER TEXT', mdLines)
    # on enregistre le nouveau fichier html :
    outFile = QtCore.QFile(outFileName)
    if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(outFile)
        stream.setCodec('UTF-8')
        stream << htmlLines
        outFile.close()
    return outFileName




def md2htmlPandoc(main, mdFile, template='md2pdf', replace=True):
    if utils.SOFTS.get('pandoc', '') == '':
        return False

    utils_functions.doWaitCursor()
    try:
        QtCore.QDir.setCurrent(main.myCardsDir)
        # fichier final :
        outFileName = '{0}/md/{1}.html'.format(
            main.tempPath, 
            QtCore.QFileInfo(mdFile).baseName())
        # s'il existe déjà, rien à faire de plus :
        if QtCore.QFileInfo(outFileName).exists():
            if replace:
                QtCore.QFile(outFileName).remove()
            else:
                return outFileName

        # fichier du modèle html à utiliser :
        templateFile = '{0}/md/{1}.html'.format(
            main.tempPath, template)
        # si on ne l'a pas trouvé, c'est qu'on lance pour la première fois.
        # on recopie alors le dossier md dans temp :
        if not(QtCore.QFileInfo(templateFile).exists()):
            utils_filesdirs.createDirs(main.tempPath, 'md')
            scrDir = '{0}/files/md'.format(utils.HERE)
            destDir = '{0}/md'.format(main.tempPath)
            utils_filesdirs.copyDir(scrDir, destDir)

        # on arrange le fichier md 
        # (ajout de divs pour centrer l'affichage verticalement)
        # et on le copie dans temp :
        theFile = QtCore.QFile(mdFile)
        if not(theFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text)):
            return False
        stream = QtCore.QTextStream(theFile)
        stream.setCodec('UTF-8')
        lines = stream.readAll()
        theFile.close()
        
        
        """
        lines = lines.split('----')
        begin = lines[0].split('-->\n\n\n')
        divBegin = '<div class="middle">\n'
        divEnd = '</div>\n'
        newLines = begin[0]
        newLines = '{0}-->\n\n\n{1}{2}{3}\n----\n\n'.format(
            newLines, divBegin, begin[1], divEnd)
        newLines2 = '</div>\n----\n\n\n\n<div class="middle">\n'.join(lines[1:])
        newLines2 = '{0}{1}\n{2}'.format(divBegin, newLines2, divEnd)
        lines = '{0}\n{1}'.format(newLines, newLines2)
        """

        # on arrange le fichier md :
        lines = lines.replace('----', '</div>\n----\n<div class="middle">')
        lines = lines.replace(
            'HEADER FOR JMS CONFIG\n-->\n\n\n', 
            'HEADER FOR JMS CONFIG\n-->\n\n\n<div class="middle">')
        lines = '{0}</div>\n'.format(lines)

        # on crée le fichier dans temp :
        mdFileTemp = '{0}/md/{1}.md'.format(
            main.tempPath, 
            QtCore.QFileInfo(mdFile).baseName())
        QtCore.QFile(mdFileTemp).remove()
        theFile = QtCore.QFile(mdFileTemp)
        if theFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(theFile)
            stream.setCodec('UTF-8')
            stream << lines
            theFile.close()



        args = [
            '--template', 
            QtCore.QDir.toNativeSeparators(templateFile)]
        """
        args.extend([
            '-s', '-t', 'html5', '--section-divs', '-o'])
        """
        args.extend([
            '-s', '-t', 'html5', '-o'])
        args.append(QtCore.QDir.toNativeSeparators(outFileName))
        #args.append(QtCore.QDir.toNativeSeparators(mdFile))
        args.append(QtCore.QDir.toNativeSeparators(mdFileTemp))

        #print(utils.SOFTS['pandoc'], ' '.join(args))
        process = QtCore.QProcess(main)
        process.start(utils.SOFTS['pandoc'], args)
        if not process.waitForStarted(3000):
            QtCore.qDebug('BUG IN PROCESS')
            return False
        if not process.waitForFinished():
            return False
        return outFileName
    finally:
        QtCore.QDir.setCurrent(utils.HERE)
        utils_functions.restoreCursor()



































class MarkdownCreationDlg(QtWidgets.QDialog):
    """
    blablabla
    """
    def __init__(self, parent=None):
        super(MarkdownCreationDlg, self).__init__(parent)
        self.main = parent

        self.markdownActualFile = {
            'fileName': '', 
            'mustSave': False}

        # les boutons :
        self.markdownNew = QtWidgets.QPushButton(
            utils.doIcon('document-new'), '', 
            toolTip=QtWidgets.QApplication.translate('main', 'New'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To create a new document.'))
        self.markdownOpen = QtWidgets.QPushButton(
            utils.doIcon('document-open'), '', 
            toolTip=QtWidgets.QApplication.translate('main', 'Open'), 
            statusTip=QtWidgets.QApplication.translate
            ('main', 'To open an existing document.'))
        self.markdownSave = QtWidgets.QPushButton(
            utils.doIcon('document-save'), '', 
            toolTip=QtWidgets.QApplication.translate('main', 'Save'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To save the document.'))
        self.markdownSaveAs = QtWidgets.QPushButton(
            utils.doIcon('document-save-as'), '', 
            toolTip=QtWidgets.QApplication.translate('main', 'Save as...'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To save as new document.'))
        self.markdown2Pdf = QtWidgets.QPushButton(
            utils.doIcon('pdf'), '', 
            toolTip=QtWidgets.QApplication.translate('main', 'Create PDF'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'To create a PDF file from the document.'))
        iconSize = int(utils.STYLE['PM_ToolBarIconSize'] * 1.5)
        tools = (
            self.markdownNew, 
            self.markdownOpen, 
            self.markdownSave, 
            self.markdownSaveAs, 
            self.markdown2Pdf)
        for tool in tools:
            tool.setIconSize(QtCore.QSize(iconSize, iconSize))
        # la zone de drag-drop :
        self.mdDropArea = utils_droparea.DropArea(
            parent=self, 
            extension='md', 
            requiredDir=self.main.absoluteMyCardsDir)
        self.mdDropArea.setMinimumWidth(400)

        # le markdownEditor :
        self.markdownEditor = MarkdownEditor(self)
        # l'affichage du PDF :
        self.pdfViewer = QWebEngineView(self)
        pdfContainer = QtWidgets.QAbstractScrollArea()
        pdfBoxLayout = QtWidgets.QVBoxLayout()
        pdfBoxLayout.addWidget(self.pdfViewer)
        pdfContainer.setLayout(pdfBoxLayout)
        # un splitter horizontal pour agencer les 2 :
        self.hsplitter = QtWidgets.QSplitter(self)
        self.hsplitter.setOrientation(QtCore.Qt.Horizontal)
        self.hsplitter.addWidget(self.markdownEditor)
        self.hsplitter.addWidget(pdfContainer)

        # mise en place :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(self.markdownNew)
        hLayout.addWidget(self.markdownOpen)
        hLayout.addWidget(self.markdownSave)
        hLayout.addWidget(self.markdownSaveAs)
        hLayout.addWidget(self.mdDropArea)
        hLayout.addStretch()
        hLayout.addWidget(self.markdown2Pdf)
        vBoxLayout = QtWidgets.QVBoxLayout()
        vBoxLayout.addLayout(hLayout)
        #vBoxLayout.addWidget(self.markdownEditor)
        vBoxLayout.addWidget(self.hsplitter)
        self.setLayout(vBoxLayout)

        self.markdownEditor.textChanged.connect(self.markdownEditorTextChanged)
        self.markdownNew.clicked.connect(self.markdownNewClicked)
        self.markdownOpen.clicked.connect(self.markdownOpenClicked)
        self.markdownSave.clicked.connect(self.markdownSaveClicked)
        self.markdownSaveAs.clicked.connect(self.markdownSaveAsClicked)
        self.markdown2Pdf.clicked.connect(self.markdown2PdfClicked)
        self.markdownSave.setEnabled(False)
        self.markdown2Pdf.setEnabled(False)

    def showEvent(self, event):
        self.markdownEditor.setFocus()
        QtWidgets.QDialog.showEvent(self, event)

    def updateInterface(self, mustSave=False):
        """
        mise à jour du texte de la zone de drop et des actions.
        Suivant que le fichier est enregistré, a été modifié, etc
        """
        self.markdownActualFile['mustSave'] = mustSave
        if mustSave:
            mustSaveText = ' *'
        else:
            mustSaveText = ''
        # mise à jour du texte de la zone de drop :
        fileName = self.markdownActualFile['fileName']
        absoluteFilePath = QtCore.QFileInfo(fileName).absoluteFilePath()
        absoluteMyCardsDir = self.main.absoluteMyCardsDir
        text = ''
        if absoluteFilePath[:len(absoluteMyCardsDir)] == absoluteMyCardsDir:
            text = absoluteFilePath[len(absoluteMyCardsDir):]
        text = '{0}{1}'.format(text, mustSaveText)
        actualText = self.mdDropArea.text()
        if text != actualText:
            self.mdDropArea.setText(text)
        # mise à jour des actions disponibles :
        self.markdownSave.setEnabled(mustSave)
        fileExists = (QtCore.QFileInfo(fileName).exists())
        self.markdown2Pdf.setEnabled(fileExists)

    def markdownEditorTextChanged(self):
        """
        pour prendre en compte dans le titre 
        le fait que le texte a été modifié
        """
        if self.markdownEditor.noChange:
            self.markdownEditor.noChange = False
            return
        self.updateInterface(mustSave=True)

    def markdownTestMustSave(self):
        """
        pour savoir s'il faut proposer d'enregistrer le fichier.
        """
        result = True
        if self.markdownActualFile['mustSave']:
            message = QtWidgets.QApplication.translate(
                'main',
                'The file has been modified.\n'
                'Do you want to save your changes?')
            reponseMustSave = utils_functions.messageBox(
                self, level='warning', message=message,
                buttons=['Save', 'Discard', 'Cancel'])
            if reponseMustSave == QtWidgets.QMessageBox.Cancel:
                self.mdDropArea.restoreLast()
                result = False
            elif reponseMustSave == QtWidgets.QMessageBox.Save:
                self.markdownSaveClicked()
        return result

    def markdownNewClicked(self):
        """
        création d'un nouveau fichier.
        """
        self.mdDropArea.saveLast()
        if not(self.markdownTestMustSave()):
            return
        self.markdownEditor.noChange = True
        self.markdownEditor.setPlainText('')
        self.markdownActualFile = {
            'fileName': '', 
            'mustSave': False}
        self.updateInterface()

    def markdownOpenClicked(self):
        """
        demande d'ouverture d'un fichier.
        """
        self.mdDropArea.saveLast()
        if not(self.markdownTestMustSave()):
            return
        # on commence par sélectionner le fichier :
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Open a Markdown File'), 
            self.main.myCardsDir, 
            QtWidgets.QApplication.translate(
                'main', 'Markdown Files (*.md)'))
        if isinstance(fileName, tuple):
            fileName = fileName[0]
        if len(fileName) < 1:
            return
        # on vérifie l'extension :
        extension = QtCore.QFileInfo(fileName).suffix()
        if extension.lower() == 'md':
            text = fileName
        else:
            text = self.mdDropArea.texts['bad file']
        # on vérifie aussi le chemin :
        absoluteFilePath = QtCore.QFileInfo(fileName).absoluteFilePath()
        if absoluteFilePath[:len(self.main.absoluteMyCardsDir)] != self.main.absoluteMyCardsDir:
            text = self.mdDropArea.texts['bad folder']
        elif text == fileName:
            text = absoluteFilePath[len(self.main.absoluteMyCardsDir):]
            # lecture du fichier :
            self.openMarkdown(fileName)
        self.mdDropArea.setText(text)

    def openMarkdown(self, fileName):
        """
        ouverture d'un fichier.
        """

        def readHeader(lines):
            """
            lecture de l'entête perso du fichier.
            Permet de sauvegarder divers réglages 
            dans le fichier md (title, etc)
            """
            css = ''
            otherTools = ''
            try:
                header = lines.split('-->')[0].split('<!--')[1].split('\n')
                for line in header:
                    if ('CSS:' in line):
                        css = line[4:]
                    elif ('OTHER TOOLS:' in line):
                        otherTools = line[12:]
                lines = '-->'.join(lines.split('-->')[1:])
                lines = '\n'.join(lines.split('\n')[3:])
            except:
                pass
            finally:
                return (css, otherTools, lines)

        # lecture du fichier :
        lines = utils_filesdirs.readTextFile(fileName)        
        (css, otherTools, lines) = readHeader(lines)
        otherTools = [tool for tool in otherTools.split('|') if len(tool) > 0]

        #self.cssEdit.setText(css)
        """
        for tool in self.configDict['TOOLS']['ORDER']:
            self.otherTools[tool][1].setChecked(tool in otherTools)
        """
        self.markdownEditor.noChange = True
        self.markdownEditor.setPlainText(lines)
        self.markdownActualFile = {
            'fileName': fileName, 
            'mustSave': False}
        self.updateInterface()

    def doAfterDrop(self):
        if not(self.markdownTestMustSave()):
            return
        fileName = '{0}{1}'.format(
            self.main.absoluteMyCardsDir, 
            self.mdDropArea.text())
        self.openMarkdown(fileName)

    def markdownDoSave(self, fileName):
        """
        enregistrement réel d'un fichier.
        Procédure appelée depuis saveFile ou saveFileAs.
        On insère l'entête perso au début du fichier.
        Elle permet de sauvegarder divers réglages (title, etc)
        """
        utils_functions.doWaitCursor()
        try:
            """
            if len(self.cssEdit.text()) > 0:
                cssText = 'CSS:{0}\n'.format(self.cssEdit.text())
            else:
                cssText = ''
            """
            cssText = ''
            otherTools = ''
            """
            for tool in self.configDict['TOOLS']['ORDER']:
                if self.otherTools[tool][1].isChecked():
                    otherTools = '{0}|{1}'.format(otherTools, tool)
            if len(otherTools) > 0:
                otherTools = 'OTHER TOOLS:{0}\n'.format(otherTools)
            """
            lines = (
                '<!--\n'
                '{1}{2}'
                'HEADER FOR JMS CONFIG\n'
                '-->\n'
                '\n\n'
                '{0}')
            lines = lines.format(
                self.markdownEditor.toPlainText(), 
                cssText, 
                otherTools)
            utils_filesdirs.writeTextFile(fileName, lines)
            self.updateInterface()
        finally:
            utils_functions.restoreCursor()

    def markdownSaveClicked(self):
        """
        enregistrement d'un fichier.
        """
        if len(self.markdownActualFile['fileName']) < 1:
            self.markdownSaveAsClicked()
            return
        self.markdownDoSave(self.markdownActualFile['fileName'])

    def markdownSaveAsClicked(self):
        """
        blablabla
        """
        self.mdDropArea.saveLast()
        proposedName = self.markdownActualFile['fileName']
        if len(proposedName) < 1:
            proposedName = QtWidgets.QApplication.translate(
                'main', 'No name')
            proposedName = '{0}/{1}.md'.format(
                self.main.myCardsDir, proposedName)
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Save as...'), 
            proposedName, 
            QtWidgets.QApplication.translate(
                'main', 'Markdown Files (*.md)'))
        if isinstance(fileName, tuple):
            fileName = fileName[0]
        if len(fileName) < 1:
            return
        # on met à jour markdownActualFile :
        self.markdownActualFile = {
            'fileName': fileName, 
            'mustSave': False}
        # on enregistre le fichier :
        self.markdownDoSave(fileName)

    def markdown2PdfClicked(self):
        """
        création d'un fichier PDF à partir du fichier md.
        """
        if self.markdownActualFile['mustSave']:
            self.markdownSaveClicked()
        import utils_pdf
        utils_functions.doWaitCursor()
        try:
            # d'abord on le transforme en html (dans temp) :
            htmlFileName = md2htmlPandoc(
                self.main, 
                self.markdownActualFile['fileName'])
            # ensuite on imprime en pdf (dans temp) :
            tempPdfFileName = '{0}.pdf'.format(htmlFileName)
            utils_pdf.htmlToPdf(
                self.main, htmlFileName, tempPdfFileName, orientation='Landscape')
            # enfin on recopie le fichier pdf :
            fileInfo = QtCore.QFileInfo(self.markdownActualFile['fileName'])
            pdfFileName = '{0}/{1}.pdf'.format(
                fileInfo.absolutePath(), 
                fileInfo.baseName())
            utils_filesdirs.removeAndCopy(tempPdfFileName, pdfFileName)
            # et on l'ouvre :
            #utils_filesdirs.openFile(pdfFileName)
            self.previewPdfFile(fileName=pdfFileName)
            # mise à jour de l'onglet pdf :
            self.main.pdfContainer.doPdfFile(fileName=pdfFileName)
        finally:
            utils_functions.restoreCursor()

    def previewPdfFile(self, fileName=''):
        """
        affiche le fichier pdf avec pdf.js
        https://mozilla.github.io/pdf.js/
        """
        viewerFileName = '{0}/libs/pdfjs/web/viewer.html'.format(utils.HERE)
        url = QtCore.QUrl.fromLocalFile(viewerFileName)
        query = QtCore.QUrlQuery()
        query.addQueryItem("file", "file:///{}".format(fileName))
        url.setQuery(query)
        # la suite foire parfois :
        #self.pdfViewer.load(url)
        # résolu en faisant attendre un peu 
        # après avoir affiché une page vide :
        self.pdfUrl = url
        self.pdfViewer.page().setHtml('')
        QtCore.QTimer.singleShot(500, self.loadPdfUrl)

    def loadPdfUrl(self):
        self.pdfViewer.load(self.pdfUrl)


























class MarkdownEditor(QtWidgets.QTextEdit):
    """
    un éditeur avec coloration syntaxique
    et reconnaissance des mots traduits.
    """
    def __init__(self, parent=None, readOnly=False):
        super(MarkdownEditor, self).__init__(parent)
        self.main = parent
        self.ctrl = False
        self.noChange = False
        self.readOnly = readOnly
        if readOnly:
            self.setReadOnly(True)
        self.setFontFamily('"DejaVu Sans Mono", monospace')
        self.fontPointSize = 10
        self.setFontPointSize(self.fontPointSize)
        self.highlighter = MarkdownHighlighter(self)

    def keyPressEvent(self, event):
        if not(self.readOnly):
            if event.key() == QtCore.Qt.Key_Control:
                self.ctrl = True
            elif event.key() == QtCore.Qt.Key_Tab:
                self.textCursor().insertText('    ')
                return
        QtWidgets.QTextEdit.keyPressEvent(self, event)

    def keyReleaseEvent(self, event):
        if not(self.readOnly):
            if event.key() == QtCore.Qt.Key_Control:
                self.ctrl = False
        QtWidgets.QTextEdit.keyReleaseEvent(self, event)

    def wheelEvent(self, event):
        if not(self.readOnly) and self.ctrl:
            angle = event.angleDelta().y()
            mini = 6
            maxi = 40
            if angle > 0:
                self.fontPointSize += 1
            else:
                self.fontPointSize -= 1
            if self.fontPointSize > maxi:
                self.fontPointSize = maxi
            elif self.fontPointSize < mini:
                self.fontPointSize = mini
            self.setFontPointSize(self.fontPointSize)
            # pour esquiver l'appel à main.textChanged :
            self.noChange = True
            self.setPlainText(self.toPlainText())
        else:
            QtWidgets.QTextEdit.wheelEvent(self, event)



class MarkdownHighlighter(QtGui.QSyntaxHighlighter):
    """
    
    """

    MARKDOWN_KEYS_REGEX = {
        'step': re.compile(u'(?u)^\#{1,1}( )(.*?)\#*(\n|$)'), 
        'header': re.compile(u'(?u)^\#{2,6}( )(.*?)\#*(\n|$)'), 
        'bold' : re.compile(u'(?P<delim>\*\*)(?P<text>.+)(?P=delim)'), 
        'italic': re.compile(u'(?P<delim>\*)(?P<text>[^*]{2,})(?P=delim)'), 
        'strikeOut' : re.compile(u'(?P<delim>\~\~)(?P<text>.+)(?P=delim)'), 
        'link': re.compile(u'(?u)(^|(?P<pre>[^!]))\[.*?\]:?[ \t]*\(?[^)]+\)?'), 
        'image': re.compile(u'(?u)!\[.*?\]\(.+?\)'), 
        'unorderedList': re.compile(u'(?u)^\s*(\* |\+ |- )+\s*'), 
        'unorderedListStar': re.compile(u'^\s*(\* )+\s*'), 
        'orderedList': re.compile(u'(?u)^\s*(\d+\. )\s*'), 
        'blockQuote': re.compile(u'(?u)^\s*>+\s*'), 
        'blockQuoteCount': re.compile(u'^[ \t]*>[ \t]?'), 
        'codeSpan': re.compile(u'(?P<delim>`+).+?(?P=delim)'), 
        'codeBlock': re.compile(u'^([ ]{4,}|\t).*'), 
        'line': re.compile(u'(?u)^(\s*(\*|-)\s*){3,}$'), 
        'html': re.compile(u'<.+?>'), 
        'commentBegin': QtCore.QRegExp('<!--'), 
        'commentEnd': QtCore.QRegExp('-->'), 
        'maths': re.compile(u'(?P<delim>\$)(?P<text>[^$]{2,})(?P=delim)'), 
        }

    def __init__(self, parent):
        QtGui.QSyntaxHighlighter.__init__(self, parent)
        self.parent = parent

        self.defaultColor = '#000000'
        self.defaultBackgroundColor = '#ffffff'
        pal = self.parent.palette()
        pal.setColor(
            QtGui.QPalette.Base, QtGui.QColor(self.defaultBackgroundColor))
        self.parent.setPalette(pal)
        self.parent.setTextColor(QtGui.QColor(self.defaultColor))

        self.MARKDOWN_KWS_FORMAT = {
            'step': self.applyFormat('#3bb2dc', 'bold'), 
            'header': self.applyFormat('#2aa198', 'bold'), 
            'bold': self.applyFormat('#859900', 'bold'), 
            'italic': self.applyFormat('#b58900', 'italic'), 
            'strikeOut': self.applyFormat('#b58900', 'strikeOut'), 
            'link': self.applyFormat('#cb4b16'), 
            'image': self.applyFormat('#cb4b16'), 
            'unorderedList': self.applyFormat('#dc322f'), 
            'orderedList': self.applyFormat('#dc322f'), 
            'blockQuote': self.applyFormat('#dc322f'), 
            'codeSpan': self.applyFormat('#dc322f'), 
            'codeBlock': self.applyFormat('#ff9900'), 
            'line': self.applyFormat('#2aa198'), 
            'html': self.applyFormat('#c000c0'), 
            'comment': self.applyFormat('#aaaaaa'), 
            'maths': self.applyFormat('#000055'), 
            }

        self.rehighlight()

    def applyFormat(self, color, *args):
        """
        return a QTextCharFormat with the given attributes.
        """
        _color = QtGui.QColor()
        _color.setNamedColor(color)
        _format = QtGui.QTextCharFormat()
        _format.setForeground(_color)
        if 'bold' in args:
            _format.setFontWeight(QtGui.QFont.Bold)
        if 'italic' in args:
            _format.setFontItalic(True)
        if 'strikeOut' in args:
            _format.setFontStrikeOut(True)
        return _format

    def highlightBlock(self, text):
        self.highlightMarkdown(text, 0)
        self.highlightComments(text)

    def highlightMarkdown(self, text, strt):
        cursor = QtGui.QTextCursor(self.document())
        bf = cursor.blockFormat()
        self.setFormat(0, len(text), QtGui.QColor(self.defaultColor))

        #Block quotes can contain all elements so process it first
        self.highlightBlockQuote(text, cursor, bf, strt)

        #If empty line no need to check for below elements just return
        if self.highlightEmptyLine(text, cursor, bf, strt):
            return

        if self.highlightHeader(text, cursor, bf, strt):
            return

        if self.highlightStep(text, cursor, bf, strt):
            return

        self.highlightEmphasis(text, cursor, bf, strt)
        self.highlightCodeBlock(text, cursor, bf, strt)
        
        others = (
            'unorderedList', 
            'orderedList', 
            'bold', 
            'strikeOut', 
            'image', 
            'codeSpan', 
            'link', 
            'line', 
            'maths', 
            'html')
        for what in others:
            self.doHighlight(text, cursor, bf, strt, what)

    def doHighlight(self, text, cursor, bf, strt, what):
        found = False
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX[what], text):
            self.setFormat(
                mo.start() + strt, 
                mo.end() - mo.start() - strt, 
                self.MARKDOWN_KWS_FORMAT[what])
            found = True
        return found

    def highlightBlockQuote(self, text, cursor, bf, strt):
        found = False
        mo = re.search(self.MARKDOWN_KEYS_REGEX['blockQuote'], text)
        if mo:
            self.setFormat(
                mo.start(), 
                mo.end() - mo.start(), 
                self.MARKDOWN_KWS_FORMAT['blockQuote'])
            unquote = re.sub(
                self.MARKDOWN_KEYS_REGEX['blockQuoteCount'], '', text)
            spcs = re.match(
                self.MARKDOWN_KEYS_REGEX['blockQuoteCount'], text)
            spcslen = 0
            if spcs:
                spcslen = len(spcs.group(0))
            self.highlightMarkdown(unquote, spcslen)
            found = True
        return found

    def highlightEmptyLine(self, text, cursor, bf, strt):
        textAscii = text.replace(u'\u2029', '\n')
        if textAscii.strip():
            return False
        else:
            return True

    def highlightHeader(self, text, cursor, bf, strt):
        found = False
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['header'], text):
            self.setFormat(
                mo.start() + strt, 
                mo.end() - mo.start(), 
                self.MARKDOWN_KWS_FORMAT['header'])
            found = True
        return found

    def highlightStep(self, text, cursor, bf, strt):
        found = False
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['step'], text):
            self.setFormat(
                mo.start() + strt, 
                mo.end() - mo.start(), 
                self.MARKDOWN_KWS_FORMAT['step'])
            found = True
        return found

    def highlightEmphasis(self, text, cursor, bf, strt):
        found = False
        unlist = re.sub(
            self.MARKDOWN_KEYS_REGEX['unorderedListStar'], '', text)
        spcs = re.match(
            self.MARKDOWN_KEYS_REGEX['unorderedListStar'], text)
        spcslen = 0
        if spcs:
            spcslen = len(spcs.group(0))
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['italic'], unlist):
            self.setFormat(
                mo.start() + strt + spcslen, 
                mo.end() - mo.start() - strt, 
                self.MARKDOWN_KWS_FORMAT['italic'])
            found = True
        return found

    def highlightCodeBlock(self, text, cursor, bf, strt):
        found = False
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['codeBlock'], text):
            stripped = text.lstrip()
            if stripped[0] not in ('*', '-', '+', '>'):
                self.setFormat(
                    mo.start() + strt, 
                    mo.end() - mo.start(), 
                    self.MARKDOWN_KWS_FORMAT['codeBlock'])
                found = True
        return found

    def highlightComments(self, text):

        self.setCurrentBlockState(0)
        startIndex = 0
        if self.previousBlockState() != 1:
            startIndex = self.MARKDOWN_KEYS_REGEX[
                'commentBegin'].indexIn(text)

        while startIndex >= 0:
            endIndex = self.MARKDOWN_KEYS_REGEX[
                'commentEnd'].indexIn(text, startIndex)
            if endIndex == -1:
                self.setCurrentBlockState(1)
                commentml_lg = len(text) - startIndex
            else:
                commentml_lg = endIndex-startIndex + self.MARKDOWN_KEYS_REGEX[
                    'commentEnd'].matchedLength()
            self.setFormat(
                startIndex, 
                commentml_lg, 
                self.MARKDOWN_KWS_FORMAT['comment'])
            startIndex = self.MARKDOWN_KEYS_REGEX[
                'commentBegin'].indexIn(text, startIndex+commentml_lg)

