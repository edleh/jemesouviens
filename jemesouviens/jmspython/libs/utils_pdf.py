# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    conversion d'un fichier html en pdf
    convertion d'un fichier pdf en images pour JMS
"""

# modules utiles :
import os
import glob

# modules perso :
import utils, utils_functions, utils_webengine, utils_droparea

# modules PyQt :
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5 import QtPrintSupport





"""
****************************************************
    VARIABLES GLOBALES
****************************************************
"""

# fabrication des PDF :
PDF_GENERATED = False
PRINT_WEBVIEW = None

# fabrication des images :
EXTENSIONS = ('jpeg', 'png',)
SIZES = ('640', '1024', '2048',)










"""
****************************************************
    CRÉATION DE FICHIERS PDF
****************************************************
"""

def htmlToPdf(main, htmlFileName, pdfFileName, orientation='Portrait'):
    """
    Création d'un fichier pdf. On utilise un WebEngineView.
    """
    if orientation != 'Portrait':
        orientation = 'Landscape'

    def convertItWebKit():
        global PDF_GENERATED
        PRINT_WEBVIEW.print_(printer)
        PDF_GENERATED = True

    def convertItWebEngine():
        pageLayout = QtGui.QPageLayout()
        pageLayout.setPageSize(QtGui.QPageSize(QtGui.QPageSize.A4))
        if orientation != 'Portrait':
            pageLayout.setOrientation(QtGui.QPageLayout.Landscape)
        #PRINT_WEBVIEW.show()
        PRINT_WEBVIEW.page().printToPdf(pdfFileName, pageLayout)

    def printingFinished(ok=None):
        global PDF_GENERATED
        PDF_GENERATED = True

    global PDF_GENERATED, PRINT_WEBVIEW
    PDF_GENERATED = False

    PRINT_WEBVIEW = utils_webengine.MyWebEngineView(
        main, linksInBrowser=True)
    #PRINT_WEBVIEW.setZoomFactor(1)

    if utils_webengine.WEB_ENGINE == 'WEBENGINE':
        PRINT_WEBVIEW.page().pdfPrintingFinished.connect(printingFinished)
        PRINT_WEBVIEW.loadFinished.connect(convertItWebEngine)
    else:
        printer = QtPrintSupport.QPrinter()
        printer.setPageSize(QtPrintSupport.QPrinter.A4)
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        if orientation == 'Portrait':
            printer.setOrientation(QtPrintSupport.QPrinter.Portrait)
        else:
            printer.setOrientation(QtPrintSupport.QPrinter.Landscape)
        printer.setOutputFileName(pdfFileName)
        PRINT_WEBVIEW.loadFinished.connect(convertItWebKit)

    url = QtCore.QUrl().fromLocalFile(htmlFileName)
    PRINT_WEBVIEW.load(url)

    while not(PDF_GENERATED):
        QtWidgets.QApplication.processEvents()
    del(PRINT_WEBVIEW)
    PRINT_WEBVIEW = None
    return True







"""
****************************************************
    CRÉATION D'IMAGES DEPUIS UN PDF
****************************************************
"""

def pdf2Images(main, fileName, extension='jpeg', size='1024', subDirectory=''):
    """
    Création d'images xxx à partir d'un fichier pdf
    """
    if utils.SOFTS.get('pdftoppm', '') == '':
        return False
    import utils_filesdirs
    utils_functions.doWaitCursor()
    result = False
    try:
        baseName = QtCore.QFileInfo(fileName).baseName()
        pdfFileName = QtCore.QFileInfo(fileName).fileName()
        cardsDir = QtCore.QFileInfo(fileName).absolutePath()

        process = QtCore.QProcess(main)

        # on travaille dans le dossier temporaire :
        tempFileName = '{0}/{1}'.format(main.tempPath, pdfFileName)
        utils_filesdirs.removeAndCopy(fileName, tempFileName)

        # transformation du pdf en images (une par page) :
        tempBaseName = '{0}/{1}'.format(main.tempPath, baseName)
        args = [tempFileName, tempBaseName, '-{0}'.format(extension), '-scale-to',  size]
        process.start(utils.SOFTS['pdftoppm'], args)
        if not process.waitForStarted(3000):
            QtCore.qDebug("allez, cherche le bug :o")
            return
        if not process.waitForFinished():
            return

        # récupération de la liste des fichiers images 
        # à traiter et du nombre de questions :
        if extension == 'jpeg':
            imagesFiles = glob.glob('{0}/*.jpg'.format(main.tempPath))
        elif extension == 'png':
            imagesFiles = glob.glob('{0}/*.png'.format(main.tempPath))
        nbQuestions = len(imagesFiles) // 2

        # on crée le dossier si demandé :
        if (subDirectory != ''):
            theDir = QtCore.QDir(
                '{0}/{1}/'.format(cardsDir, subDirectory))
            if not(theDir.exists()):
                first = True
                QtCore.QDir(cardsDir).mkdir(subDirectory)

        for imageFile in imagesFiles:
            utils_functions.afficheStatusBar(main, imageFile)
            # récupération du numéro du fichier, attribué par pdftoppm
            # (on enlève 1, car pdftoppm commence à 1 et pas 0) :
            imageNum = int(QtCore.QFileInfo(imageFile).baseName().split('-')[-1]) - 1

            # fabrication du nom du fichier carte (question ou réponse) :
            cardNum = imageNum // 2
            if 2 * cardNum == imageNum:
                # page paire : question
                terminaison = 'q'
            else:
                # page impaire : réponse
                terminaison = 'r'
            cardFile = '{0}-{1}-{2}.{3}'.format(
                baseName, cardNum, terminaison, extension)
            if (subDirectory != ''):
                cardFile = '{0}/{1}'.format(
                    subDirectory, cardFile)
            utils_functions.afficheStatusBar(main, cardFile)
            # on copie le fichier en le renommant et dans le bon dossier :
            cardFile = '{0}/{1}'.format(cardsDir, cardFile)
            utils_filesdirs.removeAndCopy(imageFile, cardFile)
            # suppression du fichier imageFile :
            QtCore.QFile(imageFile).remove()

        # suppression du fichier PDF temporaire :
        QtCore.QFile(tempFileName).remove()

        utils_functions.afficheMsgFin(main)
        result = True
    finally:
        utils_functions.restoreCursor()
        return result


class Pdf2ImagesDlg(QtWidgets.QDialog):
    """
    blablabla
    """
    def __init__(self, parent=None):
        super(Pdf2ImagesDlg, self).__init__(parent)
        self.main = parent
        # des modifications ont été faites :
        self.modified = False

        # fichier PDF :
        self.pdfFileName = ''
        text = QtWidgets.QApplication.translate(
            'main', 'PDF file')
        pdfFileGroup = QtWidgets.QGroupBox(text)
        helpPdfLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate(
                    'main', 
                    'Use a drag-drop or the button to select a PDF file:')))
        self.pdfDropArea = utils_droparea.DropArea(
            parent=self, 
            extension='pdf', 
            requiredDir=self.main.absoluteMyCardsDir)
        self.pdfDropArea.setMinimumHeight(50)
        selectPdfFileButton = QtWidgets.QPushButton(
            utils.doIcon('pdf'), 
            QtWidgets.QApplication.translate('main', 'Select a PDF File'))
        selectPdfFileButton.clicked.connect(self.selectPdfFile)
        # on agence tout ça :
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(helpPdfLabel)
        vLayout.addWidget(self.pdfDropArea)
        vLayout.addWidget(selectPdfFileButton)
        pdfFileGroup.setLayout(vLayout)

        # création des images :
        text = QtWidgets.QApplication.translate(
            'main', 'Cards creation')
        doImagesGroup = QtWidgets.QGroupBox(text)
        # extension des fichiers :
        extensionLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'Files Extension:')))
        self.extensionComboBox = QtWidgets.QComboBox()
        for extension in EXTENSIONS:
            self.extensionComboBox.addItem(extension)
        self.extensionComboBox.setCurrentIndex(0)        
        # dimensions :
        sizeLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'Images size:')))
        self.sizeComboBox = QtWidgets.QComboBox()
        for size in SIZES:
            self.sizeComboBox.addItem(size)
        self.sizeComboBox.setCurrentIndex(1)
        self.sizeComboBox.setEditable(True)
        # sous-dossier :
        subDirGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'subdirectory'))
        self.createSubDirCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'Create a subdirectory'))
        self.createSubDirCheckBox.setChecked(True)
        self.createSubDirCheckBox.stateChanged.connect(self.doChanged)
        self.subDirNameLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'Subdirectory name:')))
        self.subDirNameEdit = QtWidgets.QLineEdit()
        self.subDirNameEdit.setText('')
        # le bouton de création :
        self.createImagesButton = QtWidgets.QPushButton(
            utils.doIcon('pdf'), 
            QtWidgets.QApplication.translate('main', 'Create cards'))
        self.createImagesButton.clicked.connect(self.createImages)
        # on agence tout ça :
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(extensionLabel)
        vLayout.addWidget(self.extensionComboBox)
        vLayout.addWidget(sizeLabel)
        vLayout.addWidget(self.sizeComboBox)
        vLayout.addWidget(self.createSubDirCheckBox)
        vLayout.addWidget(self.subDirNameLabel)
        vLayout.addWidget(self.subDirNameEdit)
        vLayout.addWidget(self.createImagesButton)
        doImagesGroup.setLayout(vLayout)
        doImagesGroup.setMaximumWidth(400)

        # on agence tout ça :
        vBoxLeft = QtWidgets.QVBoxLayout()
        vBoxLeft.addWidget(pdfFileGroup)
        vBoxLeft.addStretch(1)
        vBoxRight = QtWidgets.QVBoxLayout()
        vBoxRight.addWidget(doImagesGroup)
        vBoxRight.addStretch(1)
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addLayout(vBoxLeft)
        mainLayout.addLayout(vBoxRight)

        # mise en place finale :
        self.setLayout(mainLayout)
        self.updateInterface()

    def showEvent(self, event):
        self.pdfDropArea.setFocus()
        QtWidgets.QDialog.showEvent(self, event)

    def doPdfFile(self, fileName=''):
        """
        blablabla
        """
        if len(fileName) < 1:
            return
        self.pdfFileName = ''
        # on vérifie l'extension :
        extension = QtCore.QFileInfo(fileName).suffix()
        if extension.lower() == 'pdf':
            text = fileName
        else:
            text = self.pdfDropArea.texts['bad file']
        # on vérifie aussi le chemin :
        absoluteFilePath = QtCore.QFileInfo(fileName).absoluteFilePath()
        if absoluteFilePath[:len(self.main.absoluteMyCardsDir)] != self.main.absoluteMyCardsDir:
            text = self.pdfDropArea.texts['bad folder']
        elif text == fileName:
            text = absoluteFilePath[len(self.main.absoluteMyCardsDir):]
            self.pdfFileName = fileName
        self.updateInterface()
        self.pdfDropArea.setText(text)

    def selectPdfFile(self):
        """
        blablabla
        """
        # on commence par sélectionner le fichier :
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Open a PDF File'), 
            self.main.myCardsDir, 
            QtWidgets.QApplication.translate(
                'main', 'PDF Files (*.pdf)'))
        if isinstance(fileName, tuple):
            fileName = fileName[0]
        if len(fileName) < 1:
            return
        self.doPdfFile(fileName=fileName)

    def updateInterface(self, fromDrop=False):
        """
        blablabla
        """
        if self.pdfFileName == '':
            self.createImagesButton.setEnabled(False)
            if not(fromDrop):
                self.pdfDropArea.setText(
                    self.pdfDropArea.texts['nothing'])
            self.subDirNameEdit.setText('')
        else:
            self.createImagesButton.setEnabled(True)
            if not(fromDrop):
                absoluteFilePath = QtCore.QFileInfo(
                    self.pdfFileName).absoluteFilePath()
                text = absoluteFilePath[len(self.main.absoluteMyCardsDir):]
                self.pdfDropArea.setText(text)
            baseName = QtCore.QFileInfo(self.pdfFileName).baseName()
            self.subDirNameEdit.setText(baseName)

    def doChanged(self, value):
        isChecked = (value == QtCore.Qt.Checked)
        self.subDirNameLabel.setEnabled(isChecked)
        self.subDirNameEdit.setEnabled(isChecked)

    def doAfterDrop(self):
        fileName = '{0}{1}'.format(
            self.main.absoluteMyCardsDir, 
            self.pdfDropArea.text())
        if QtCore.QFileInfo(fileName).exists():
            self.pdfFileName = fileName
        else:
            self.pdfFileName = ''
        self.updateInterface(fromDrop=True)

    def createImages(self):
        """
        blablabla
        """
        if self.createSubDirCheckBox.isChecked():
            subDirectory = self.subDirNameEdit.text()
        else:
            subDirectory = ''
        # on lance la création des images :
        pdf2Images(
            self.main, 
            self.pdfFileName, 
            extension=self.extensionComboBox.currentText(), 
            size=self.sizeComboBox.currentText(), 
            subDirectory=subDirectory)
        # on met à jour le fichier cards.js :
        self.main.createCardsJs()


