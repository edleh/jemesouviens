# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    détection des logiciels tiers (dépendances)
"""

# modules utiles :
import os

# modules perso :
import utils






"""
****************************************************
    DÉTECTION DES LOGICIELS TIERS
****************************************************
"""

def testSofts():
    """
    D'une part on teste si les outils sont installés, 
    on récupère les chemins et on les ajoute au path.
    """
    SOFTS = {
        'pdftoppm': '', 
        'pandoc': '', 
        }

    message = 'test softs:\n'
    if utils.OS_NAME[0] == 'win':
        # décompression du dossier win si besoin :
        winLibsDir = os.path.join(utils.HERE, 'libs', 'win')
        if not(os.path.exists(winLibsDir)):
            tarFile = os.path.join(utils.HERE, 'libs', 'win.tar.gz')
            destDir = os.path.join(utils.HERE, 'libs')
            import utils_filesdirs
            if utils_filesdirs.untarDirectory(tarFile, destDir):
                print('THE WIN FOLDER IS UNCOMPRESSED')
        # recherche des outils :
        for softName in SOFTS:
            softBin = '{0}.exe'.format(softName)
            fileName = os.path.join(utils.HERE, 'libs', 'win', softName, softBin)
            if (fileName != '') and (os.path.exists(fileName)):
                SOFTS[softName] = fileName
                message = '{0} * {1} FOUND ({2})\n'.format(message, softName, fileName)
            else:
                message = '{0} * {1} NOT FOUND\n'.format(message, softName)
    else:
        for softName in SOFTS:
            # chemins possibles :
            possiblesPaths = ('/usr/local/bin', '/opt', '/usr/bin', )
            # on cherche où est le binaire :
            for possiblePath in possiblesPaths:
                fileName = os.path.join(possiblePath, softName)
                if os.path.exists(fileName):
                    SOFTS[softName] = fileName
                    break
            if SOFTS[softName] != '':
                message = '{0} * {1} FOUND ({2})\n'.format(message, softName, SOFTS[softName])
            else:
                message = '{0} * {1} NOT FOUND\n'.format(message, softName)

    utils.changeSofts(SOFTS)
    return message



