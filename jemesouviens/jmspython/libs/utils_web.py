# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
Module contenant tout ce qui permet de manipuler le web :
tester la connexion internet, downloader, ...
"""

# modules utiles :
import json
import random

# modules perso :
import utils, utils_functions, utils_filesdirs

# modules PyQt :
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5 import QtNetwork





def doEndMessage(text=''):
    endMessage = QtWidgets.QApplication.translate('main', 'END !')
    return '{0}<p align="center"><br /><b>{1}</b></p><p></p>'.format(
        text, endMessage)




def translateNumber(n, what='files'):
    if what == 'cards':
        if n < 1:
            result = QtWidgets.QApplication.translate('main', 'no card')
        elif n < 2:
            result = '1 {0}'.format(
                QtWidgets.QApplication.translate('main', 'card'))
        else:
            result = '{0} {1}'.format(
                n, QtWidgets.QApplication.translate('main', 'cards'))
    else:
        if n < 1:
            result = QtWidgets.QApplication.translate('main', 'no file')
        elif n < 2:
            result = '1 {0}'.format(
                QtWidgets.QApplication.translate('main', 'file'))
        else:
            result = '{0} {1}'.format(
                n, QtWidgets.QApplication.translate('main', 'files'))
    return result





class ChangeMdpDlg(QtWidgets.QDialog):
    """
    dialog de modification de mot de passe
    """
    def __init__(self, parent=None):
        super(ChangeMdpDlg, self).__init__(parent)
        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate(
                'main', 'Change your password'))
        # les champs pour mdp et confirmation :
        passwordLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'New password:')))
        self.passwordEdit = QtWidgets.QLineEdit()
        self.passwordEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        passwordConfirmLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'Confirm:')))
        self.passwordConfirmEdit = QtWidgets.QLineEdit()
        self.passwordConfirmEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        # les boutons :
        buttonBox = QtWidgets.QDialogButtonBox()
        # le bouton OK :
        okButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-ok-apply'), 
            QtWidgets.QApplication.translate('main', 'Ok'))
        okButton.clicked.connect(self.accept)
        buttonBox.addButton(
            okButton, QtWidgets.QDialogButtonBox.ActionRole)
        # le bouton annuler :
        cancelButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-cancel'), 
            QtWidgets.QApplication.translate('main', 'Cancel'))
        cancelButton.clicked.connect(self.close)
        buttonBox.addButton(
            cancelButton, QtWidgets.QDialogButtonBox.ActionRole)
        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(passwordLabel, 1, 0)
        grid.addWidget(self.passwordEdit, 1, 1)
        grid.addWidget(passwordConfirmLabel, 2, 0)
        grid.addWidget(self.passwordConfirmEdit, 2, 1)
        grid.addWidget(buttonBox, 9, 0, 1, 2)
        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

    def accept(self):
        if self.passwordEdit.text() == '':
            message = QtWidgets.QApplication.translate(
                'main', 'The password is empty.')
            utils_functions.messageBox(
                self.main, level='warning', message=message)
            return
        elif self.passwordConfirmEdit.text() != self.passwordEdit.text():
            message = QtWidgets.QApplication.translate(
                'main', 'The two entries are different.')
            utils_functions.messageBox(
                self.main, level='warning', message=message)
            return
        QtWidgets.QDialog.accept(self)


class UploadCardsDlg(QtWidgets.QDialog):
    """
    blablabla
    """
    def __init__(self, parent=None):
        super(UploadCardsDlg, self).__init__(parent)
        self.main = parent

        # réglages du compte :
        text = QtWidgets.QApplication.translate(
            'main', 'Settings')
        settingsGroup = QtWidgets.QGroupBox(text)
        # adresse web :
        webUrlLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'url:')))
        self.webUrlEdit = QtWidgets.QLineEdit()
        self.webUrlValidator = QtWidgets.QLabel()
        # user :
        userLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'user:')))
        self.userEdit = QtWidgets.QLineEdit()
        self.userValidator = QtWidgets.QLabel()
        # password :
        passwordLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'password:')))
        passwordView = QtWidgets.QToolButton()
        passwordView.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.actionShowPassword = QtWidgets.QAction(
            '', self, icon=utils.doIcon('view'))
        self.actionShowPassword.setCheckable(True)
        self.actionShowPassword.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Show/Hide password'))
        passwordView.setDefaultAction(self.actionShowPassword)
        self.passwordEdit = QtWidgets.QLineEdit()
        self.passwordEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.passwordValidator = QtWidgets.QLabel()
        # webSubDirs :
        webSubDirsLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'remote subfolder:')))
        self.webSubDirsValidator = QtWidgets.QLabel()
        self.webSubDirsComboBox = QtWidgets.QComboBox()
        # bouton de test :
        self.testSettingsButton = QtWidgets.QPushButton(
            utils.doIcon('settings-preview'), 
            QtWidgets.QApplication.translate('main', 'Test configuration'))
        # bouton de changement de mot de passe :
        self.passwordButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-password'), 
            QtWidgets.QApplication.translate('main', 'Change your password'))
        validators = (
            self.webUrlValidator, 
            self.userValidator, 
            self.passwordValidator, 
            self.webSubDirsValidator, 
            )
        self.buttonSize = 18
        for validator in validators:
            validator.setMaximumSize(
                self.buttonSize, self.buttonSize)
            validator.setAlignment(
                QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        # on agence tout ça :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(webUrlLabel, 0, 0)
        grid.addWidget(self.webUrlEdit, 0, 2)
        grid.addWidget(self.webUrlValidator, 0, 3)
        grid.addWidget(userLabel, 2, 0)
        grid.addWidget(self.userEdit, 2, 2)
        grid.addWidget(self.userValidator, 2, 3)
        grid.addWidget(passwordLabel, 3, 0)
        grid.addWidget(passwordView, 3, 1)
        grid.addWidget(self.passwordEdit, 3, 2)
        grid.addWidget(self.passwordValidator, 3, 3)
        grid.addWidget(webSubDirsLabel, 4, 0)
        grid.addWidget(self.webSubDirsComboBox, 4, 2)
        grid.addWidget(self.webSubDirsValidator, 4, 3)
        grid.addWidget(self.testSettingsButton, 8, 0, 1, 4)
        grid.addWidget(self.passwordButton, 9, 0, 1, 4)
        settingsGroup.setLayout(grid)
        settingsGroup.setMaximumWidth(500)

        # envoi des fichiers :
        text = QtWidgets.QApplication.translate(
            'main', 'File management')
        fileManagementGroup = QtWidgets.QGroupBox(text)
        # bouton de téléchargement :
        self.downloadButton = QtWidgets.QPushButton(
            utils.doIcon('download'), 
            QtWidgets.QApplication.translate('main', 'Download cards of the site'))
        # boutons de synchronisation du dossier perso :
        myFolderLabel = QtWidgets.QLabel(
            '<p><b>{0}</b></p>'.format(
                QtWidgets.QApplication.translate('main', 'Management of my folder:')))
        self.compareButton = QtWidgets.QPushButton(
            utils.doIcon('folder-check'), '', 
            toolTip=QtWidgets.QApplication.translate('main', 'Compare'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 
                'Compare local and remote versions of the selected subfolder.'))
        self.synchronizeButton = QtWidgets.QPushButton(
            utils.doIcon('upload'), '', 
            toolTip=QtWidgets.QApplication.translate('main', 'Synchronize'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 
                'Synchronization of the selected sub-folder; '
                'files present only on the site will be ignored.'))
        self.onlySiteDownloadButton = QtWidgets.QPushButton(
            utils.doIcon('download'), '', 
            toolTip=QtWidgets.QApplication.translate(
                'main', 'Download files from the site'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 
                'Download the files present only on the site.'))
        self.onlySiteDeleteButton = QtWidgets.QPushButton(
            utils.doIcon('clear'), '', 
            toolTip=QtWidgets.QApplication.translate(
                'main', 'Delete files from the site'), 
            statusTip=QtWidgets.QApplication.translate(
                'main', 
                'Delete files present only on the site.'))
        iconSize = int(utils.STYLE['PM_ToolBarIconSize'] * 1.5)
        tools = (
            self.compareButton, 
            self.synchronizeButton, 
            self.onlySiteDownloadButton, 
            self.onlySiteDeleteButton)
        for tool in tools:
            tool.setIconSize(QtCore.QSize(iconSize, iconSize))

        # consoleTextEdit :
        self.consoleTextEdit = QtWidgets.QTextEdit()
        self.consoleTextEdit.setReadOnly(True)
        # on agence tout ça :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(self.compareButton)
        hLayout.addWidget(self.synchronizeButton)
        hLayout.addStretch()
        hLayout.addWidget(self.onlySiteDownloadButton)
        hLayout.addWidget(self.onlySiteDeleteButton)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(self.downloadButton)
        vLayout.addWidget(myFolderLabel)
        vLayout.addLayout(hLayout)
        vLayout.addWidget(self.consoleTextEdit)
        fileManagementGroup.setLayout(vLayout)

        # mise en place finale :
        vBoxLeft = QtWidgets.QVBoxLayout()
        vBoxLeft.addWidget(settingsGroup)
        vBoxLeft.addStretch(1)
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addLayout(vBoxLeft)
        mainLayout.addWidget(fileManagementGroup)
        self.setLayout(mainLayout)

        # fin de l'initialisation :
        self.clearSynchro()
        self.actionShowPassword.triggered.connect(self.showPassword)
        self.testSettingsButton.clicked.connect(self.testSettings)
        self.passwordButton.clicked.connect(self.changePassword)
        self.webSubDirsComboBox.currentIndexChanged.connect(self.webSubDirsComboBoxChanged)
        self.downloadButton.clicked.connect(self.downloadCards)
        self.compareButton.clicked.connect(self.compareFolder)
        self.synchronizeButton.clicked.connect(self.synchronizeFolder)
        self.onlySiteDownloadButton.clicked.connect(self.onlySiteDownload)
        self.onlySiteDeleteButton.clicked.connect(self.onlySiteDelete)
        self.webUrlEdit.setText(self.main.configDict['CONFIG']['webUrl'])
        self.userEdit.setText(self.main.configDict['CONFIG']['user'])
        self.passwordEdit.setText(self.main.configDict['CONFIG']['password'])
        self.testSettings()


    def showEvent(self, event):
        self.consoleTextEdit.setFocus()
        QtWidgets.QDialog.showEvent(self, event)


    def showPassword(self):
        if self.actionShowPassword.isChecked():
            self.passwordEdit.setEchoMode(QtWidgets.QLineEdit.Normal)
        else:
            self.passwordEdit.setEchoMode(QtWidgets.QLineEdit.Password)


    def clearSynchro(self):
        self.synchro = {
            'SITE_ONLY': [], 
            'LOCAL_ONLY': [], 
            'IDEM': [], 
            'SITE_LAST': [], 
            'LOCAL_LAST': [], 
            }


    def webSubDirsComboBoxChanged(self, index):
        self.clearSynchro()


    def testSettings(self):
        if self.sender() == self.testSettingsButton:
            self.main.configDict['CONFIG']['webUrl'] = self.webUrlEdit.text()
            self.main.configDict['CONFIG']['user'] = self.userEdit.text()
            self.main.configDict['CONFIG']['password'] = self.passwordEdit.text()
            self.main.configDict['CONFIG']['webSubDirs'] = ''
            self.main.configDict['CONFIG']['userId'] = -1

        httpClient = HttpClient(self.main)

        # test du site :
        siteOK = False
        theUrl = '{0}/CARDS/_tools/test_site.php'.format(
            self.main.configDict['CONFIG']['webUrl'])
        testNetDelay = 5000
        httpClient.launch(theUrl)
        while httpClient.state == STATE_LAUNCHED:
            dureeTotale = httpClient.timeDepart.msecsTo(
                QtCore.QTime.currentTime())
            if dureeTotale > testNetDelay:
                httpClient.cancel()
            QtWidgets.QApplication.processEvents()
        if httpClient.state == STATE_OK:
            try:
                response = httpClient.reponse.split('|')[1]
                siteOK = (response == 'OK')
            except:
                pass
        self.setOk(self.webUrlValidator, ok=siteOK)

        # test de l'utilisateur :
        userOk = otherOk = False
        if siteOK:
            theUrl = '{0}/CARDS/_tools/login.php'.format(
                self.main.configDict['CONFIG']['webUrl'])
            userId = -1
            postData = 'user={0}&password={1}'.format(
                self.main.configDict['CONFIG']['user'], 
                self.main.configDict['CONFIG']['password'])
            httpClient.launch(theUrl, postData)
            while httpClient.state == STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if httpClient.state == STATE_OK:
                try:
                    response = httpClient.reponse.split('||')
                    userId = utils_functions.verifyLibs_toInt(response[1])
                    self.main.configDict['CONFIG']['webSubDirs'] = response[2]
                except:
                    print('except in testSettings')
                if userId > -1:
                    self.main.configDict['CONFIG']['userId'] = userId
                    userOk = True
                    otherOk = True
                elif userId == -2:
                    userOk = True

        # mise à jour de l'interface :
        self.setOk(self.userValidator, ok=userOk)
        self.setOk(self.passwordValidator, ok=otherOk)
        self.setOk(self.webSubDirsValidator, ok=otherOk)
        webSubDirs = self.main.configDict['CONFIG']['webSubDirs'].split('|')
        self.webSubDirsComboBox.clear()
        for webSubDir in webSubDirs:
            if len(webSubDir) > 0:
                utils_filesdirs.createDirs(
                    self.main.absoluteMyCardsDir, webSubDir)
                self.webSubDirsComboBox.addItem(webSubDir)
        self.webSubDirsComboBox.setCurrentIndex(0)
        self.downloadButton.setEnabled(siteOK)
        controls = (
            self.passwordButton, 
            self.compareButton, 
            self.synchronizeButton, 
            self.onlySiteDownloadButton, 
            self.onlySiteDeleteButton)
        for what in controls:
            what.setEnabled(otherOk)


    def setOk(self, validator, ok=True):
        if ok:
            iconFileName = 'dialog-ok-apply'
        else:
            iconFileName = 'dialog-warning'
        validator.setPixmap(
            utils.doIcon(iconFileName, what='PIXMAP').scaled(
                self.buttonSize, self.buttonSize,
                QtCore.Qt.KeepAspectRatio,
                QtCore.Qt.SmoothTransformation))


    def changePassword(self):
        """
        changement de mot de passe
        """
        # affichage du dialog pour saisir le nouveau mot de passe :
        dialog = ChangeMdpDlg(parent=self.main)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        newPassword = dialog.passwordEdit.text()
        netPasswordChanged = False

        utils_functions.doWaitCursor()
        try:
            # envoi de la demande à la page change_password.php :
            httpClient = HttpClient(self.main)
            theUrl = '{0}/CARDS/_tools/change_password.php'.format(
                self.main.configDict['CONFIG']['webUrl'])
            userId = -1
            postData = 'user={0}&password={1}&new_password={2}'.format(
                self.main.configDict['CONFIG']['user'], 
                self.main.configDict['CONFIG']['password'], 
                newPassword)
            httpClient.launch(theUrl, postData)
            while httpClient.state == STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if httpClient.state == STATE_OK:
                try:
                    response = httpClient.reponse.split('|')[1]
                    netPasswordChanged = (response == 'OK')
                except:
                    pass
            # modification de self.passwordEdit.text() :
            if netPasswordChanged:
                self.passwordEdit.setText(newPassword)
                self.main.configDict['CONFIG']['password'] = newPassword
        finally:
            utils_functions.restoreCursor()
            # message final en fonction de la réponse :
            if netPasswordChanged:                
                message = QtWidgets.QApplication.translate(
                    'main', 'The password was changed successfully.')
            else:
                message = QtWidgets.QApplication.translate(
                    'main', 'The password change failed.')
            utils_functions.messageBox(self.main, message=message)


    def updateConsoleText(self, text=''):
        self.consoleTextEdit.setHtml(text)
        self.consoleTextEdit.moveCursor(QtGui.QTextCursor.End)


    def downloadCards(self):
        utils_functions.doWaitCursor()
        try:
            self.consoleTextEdit.clear()
            consoleText = ''

            # partie 1.
            # Récupération de la liste des cartes du site :
            # (avec leur taille pour comparer)
            consoleText = '{0}<p><b>{1}</b>'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 'Retrieve the list of site cards:'))
            self.updateConsoleText(consoleText)
            siteCardsList = []
            siteJson = {'CARDS': [], }
            downLoader = DownLoader(self.main)
            theUrl = '{0}/CARDS/_tools/list_cards.php'.format(
                self.main.configDict['CONFIG']['webUrl'])
            testNetDelay = 5000
            httpClient = HttpClient(self.main)
            httpClient.launch(theUrl)
            while httpClient.state == STATE_LAUNCHED:
                dureeTotale = httpClient.timeDepart.msecsTo(
                    QtCore.QTime.currentTime())
                if dureeTotale > testNetDelay:
                    httpClient.cancel()
                QtWidgets.QApplication.processEvents()
            if httpClient.state == STATE_OK:
                try:
                    if not(QtCore.QFileInfo('{0}/down'.format(self.main.tempPath)).exists()):
                        utils_filesdirs.createDirs(self.main.tempPath, 'down')
                    destDir = '{0}/down/'.format(self.main.tempPath)
                    theUrl = '{0}/CARDS/_tools/'.format(
                        self.main.configDict['CONFIG']['webUrl'])
                    downLoader.launch(fileName='cards.json', localDir=destDir, netDir=theUrl)
                    while downLoader.state == STATE_LAUNCHED:
                        QtWidgets.QApplication.processEvents()
                    if downLoader.state == STATE_OK:
                        siteJsonFile = '{0}/down/cards.json'.format(self.main.tempPath)
                        try:
                            theFile = open(siteJsonFile, newline='', encoding='utf-8')
                            siteJson = json.load(theFile)
                            theFile.close()
                        except:
                            siteJson = {'CARDS': [], }
                except:
                    print('except in downloadCards')
            #print(siteJson)
            for card in siteJson['CARDS']:
                cardText = '{0}/{1} | {2} | {3} | {4}'.format(card[0], card[1], card[2], card[3], card[4])
                siteCardsList.append(cardText)
            siteCardsList.sort()
            # affichage du résultat :
            #"""
            for cardText in siteCardsList[:10]:
                consoleText = '{0}<br />{1}'.format(consoleText, cardText)
            self.updateConsoleText(consoleText)
            #"""
            consoleText = '{0}<br />{1}.</p><p></p>'.format(
                consoleText, 
                translateNumber(len(siteCardsList), what='cards'))
            self.updateConsoleText(consoleText)

            # partie 2.
            # Récupération de la liste des cartes locales :
            # (avec leur taille pour comparer)
            consoleText = '{0}<p><b>{1}</b>'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 'Retrieve the list of local cards:'))
            self.updateConsoleText(consoleText)
            localCardsList = []
            cardsDirLength = len(self.main.absoluteCardsDir)
            myCardsDirLength = len(self.main.absoluteMyCardsDir)
            dirIterator = QtCore.QDirIterator(
                self.main.cardsDir, 
                QtCore.QDir.Files, 
                QtCore.QDirIterator.Subdirectories)
            while dirIterator.hasNext():
                fileName = dirIterator.next()
                fileInfo = QtCore.QFileInfo(fileName)
                absolutePath = fileInfo.absolutePath()
                if absolutePath[:myCardsDirLength] != self.main.absoluteMyCardsDir:
                    baseName = fileInfo.baseName()
                    extension = fileInfo.suffix()
                    if baseName[-2:] == '-q':
                        questionSize = fileInfo.size()                        
                        response = '{0}/{1}-r.{2}'.format(
                            absolutePath, baseName[:-2], extension)
                        if QtCore.QFileInfo(response).exists():
                            responseSize = QtCore.QFileInfo(response).size()
                            cardText = '{0}/{1} | {2} | {3} | {4}'.format(
                                absolutePath[cardsDirLength:], 
                                baseName[:-2], 
                                '.{0}'.format(extension), 
                                questionSize, 
                                responseSize)
                            localCardsList.append(cardText)
            localCardsList.sort()
            # affichage du résultat :
            #"""
            for cardText in localCardsList[:10]:
                consoleText = '{0}<br />{1}'.format(consoleText, cardText)
            self.updateConsoleText(consoleText)
            #"""
            consoleText = '{0}<br />{1}.</p><p></p>'.format(
                consoleText, 
                translateNumber(len(localCardsList), what='cards'))
            self.updateConsoleText(consoleText)

            # partie 3.
            # Suppression des cartes obsolètes :
            consoleText = '{0}<p><b>{1}</b>'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 'Removal of obsolete cards:'))
            self.updateConsoleText(consoleText)
            cardsToDelete = [cardText for cardText in localCardsList if not(cardText in siteCardsList)]
            for cardText in cardsToDelete:
                card = cardText.split(' | ')
                qFileName = '{0}-q{1}'.format(card[0], card[1])
                QtCore.QFile('{0}/{1}'.format(
                    self.main.absoluteCardsDir, qFileName)).remove()
                rFileName = '{0}-r{1}'.format(card[0], card[1])
                QtCore.QFile('{0}/{1}'.format(
                    self.main.absoluteCardsDir, rFileName)).remove()
                consoleText = '{0}<br />{1}<br />{2}'.format(
                    consoleText, 
                    qFileName, 
                    rFileName)
                self.updateConsoleText(consoleText)
            consoleText = '{0}<br />{1}.</p><p></p>'.format(
                consoleText, 
                translateNumber(2 * len(cardsToDelete)))
            self.updateConsoleText(consoleText)

            # partie 4.
            # Téléchargement des nouvelles cartes :
            consoleText = '{0}<p><b>{1}</b>'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 'Download new cards:'))
            self.updateConsoleText(consoleText)
            cardsToDownload = [cardText for cardText in siteCardsList if not(cardText in localCardsList)]
            theUrlBegin = '{0}/CARDS/'.format(
                self.main.configDict['CONFIG']['webUrl'])
            for cardText in cardsToDownload:
                card = cardText.split(' | ')
                qFileName = '{0}-q{1}'.format(card[0], card[1])
                fileInfo = QtCore.QFileInfo(qFileName)
                filePath = fileInfo.path()
                fileName = fileInfo.fileName()
                utils_filesdirs.createDirs(
                    self.main.absoluteCardsDir, filePath)
                destDir = '{0}{1}/'.format(self.main.absoluteCardsDir, filePath)
                theUrl = '{0}{1}/'.format(theUrlBegin, filePath)
                downLoader.launch(fileName=fileName, localDir=destDir, netDir=theUrl)
                while downLoader.state == STATE_LAUNCHED:
                    QtWidgets.QApplication.processEvents()
                rFileName = '{0}-r{1}'.format(card[0], card[1])
                fileName = QtCore.QFileInfo(rFileName).fileName()
                downLoader.launch(fileName=fileName, localDir=destDir, netDir=theUrl)
                while downLoader.state == STATE_LAUNCHED:
                    QtWidgets.QApplication.processEvents()
                consoleText = '{0}<br />{1}<br />{2}'.format(
                    consoleText, 
                    qFileName, 
                    rFileName)
                self.updateConsoleText(consoleText)
            consoleText = '{0}<br />{1}.</p><p></p>'.format(
                consoleText, 
                translateNumber(2 * len(cardsToDownload)))
            self.updateConsoleText(consoleText)

        finally:
            utils_functions.restoreCursor()


    def compareFolder(self):
        utils_functions.doWaitCursor()
        try:
            webSubDir = self.webSubDirsComboBox.currentText()
            localSubdir = '{0}{1}'.format(
                self.main.absoluteMyCardsDir, webSubDir)
            self.consoleTextEdit.clear()
            consoleText = ''

            # partie 1.
            # Récupération de la liste des fichiers du site :
            # (avec leur taille et leur date pour comparer)
            siteFiles = {'NAMES': [], 'DATA': {}}
            siteJson = {'FILES': [], }
            theUrl = '{0}/CARDS/_tools/list_files.php'.format(
                self.main.configDict['CONFIG']['webUrl'])
            postData = 'path={0}'.format(webSubDir[1:])
            httpClient = HttpClient(self.main)
            httpClient.launch(theUrl, postData)
            while httpClient.state == STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if httpClient.state == STATE_OK:
                try:
                    response = httpClient.reponse
                    #print('response:', response)
                    if not(QtCore.QFileInfo('{0}/down'.format(self.main.tempPath)).exists()):
                        utils_filesdirs.createDirs(self.main.tempPath, 'down')
                    localDir='{0}/down/'.format(self.main.tempPath)
                    netDir = '{0}/CARDS/_tools/'.format(
                        self.main.configDict['CONFIG']['webUrl'])
                    downLoader = DownLoader(
                        self.main, 
                        fileName='files.json', 
                        localDir=localDir, 
                        netDir=netDir)
                    downLoader.launch()
                    while downLoader.state == STATE_LAUNCHED:
                        QtWidgets.QApplication.processEvents()
                    if downLoader.state == STATE_OK:
                        siteJsonFile = '{0}/down/files.json'.format(self.main.tempPath)
                        try:
                            theFile = open(siteJsonFile, newline='', encoding='utf-8')
                            siteJson = json.load(theFile)
                            theFile.close()
                        except:
                            siteJson = {'FILES': [], }
                except:
                    print('except in downloadCards')
            #print(siteJson)
            for aFile in siteJson['FILES']:
                fileName = '{0}{1}'.format(aFile[0], aFile[1])
                siteFiles['NAMES'].append(fileName)
                siteFiles['DATA'][fileName] = '{0} | {1}'.format(
                    aFile[2], aFile[3])
            siteFiles['NAMES'].sort()
            # affichage du résultat :
            consoleText = '{0}<p><b>{1}</b> {2}'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 'Retrieve the list of site files:'), 
                translateNumber(len(siteFiles['NAMES'])))
            self.updateConsoleText(consoleText)
            for fileName in siteFiles['NAMES']:
                consoleText = '{0}<br />{1} {2}'.format(
                    consoleText, 
                    fileName, 
                    siteFiles['DATA'][fileName])
            consoleText = '{0}</p><p></p>'.format(consoleText)
            self.updateConsoleText(consoleText)

            # partie 2.
            # Récupération de la liste des fichiers locaux :
            # (avec leur taille et leur date pour comparer)
            localFiles = {'NAMES': [], 'DATA': {}}
            myCardsDirLength = len(self.main.absoluteMyCardsDir)
            dirIterator = QtCore.QDirIterator(
                localSubdir, 
                QtCore.QDir.Files, 
                QtCore.QDirIterator.Subdirectories)
            while dirIterator.hasNext():
                fileInfo = QtCore.QFileInfo(dirIterator.next())
                #lastModified = fileInfo.lastModified().toString('yyyyMMddHHmmss')
                #lastModified = fileInfo.lastModified().toString('yyyyMMddHHmm')
                lastModified = fileInfo.lastModified().toUTC().toString('yyyyMMddHHmm')
                fileName = '{0}'.format(
                    fileInfo.absoluteFilePath()[myCardsDirLength:])
                localFiles['NAMES'].append(fileName)
                localFiles['DATA'][fileName] = '{0} | {1}'.format(
                    fileInfo.size(), lastModified)
            localFiles['NAMES'].sort()
            # affichage du résultat :
            consoleText = '{0}<p><b>{1}</b> {2}'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 'Retrieve the list of local files:'), 
                translateNumber(len(localFiles['NAMES'])))
            self.updateConsoleText(consoleText)
            for fileName in localFiles['NAMES']:
                consoleText = '{0}<br />{1} {2}'.format(
                    consoleText, 
                    fileName, 
                    localFiles['DATA'][fileName])
            consoleText = '{0}</p><p></p>'.format(consoleText)
            self.updateConsoleText(consoleText)

            # partie 3.
            # Fichiers présents que d'un côté ou différents :
            self.clearSynchro()
            both = []
            self.synchro['SITE_ONLY'] = [fileName for fileName in siteFiles['NAMES'] if not(fileName in localFiles['NAMES'])]
            self.synchro['LOCAL_ONLY'] = [fileName for fileName in localFiles['NAMES'] if not(fileName in siteFiles['NAMES'])]
            temp = [fileName for fileName in localFiles['NAMES'] if (fileName in siteFiles['NAMES'])]
            for fileName in siteFiles['NAMES']:
                if fileName in localFiles['NAMES']:
                    if not(fileName in temp):
                        temp.append(fileName)
            temp.sort()
            for fileName in temp:
                siteData = siteFiles['DATA'][fileName]
                localData = localFiles['DATA'][fileName]
                if (localData != siteData):
                    siteDatas = siteData.split(' | ')
                    localDatas = localData.split(' | ')
                    if (localDatas[0] != siteDatas[0]):
                        if int(localDatas[1]) < int(siteDatas[1]):
                            self.synchro['SITE_LAST'].append((fileName, siteData, localData))
                        else:
                            self.synchro['LOCAL_LAST'].append((fileName, siteData, localData))
                    else:
                        self.synchro['IDEM'].append(fileName)
                else:
                    self.synchro['IDEM'].append(fileName)
            # affichage des résultats :
            titles = {
                'IDEM': QtWidgets.QApplication.translate('main', 'IDENTICAL FILES:'), 
                'SITE_ONLY': QtWidgets.QApplication.translate('main', 'ONLY ON THE SITE:'), 
                'LOCAL_ONLY': QtWidgets.QApplication.translate('main', 'ONLY LOCALLY:'), 
                'SITE_LAST': QtWidgets.QApplication.translate('main', 'MORE RECENT ON THE SITE:'), 
                'LOCAL_LAST': QtWidgets.QApplication.translate('main', 'MORE RECENT LOCALLY:'), 
                }
            for what in ('IDEM', 'SITE_ONLY', 'LOCAL_ONLY', ):
                consoleText = '{0}<p><b>{1}</b> {2}'.format(
                    consoleText, 
                    titles[what], 
                    translateNumber(len(self.synchro[what])))
                self.updateConsoleText(consoleText)
                for fileName in self.synchro[what]:
                    if what == 'SITE_ONLY':
                        data = siteFiles['DATA'][fileName]
                    else:
                        data = localFiles['DATA'][fileName]
                    consoleText = '{0}<br />{1} {2}'.format(
                        consoleText, fileName, data)
                consoleText = '{0}</p><p></p>'.format(consoleText)
                self.updateConsoleText(consoleText)
            for what in ('SITE_LAST', 'LOCAL_LAST', ):
                consoleText = '{0}<p><b>{1}</b> {2}'.format(
                    consoleText, 
                    titles[what], 
                    translateNumber(len(self.synchro[what])))
                self.updateConsoleText(consoleText)
                for (fileName, siteData, localData) in self.synchro[what]:
                    consoleText = '{0}<br />{1} - {2} - {3}'.format(
                        consoleText, fileName, siteData, localData)
                consoleText = '{0}</p><p></p>'.format(consoleText)
                self.updateConsoleText(consoleText)

            self.updateConsoleText(doEndMessage(consoleText))
        finally:
            utils_functions.restoreCursor()
            return consoleText


    def synchronizeFolder(self):
        """
        synchronisation des dossiers local et distant.
        Les fichiers présents uniquement dans le dossier distant sont ignorés
        pour que l'utilisateur choisisse ce qu'il en fait.
        """
        test = 0
        for what in self.synchro:
            test += len(self.synchro[what])
        if test < 1:
            consoleText = self.compareFolder()
        else:
            self.consoleTextEdit.clear()
            consoleText = ''

        utils_functions.doWaitCursor()
        try:
            theUrlBegin = '{0}/CARDS'.format(
                self.main.configDict['CONFIG']['webUrl'])

            # partie 1.
            # téléchargement des fichiers plus récents sur le site
            # (SITE_LAST)
            consoleText = '{0}<p><b>{1}</b>'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Downloading of files that are more recent on the site:'))
            self.updateConsoleText(consoleText)
            downLoader = DownLoader(self.main)
            for (fileName, siteData, localData) in self.synchro['SITE_LAST']:
                consoleText = '{0}<br />{1}'.format(
                    consoleText, fileName)
                self.updateConsoleText(consoleText)
                fileInfo = QtCore.QFileInfo(fileName)
                filePath = fileInfo.path()
                onlyFileName = fileInfo.fileName()
                utils_filesdirs.createDirs(
                    self.main.absoluteMyCardsDir, filePath)
                localDir = '{0}{1}/'.format(
                    self.main.absoluteMyCardsDir, filePath)
                netDir = '{0}{1}/'.format(theUrlBegin, filePath)
                downLoader.launch(
                    fileName=onlyFileName, localDir=localDir, netDir=netDir)
                while downLoader.state == STATE_LAUNCHED:
                    QtWidgets.QApplication.processEvents()
            consoleText = '{0}</p><p></p>'.format(consoleText)
            self.updateConsoleText(consoleText)

            # partie 2.
            # envoi des fichiers locaux plus récents ou pas encore sur le site
            # (LOCAL_LAST et LOCAL_ONLY)
            consoleText = '{0}<p><b>{1}</b>'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Sending more recent or not yet local files to the site:'))
            self.updateConsoleText(consoleText)
            postData = (
                self.main.configDict['CONFIG']['user'], 
                self.main.configDict['CONFIG']['password'])
            upLoader = UpLoader(
                self.main, 
                localDir=self.main.absoluteMyCardsDir, 
                theUrl='{0}/_tools/upload.php'.format(theUrlBegin), 
                postData=postData)
            temp = []
            for (fileName, siteData, localData) in self.synchro['LOCAL_LAST']:
                temp.append(fileName)
            for fileName in self.synchro['LOCAL_ONLY']:
                temp.append(fileName)
            for fileName in temp:
                consoleText = '{0}<br />{1}'.format(consoleText, fileName)
                self.updateConsoleText(consoleText)
                upLoader.launch(fileName=fileName)
                while upLoader.state == STATE_LAUNCHED:
                    QtWidgets.QApplication.processEvents()
            consoleText = '{0}</p><p></p>'.format(consoleText)
            self.updateConsoleText(consoleText)

            # on vide self.synchro (plus à jour)
            self.clearSynchro()
            self.updateConsoleText(doEndMessage(consoleText))
        finally:
            utils_functions.restoreCursor()


    def onlySiteDownload(self):
        """
        téléchargement des fichiers présents seulement sur le site
        """
        test = 0
        for what in self.synchro:
            test += len(self.synchro[what])
        if test < 1:
            consoleText = self.compareFolder()
        else:
            self.consoleTextEdit.clear()
            consoleText = ''

        utils_functions.doWaitCursor()
        try:
            theUrlBegin = '{0}/CARDS'.format(
                self.main.configDict['CONFIG']['webUrl'])
            consoleText = '{0}<p><b>{1}</b>'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Downloading of files that are only on the site:'))
            self.updateConsoleText(consoleText)
            downLoader = DownLoader(self.main)
            for fileName in self.synchro['SITE_ONLY']:
                consoleText = '{0}<br />{1}'.format(consoleText, fileName)
                self.updateConsoleText(consoleText)
                fileInfo = QtCore.QFileInfo(fileName)
                filePath = fileInfo.path()
                onlyFileName = fileInfo.fileName()
                utils_filesdirs.createDirs(
                    self.main.absoluteMyCardsDir, filePath)
                localDir = '{0}{1}/'.format(self.main.absoluteMyCardsDir, filePath)
                netDir = '{0}{1}/'.format(theUrlBegin, filePath)
                downLoader.launch(
                    fileName=onlyFileName, localDir=localDir, netDir=netDir)
                while downLoader.state == STATE_LAUNCHED:
                    QtWidgets.QApplication.processEvents()
            consoleText = '{0}</p><p></p>'.format(consoleText)
            self.updateConsoleText(consoleText)

            # on vide self.synchro (plus à jour)
            self.clearSynchro()
            self.updateConsoleText(doEndMessage(consoleText))
        finally:
            utils_functions.restoreCursor()



    def onlySiteDelete(self):
        """
        suppression des fichiers présents seulement sur le site
        """
        test = 0
        for what in self.synchro:
            test += len(self.synchro[what])
        if test < 1:
            consoleText = self.compareFolder()
        else:
            self.consoleTextEdit.clear()
            consoleText = ''

        utils_functions.doWaitCursor()
        try:
            consoleText = '{0}<p><b>{1}</b>'.format(
                consoleText, 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Deleting of files that are only on the site:'))
            self.updateConsoleText(consoleText)
            postData = ''
            for fileName in self.synchro['SITE_ONLY']:
                consoleText = '{0}<br />{1}'.format(consoleText, fileName)
                self.updateConsoleText(consoleText)
                postData = '{0}#####{1}'.format(postData, fileName[1:])
            consoleText = '{0}</p><p></p>'.format(consoleText)
            self.updateConsoleText(consoleText)
            postData = 'user={0}&password={1}&what={2}'.format(
                self.main.configDict['CONFIG']['user'], 
                self.main.configDict['CONFIG']['password'], 
                postData)
            theUrl = '{0}/CARDS/_tools/remove_files.php'.format(
                self.main.configDict['CONFIG']['webUrl'])
            httpClient = HttpClient(self.main)
            httpClient.launch(theUrl, postData)
            while httpClient.state == STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if httpClient.state == STATE_OK:
                try:
                    response = httpClient.reponse
                except:
                    print('except in onlySiteDelete')

            # on vide self.synchro (plus à jour)
            self.clearSynchro()
            self.updateConsoleText(doEndMessage(consoleText))
        finally:
            utils_functions.restoreCursor()









































































###########################################################"
#   VARIABLES GLOBALES
###########################################################"

STATE_WAITING = 0
STATE_LAUNCHED = 1
STATE_FINISHED = 2
STATE_OK = 3
STATE_ERROR = -1
STATE_NONET = -2
STATE_CANCELED = -3



class HttpClient(QtCore.QObject):
    """
    Une classe pour dialoguer avec http
    (fichiers html ou php)
    """
    def __init__(self, parent=None):
        super(HttpClient, self).__init__(parent)
        self.main = parent

        self.manager = QtNetwork.QNetworkAccessManager(self.main) 
        self.reply = None
        self.reponse = ''
        self.theUrl = ''
        self.state = STATE_WAITING
        self.timeDepart = QtCore.QTime.currentTime()

    def launch(self, theUrl='', postData=''):
        utils_functions.doWaitCursor()
        self.reply = None
        self.reponse = ''
        self.theUrl = theUrl
        request = QtNetwork.QNetworkRequest(QtCore.QUrl(theUrl))
        try:
            host = theUrl.split('://')[1]
        except:
            host = ''
        host = host.split('/')[0]
        request.setRawHeader(
            QtCore.QByteArray().append('Host'),
            QtCore.QByteArray().append(host))
        request.setRawHeader(
            QtCore.QByteArray().append('User-Agent'), 
            QtCore.QByteArray().append('MyOwnBrowser 1.0'))
        contentType = 'application/x-www-form-urlencoded'
        request.setHeader(QtNetwork.QNetworkRequest.ContentTypeHeader, contentType)

        if len(postData) > 0:
            self.reply = self.manager.post(
                request, postData.encode('utf8'))
        else:
            self.reply = self.manager.get(request)
        self.reply.finished.connect(self.finished)
        self.reply.error.connect(self.error)

        self.timeDepart = QtCore.QTime.currentTime()
        self.state = STATE_LAUNCHED

    def finished(self):
        utils_functions.restoreCursor()
        # reply.readAll renvoie un QByteArray :
        self.reponse = str(self.reply.readAll())
        if self.state == STATE_LAUNCHED:
            self.state = STATE_OK
        if self.state != STATE_OK:
            self.state = STATE_NONET
            message = QtWidgets.QApplication.translate(
                'main', 'INTERNET CONNECTION PROBLEM:')
            message = '{0} {1}'.format(message, self.theUrl)
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(self.main, message)

    def error(self, code):
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(
            self.main, 'error in HttpClient: {0}'.format(code))
        self.state = STATE_NONET

    def cancel(self):
        if self.state != STATE_LAUNCHED:
            return
        self.reply.abort()
        self.state = STATE_CANCELED



class DownLoader(QtCore.QObject):
    """
    Une classe pour gérer les téléchargements en http.
    La variable self.state indique l'état du DownLoader.
    Les variables fileName, localDir et netDir peuvent être définis
    à la création ou à l'appel de launch.
    """
    def __init__(self, parent=None, fileName='', localDir='', netDir='', showProgress=True):
        super(DownLoader, self).__init__(parent)
        self.main = parent

        self.fileName = fileName
        self.localDir = localDir
        self.netDir = netDir
        self.theLocalFile = '{0}{1}'.format(localDir, fileName)
        self.theFile = None

        self.manager = QtNetwork.QNetworkAccessManager(self) 
        self.reply = None
        self.reponse = ''

        self.showProgress = showProgress
        self.progressDialog = None
        self.state = STATE_WAITING

    def launch(self, fileName='', localDir='', netDir=''):
        if len(fileName) > 0:
            self.fileName = fileName
        if len(localDir) > 0:
            self.localDir = localDir
        if len(netDir) > 0:
            self.netDir = netDir
        self.theLocalFile = '{0}{1}'.format(
            self.localDir, self.fileName)
        theUrl = QtCore.QUrl(
                '{0}{1}'.format(self.netDir, self.fileName))

        if self.showProgress:
            self.progressDialog = QtWidgets.QProgressDialog(self.main)
            self.progressDialog.setWindowTitle(utils.PROGNAME)
            m1 = QtWidgets.QApplication.translate('main', 'Downloading {0}.')
            text = m1.format(fileName)
            self.progressDialog.setLabelText(text)
            self.progressDialog.canceled.connect(self.cancel)

        utils_functions.afficheStatusBar(self.main)
        m1 = QtWidgets.QApplication.translate('main', 'download:')
        m1 = '{0} {1}'.format(m1, self.theLocalFile)
        utils_functions.afficheStatusBar(self.main, m1)
        QtCore.QFile(self.theLocalFile).remove()
        self.theFile = QtCore.QFile(self.theLocalFile, self)
        self.theFile.open(QtCore.QIODevice.WriteOnly)
        request = QtNetwork.QNetworkRequest(theUrl)
        request.setRawHeader(
            QtCore.QByteArray().append('User-Agent'), 
            QtCore.QByteArray().append('MyOwnBrowser 1.0'))
        self.reply = self.manager.get(request)
        self.reply.finished.connect(self.finished)
        self.reply.downloadProgress.connect(self.downloadProgress)
        self.state = STATE_LAUNCHED

    def finished(self):
        self.state = STATE_FINISHED
        if self.showProgress:
            self.progressDialog.close()
        self.reponse = self.reply.readAll()
        if not(QtCore.QByteArray().append('Error 404') in self.reponse):
            self.theFile.write(self.reponse)
            self.theFile.close()
        else:
            self.theFile.close()
            QtCore.QFile(self.theLocalFile).remove()
        self.theFile = None
        m1 = QtWidgets.QApplication.translate('main', 'download finished')
        utils_functions.afficheStatusBar(self.main, m1)
        if self.reply.error() == QtNetwork.QNetworkReply.NoError:
            self.state = STATE_OK
        else:
            print('NETWORK ERROR : ', self.reply.error())
            self.state = STATE_ERROR

    def downloadProgress(self, bytesReceived, bytesTotal):
        if self.showProgress:
            self.progressDialog.setMaximum(bytesTotal)
            self.progressDialog.setValue(bytesReceived)

    def cancel(self):
        if self.state != STATE_LAUNCHED:
            return
        self.reply.abort()
        m1 = QtWidgets.QApplication.translate('main', 'download canceled')
        utils_functions.afficheStatusBar(self.main, m1)
        self.theFile.close()
        self.theFile = None
        QtCore.QFile(self.theLocalFile).remove()
        self.state = STATE_CANCELED



class UpLoader(QtCore.QObject):
    """
    Une classe pour gérer les envois en http
    La variable self.state indique l'état du UpLoader.
    Les variables fileName, localDir et theUrl peuvent être définis
    à la création ou à l'appel de launch.
    """
    def __init__(self, parent=None, fileName='', localDir='', theUrl='', postData=('', ''), showProgress=True):
        super(UpLoader, self).__init__(parent)
        self.main = parent

        self.fileName = fileName
        self.localDir = localDir
        self.theUrl = theUrl
        self.postData = postData

        self.manager = QtNetwork.QNetworkAccessManager(self) 
        self.reply = None
        self.reponse = ''

        self.showProgress = showProgress
        self.progressDialog = None
        self.state = STATE_WAITING

    def launch(self, fileName='', localDir='', theUrl='', postData=None):
        if len(fileName) > 0:
            self.fileName = fileName
        if len(localDir) > 0:
            self.localDir = localDir
        if len(theUrl) > 0:
            self.theUrl = theUrl
        if postData != None:
            self.postData = postData
        theLocalFile = '{0}{1}'.format(self.localDir, self.fileName)

        if self.showProgress:
            self.progressDialog = QtWidgets.QProgressDialog(self.main)
            self.progressDialog.setWindowTitle(utils.PROGNAME)
            m1 = QtWidgets.QApplication.translate('main', 'Uploading {0}.')
            text = m1.format(fileName)
            self.progressDialog.setLabelText(text)
            self.progressDialog.canceled.connect(self.cancel)

        utils_functions.afficheStatusBar(self.main)
        m1 = QtWidgets.QApplication.translate('main', 'upload:')
        m1 = '{0} {1}'.format(m1, theLocalFile)
        utils_functions.afficheStatusBar(self.main, m1)
        request = QtNetwork.QNetworkRequest(QtCore.QUrl(self.theUrl))

        multiPart = QtNetwork.QHttpMultiPart(QtNetwork.QHttpMultiPart.FormDataType)

        userPart = QtNetwork.QHttpPart()
        userPart.setHeader(
            QtNetwork.QNetworkRequest.ContentDispositionHeader, 
            'form-data; name="user"')
        userPart.setBody(self.postData[0].encode('utf8'))
        multiPart.append(userPart)
        passwordPart = QtNetwork.QHttpPart()
        passwordPart.setHeader(
            QtNetwork.QNetworkRequest.ContentDispositionHeader, 
            'form-data; name="password"')
        passwordPart.setBody(self.postData[1].encode('utf8'))
        multiPart.append(passwordPart)

        filepart = QtNetwork.QHttpPart()
        filepart.setHeader(
            QtNetwork.QNetworkRequest.ContentTypeHeader, 
            'application/octet-stream')
        modifiedFileName = self.fileName[1:].replace('/', '#####')
        filepart.setHeader(
            QtNetwork.QNetworkRequest.ContentDispositionHeader, 
            'form-data; name="{0}"; filename="{1}"'.format(
                utils.PROGNAME, modifiedFileName))
        theFile = QtCore.QFile(theLocalFile)
        theFile.open(QtCore.QIODevice.ReadOnly)
        filepart.setBodyDevice(theFile)
        theFile.setParent(multiPart)
        multiPart.append(filepart)

        self.reply = self.manager.post(request, multiPart)
        multiPart.setParent(self.reply)

        self.reply.finished.connect(self.finished)
        self.reply.uploadProgress.connect(self.uploadProgress)
        self.state = STATE_LAUNCHED

    def finished(self):
        self.state = STATE_FINISHED
        if self.showProgress:
            self.progressDialog.close()
        self.reponse = self.reply.readAll()
        m1 = QtWidgets.QApplication.translate('main', 'upload finished')
        utils_functions.afficheStatusBar(self.main, m1)
        if self.reply.error() == QtNetwork.QNetworkReply.NoError:
            self.state = STATE_OK
        else:
            print('NETWORK ERROR : ', self.reply.error())
            self.state = STATE_ERROR

    def uploadProgress(self, bytesSent, bytesTotal):
        if self.showProgress:
            self.progressDialog.setMaximum(bytesTotal)
            self.progressDialog.setValue(bytesSent)

    def cancel(self):
        if self.state != STATE_LAUNCHED:
            return
        self.reply.abort()
        m1 = QtWidgets.QApplication.translate('main', 'upload canceled')
        utils_functions.afficheStatusBar(self.main, m1)
        self.state = STATE_CANCELED


