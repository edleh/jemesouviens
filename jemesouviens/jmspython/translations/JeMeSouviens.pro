#SOURCES      += ../JeMeSouviens.pyw

SOURCES      += ../libs/main.py
SOURCES      += ../libs/utils_about.py
SOURCES      += ../libs/utils_droparea.py
#SOURCES      += ../libs/utils_filesdirs.py
SOURCES      += ../libs/utils_functions.py
SOURCES      += ../libs/utils_markdown.py
SOURCES      += ../libs/utils_pdf.py
#SOURCES      += ../libs/utils_softs.py
SOURCES      += ../libs/utils_web.py
#SOURCES      += ../libs/utils_webengine.py
#SOURCES      += ../libs/utils.py

TRANSLATIONS += ../translations/JeMeSouviens.ts
TRANSLATIONS += ../translations/JeMeSouviens_fr.ts
