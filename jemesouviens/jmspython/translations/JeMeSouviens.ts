<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/main.py" line="676"/>
        <source>Base Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="735"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="864"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="86"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="841"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="89"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="94"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="107"/>
        <source>About {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="45"/>
        <source>END !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="358"/>
        <source>Open a PDF File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="358"/>
        <source>PDF Files (*.pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="854"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="761"/>
        <source>To create cards from a PDF file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="264"/>
        <source>Files Extension:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="272"/>
        <source>Images size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="281"/>
        <source>subdirectory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="283"/>
        <source>Create a subdirectory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="287"/>
        <source>Subdirectory name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="799"/>
        <source>Create a launcher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="799"/>
        <source>To create a launcher (*.desktop file) in the folder of your choice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="67"/>
        <source>(version {0})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="819"/>
        <source>Project page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="742"/>
        <source>Open CARDS Dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="752"/>
        <source>Test the game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="752"/>
        <source>To test the game directly in the interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="768"/>
        <source>Markdown editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="768"/>
        <source>To create cards in Markdown syntax.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="783"/>
        <source>Download or upload cards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="783"/>
        <source>To download cards of a site or send your own.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="235"/>
        <source>PDF file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="238"/>
        <source>Use a drag-drop or the button to select a PDF file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="248"/>
        <source>Select a PDF File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="260"/>
        <source>Cards creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="293"/>
        <source>Create cards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_droparea.py" line="44"/>
        <source>&lt;drop here&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_droparea.py" line="45"/>
        <source>This file is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="742"/>
        <source>Open the directory containing cards.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="265"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="569"/>
        <source>Markdown Files (*.md)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="565"/>
        <source>No name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="569"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="260"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="260"/>
        <source>To create a new document.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="265"/>
        <source>To open an existing document.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="270"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="270"/>
        <source>To save the document.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="275"/>
        <source>To save as new document.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="280"/>
        <source>Create PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="280"/>
        <source>To create a PDF file from the document.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="152"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="156"/>
        <source>url:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="184"/>
        <source>remote subfolder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="162"/>
        <source>user:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="168"/>
        <source>password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="190"/>
        <source>Test configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="230"/>
        <source>File management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1207"/>
        <source>INTERNET CONNECTION PROBLEM:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1270"/>
        <source>Downloading {0}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1368"/>
        <source>Uploading {0}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1276"/>
        <source>download:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1374"/>
        <source>upload:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1303"/>
        <source>download finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1422"/>
        <source>upload finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1320"/>
        <source>download canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1439"/>
        <source>upload canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="406"/>
        <source>Choose the directory where the desktop file will be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="176"/>
        <source>Show/Hide password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="819"/>
        <source>Opens the project page in your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="806"/>
        <source>Create a shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="806"/>
        <source>To create a shortcut (*.lnk file) on your Desktop.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="440"/>
        <source>Shortcut to JeMeSouviens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="429"/>
        <source>You must install pywin32 to do this action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="84"/>
        <source>MyCards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_droparea.py" line="46"/>
        <source>The file must be in the &lt;b&gt;MyCards&lt;/b&gt; folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="142"/>
        <source>information message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="143"/>
        <source>question message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="144"/>
        <source>warning message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="145"/>
        <source>critical message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="387"/>
        <source>The file has been modified.
Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="423"/>
        <source>Open a Markdown File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="194"/>
        <source>Change your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="234"/>
        <source>Download cards of the site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="247"/>
        <source>Synchronization of the selected sub-folder; files present only on the site will be ignored.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="505"/>
        <source>Retrieve the list of site cards:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="565"/>
        <source>Retrieve the list of local cards:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="611"/>
        <source>Removal of obsolete cards:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="639"/>
        <source>Download new cards:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="56"/>
        <source>no card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="58"/>
        <source>card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="61"/>
        <source>cards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="65"/>
        <source>no file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="67"/>
        <source>file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="70"/>
        <source>files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="740"/>
        <source>Retrieve the list of site files:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="776"/>
        <source>Retrieve the list of local files:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="820"/>
        <source>IDENTICAL FILES:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="821"/>
        <source>ONLY ON THE SITE:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="822"/>
        <source>ONLY LOCALLY:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="823"/>
        <source>MORE RECENT ON THE SITE:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="824"/>
        <source>MORE RECENT LOCALLY:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="886"/>
        <source>Downloading of files that are more recent on the site:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="916"/>
        <source>Sending more recent or not yet local files to the site:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="241"/>
        <source>Compare local and remote versions of the selected subfolder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="238"/>
        <source>Management of my folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="241"/>
        <source>Compare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="247"/>
        <source>Synchronize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="254"/>
        <source>Download files from the site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="254"/>
        <source>Download the files present only on the site.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="261"/>
        <source>Delete files from the site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="261"/>
        <source>Delete files present only on the site.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="971"/>
        <source>Downloading of files that are only on the site:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1022"/>
        <source>Deleting of files that are only on the site:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="90"/>
        <source>New password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="95"/>
        <source>Confirm:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="103"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="110"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="129"/>
        <source>The password is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="135"/>
        <source>The two entries are different.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="482"/>
        <source>The password was changed successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="485"/>
        <source>The password change failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="790"/>
        <source>User management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="790"/>
        <source>You must be a site administrator to use this action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="775"/>
        <source>Print cards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="775"/>
        <source>To create printable versions of the cards.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="210"/>
        <source>Choose the folder containing the cards to be printed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
