<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr" sourcelanguage="">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/main.py" line="676"/>
        <source>Base Bar</source>
        <translation>Barre principale</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="735"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="864"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="86"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="841"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="89"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="94"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="107"/>
        <source>About {0}</source>
        <translation>À propos de {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="45"/>
        <source>END !</source>
        <translation>TERMINÉ !</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="358"/>
        <source>Open a PDF File</source>
        <translation>Ouvrir un fichier PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="358"/>
        <source>PDF Files (*.pdf)</source>
        <translation>Fichiers PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="854"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="761"/>
        <source>To create cards from a PDF file.</source>
        <translation>Pour créer des cartes à partir d&apos;un fichier PDF.</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="264"/>
        <source>Files Extension:</source>
        <translation>Extension des fichiers :</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="272"/>
        <source>Images size:</source>
        <translation>Dimension des images :</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="281"/>
        <source>subdirectory</source>
        <translation>sous-dossier</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="283"/>
        <source>Create a subdirectory</source>
        <translation>Créer un sous-dossier</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="287"/>
        <source>Subdirectory name:</source>
        <translation>Nom du sous-dossier :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="406"/>
        <source>Choose the directory where the desktop file will be created</source>
        <translation>Choisissez le dossier où le lanceur sera créé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="799"/>
        <source>Create a launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="799"/>
        <source>To create a launcher (*.desktop file) in the folder of your choice.</source>
        <translation>Pour créer un lanceur (fichier *.desktop) dans le dossier de votre choix.</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="67"/>
        <source>(version {0})</source>
        <translation>(version {0})</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="819"/>
        <source>Project page</source>
        <translation>Page du projet</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="742"/>
        <source>Open CARDS Dir</source>
        <translation>Ouvrir le dossier CARDS</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="752"/>
        <source>Test the game</source>
        <translation>Tester le jeu</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="752"/>
        <source>To test the game directly in the interface.</source>
        <translation>Pour tester le jeu directement dans l&apos;interface.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="768"/>
        <source>Markdown editor</source>
        <translation>Éditeur Markdown</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="768"/>
        <source>To create cards in Markdown syntax.</source>
        <translation>Pour créer des cartes en utilisant la syntaxe Markdown.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="783"/>
        <source>Download or upload cards</source>
        <translation>Télécharger ou poster les cartes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="783"/>
        <source>To download cards of a site or send your own.</source>
        <translation>Pour télécharger les cartes d&apos;un site ou y envoyer les vôtres.</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="235"/>
        <source>PDF file</source>
        <translation>Fichier PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="238"/>
        <source>Use a drag-drop or the button to select a PDF file:</source>
        <translation>Utilisez un glisser-déposer ou le bouton pour choisir un fichier PDF :</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="248"/>
        <source>Select a PDF File</source>
        <translation>Choisir un fichier PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="260"/>
        <source>Cards creation</source>
        <translation>Création des cartes</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="293"/>
        <source>Create cards</source>
        <translation>Créer les cartes</translation>
    </message>
    <message>
        <location filename="../libs/utils_droparea.py" line="44"/>
        <source>&lt;drop here&gt;</source>
        <translation>&lt;déposer ici&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_droparea.py" line="45"/>
        <source>This file is not valid.</source>
        <translation>Ce fichier n&apos;est pas valable.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="742"/>
        <source>Open the directory containing cards.</source>
        <translation>Ouvre le dossier contenant les cartes.</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="265"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="569"/>
        <source>Markdown Files (*.md)</source>
        <translation>Fichiers Markdown (*.md)</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="565"/>
        <source>No name</source>
        <translation>Sans nom</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="569"/>
        <source>Save as...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="260"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="260"/>
        <source>To create a new document.</source>
        <translation>Pour créer un nouveau document.</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="265"/>
        <source>To open an existing document.</source>
        <translation>Pour ouvrir un document existant.</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="270"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="270"/>
        <source>To save the document.</source>
        <translation>Pour enregistrer le document.</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="275"/>
        <source>To save as new document.</source>
        <translation>Pour enregistrer comme nouveau document.</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="280"/>
        <source>Create PDF</source>
        <translation>Créer un PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="280"/>
        <source>To create a PDF file from the document.</source>
        <translation>Pour créer un fichier PDF à partir du document.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="152"/>
        <source>Settings</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="156"/>
        <source>url:</source>
        <translation>adresse web :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="184"/>
        <source>remote subfolder:</source>
        <translation>sous-dossier distant :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="162"/>
        <source>user:</source>
        <translation>utilisateur :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="168"/>
        <source>password:</source>
        <translation>mot de passe :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="190"/>
        <source>Test configuration</source>
        <translation>Tester la configuration</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="230"/>
        <source>File management</source>
        <translation>Gestion des fichiers</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1207"/>
        <source>INTERNET CONNECTION PROBLEM:</source>
        <translation>PROBLÈME DE CONNEXION INTERNET :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1270"/>
        <source>Downloading {0}.</source>
        <translation>Téléchargement de {0}.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1368"/>
        <source>Uploading {0}.</source>
        <translation>Envoi de {0}.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1276"/>
        <source>download:</source>
        <translation>téléchargement :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1374"/>
        <source>upload:</source>
        <translation>envoi :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1303"/>
        <source>download finished</source>
        <translation>téléchargement terminé</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1422"/>
        <source>upload finished</source>
        <translation>envoi terminé</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1320"/>
        <source>download canceled</source>
        <translation>téléchargement annulé</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1439"/>
        <source>upload canceled</source>
        <translation>envoi annulé</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="176"/>
        <source>Show/Hide password</source>
        <translation>Afficher/Masquer le mot de passe</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="819"/>
        <source>Opens the project page in your browser.</source>
        <translation>Ouvre la page du projet dans votre navigateur.</translation>
    </message>
    <message>
        <location filename="../libs/utils_droparea.py" line="46"/>
        <source>The file must be in the &lt;b&gt;MyCards&lt;/b&gt; folder.</source>
        <translation>Le fichier doit être dans le dossier &lt;b&gt;MesCartes&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="806"/>
        <source>Create a shortcut</source>
        <translation>Créer un raccourci</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="806"/>
        <source>To create a shortcut (*.lnk file) on your Desktop.</source>
        <translation>Pour créer un raccourci (fichier *.lnk) sur votre bureau.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="440"/>
        <source>Shortcut to JeMeSouviens</source>
        <translation>Raccourci vers JeMeSouviens</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="429"/>
        <source>You must install pywin32 to do this action.</source>
        <translation>Vous devez installer pywin32 pour faire cette action.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="84"/>
        <source>MyCards</source>
        <translation>MesCartes</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="142"/>
        <source>information message</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="143"/>
        <source>question message</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="144"/>
        <source>warning message</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="145"/>
        <source>critical message</source>
        <translation>Message critique</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="387"/>
        <source>The file has been modified.
Do you want to save your changes?</source>
        <translation>Le fichier a été modifié.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_markdown.py" line="423"/>
        <source>Open a Markdown File</source>
        <translation>Ouvrir un fichier Markdown</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="194"/>
        <source>Change your password</source>
        <translation>Changer de mot de passe</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="234"/>
        <source>Download cards of the site</source>
        <translation>Télécharger les cartes du site</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="247"/>
        <source>Synchronization of the selected sub-folder; files present only on the site will be ignored.</source>
        <translation>Synchronisation du sous-dossier sélectionné ; les fichiers présents seulement sur le site seront ignorés.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="505"/>
        <source>Retrieve the list of site cards:</source>
        <translation>Récupération de la liste des cartes du site :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="565"/>
        <source>Retrieve the list of local cards:</source>
        <translation>Récupération de la liste des cartes locales :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="611"/>
        <source>Removal of obsolete cards:</source>
        <translation>Suppression des cartes obsolètes :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="639"/>
        <source>Download new cards:</source>
        <translation>Téléchargement des nouvelles cartes :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="56"/>
        <source>no card</source>
        <translation>aucune carte</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="58"/>
        <source>card</source>
        <translation>carte</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="61"/>
        <source>cards</source>
        <translation>cartes</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="65"/>
        <source>no file</source>
        <translation>aucun fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="67"/>
        <source>file</source>
        <translation>fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="70"/>
        <source>files</source>
        <translation>fichiers</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="740"/>
        <source>Retrieve the list of site files:</source>
        <translation>Récupération de la liste des fichiers du site :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="776"/>
        <source>Retrieve the list of local files:</source>
        <translation>Récupération de la liste des fichiers locaux :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="820"/>
        <source>IDENTICAL FILES:</source>
        <translation>FICHIERS IDENTIQUES :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="821"/>
        <source>ONLY ON THE SITE:</source>
        <translation>SEULEMENT SUR LE SITE :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="822"/>
        <source>ONLY LOCALLY:</source>
        <translation>SEULEMENT EN LOCAL :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="823"/>
        <source>MORE RECENT ON THE SITE:</source>
        <translation>PLUS RÉCENTS SUR LE SITE :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="824"/>
        <source>MORE RECENT LOCALLY:</source>
        <translation>PLUS RÉCENTS EN LOCAL :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="886"/>
        <source>Downloading of files that are more recent on the site:</source>
        <translation>Téléchargement des fichiers qui sont plus récents sur le site :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="916"/>
        <source>Sending more recent or not yet local files to the site:</source>
        <translation>Envoi des fichiers locaux plus récents ou pas encore sur le site :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="241"/>
        <source>Compare local and remote versions of the selected subfolder.</source>
        <translation>Comparer les versions locale et distante du sous-dossier sélectionné.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="238"/>
        <source>Management of my folder:</source>
        <translation>Gestion de mon dossier :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="241"/>
        <source>Compare</source>
        <translation>Comparer</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="247"/>
        <source>Synchronize</source>
        <translation>Synchroniser</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="254"/>
        <source>Download files from the site</source>
        <translation>Télécharger les fichiers du site</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="254"/>
        <source>Download the files present only on the site.</source>
        <translation>Télécharger les fichiers présents seulement sur le site.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="261"/>
        <source>Delete files from the site</source>
        <translation>Supprimer les fichiers du site</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="261"/>
        <source>Delete files present only on the site.</source>
        <translation>Supprimer les fichiers présents seulement sur le site.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="971"/>
        <source>Downloading of files that are only on the site:</source>
        <translation>Téléchargement des fichiers présents seulement sur le site :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="1022"/>
        <source>Deleting of files that are only on the site:</source>
        <translation>Suppression des fichiers présents seulement sur le site :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="90"/>
        <source>New password:</source>
        <translation>Nouveau mot de passe :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="95"/>
        <source>Confirm:</source>
        <translation>Confirmer :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="103"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="110"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="129"/>
        <source>The password is empty.</source>
        <translation>Le mot de passe est vide.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="135"/>
        <source>The two entries are different.</source>
        <translation>Les deux saisies sont différentes.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="482"/>
        <source>The password was changed successfully.</source>
        <translation>Le mot de passe a été changé avec succès.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="485"/>
        <source>The password change failed.</source>
        <translation>Le changement de mot de passe a échoué.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="790"/>
        <source>User management</source>
        <translation>Gestion des utilisateurs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="790"/>
        <source>You must be a site administrator to use this action.</source>
        <translation>Vous devez être administrateur du site pour utiliser cette action.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="775"/>
        <source>Print cards</source>
        <translation>Imprimer des cartes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="775"/>
        <source>To create printable versions of the cards.</source>
        <translation>Pour créer des versions imprimables des cartes.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="210"/>
        <source>Choose the folder containing the cards to be printed</source>
        <translation>Choisissez le dossier contenant les cartes à imprimer</translation>
    </message>
</context>
</TS>
