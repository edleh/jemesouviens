# JeMeSouviens

#### flashcards in classroom

* **Website:** http://pascal.peter.free.fr/jemesouviens.html
* **Email:** pascal.peter at free.fr
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2008-2020

----

### The tools used to develop JeMeSouviens

#### Basic Tools
* [Python](https://www.python.org): programming language
* [Qt](https://www.qt.io): "toolkit" comprehensive (graphical user interface and a lot of things)
* [PyQt](https://riverbankcomputing.com): link between Python and Qt

#### For the web interface
* [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript): programming language
* [Bootstrap](http://getbootstrap.com): framework CSS/JS
* [PHP](http://php.net): programming language

#### Other libraries used and other stuff
* [marked](https://github.com/chjj/marked): to view Markdown files
* [PDF.js](https://mozilla.github.io/pdf.js/): to view PDF files
* [pdftoppm from Poppler project](https://poppler.freedesktop.org): to convert PDF files to images
* [TaffyDB](http://taffydb.com) : database for JavaScript
* [OpenDyslexic](https://opendyslexic.org) : font




#### Miscellaneous
* [GNU GPL 3](http://www.gnu.org/copyleft/gpl.html): GNU General Public License
* logo from [Openclipart](https://upload.wikimedia.org/wikipedia/commons/8/85/Ampoule-electrique.png)

