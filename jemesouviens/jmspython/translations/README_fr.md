# JeMeSouviens

#### répétition espacée en classe

* **Site Web :** http://pascal.peter.free.fr/jemesouviens.html
* **Email :** pascal.peter at free.fr
* **Licence :** GNU General Public License (version 3)
* **Copyright :** (c) 2008-2020

----

### Les outils utilisés pour développer JeMeSouviens

#### Outils de base
* [Python](https://www.python.org) : langage de programmation
* [Qt](https://www.qt.io) : "toolkit" très complet (interface graphique et tout un tas de choses)
* [PyQt](https://riverbankcomputing.com) : lien entre Python et Qt

#### Pour l'interface web
* [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript) : langage de programmation
* [Bootstrap](http://getbootstrap.com) : un framework CSS/JS
* [PHP](http://php.net) : langage de programmation

#### Autres bibliothèques utilisées et trucs divers
* [marked](https://github.com/chjj/marked) : pour afficher les fichiers Markdown
* [PDF.js](https://mozilla.github.io/pdf.js/) : pour afficher les fichiers PDF
* [pdftoppm du projet Poppler](https://poppler.freedesktop.org) : pour convertir les fichiers PDF en images
* [TaffyDB](http://taffydb.com) : base de données pour JavaScript
* [OpenDyslexic](https://opendyslexic.org) : police d'écriture





#### Divers
* [GNU GPL 3](http://www.gnu.org/copyleft/gpl.html) : licence publique générale GNU
* logo d'après [Openclipart](https://upload.wikimedia.org/wikipedia/commons/8/85/Ampoule-electrique.png)

