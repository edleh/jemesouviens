
<table style="width: 100%">
    <tr>
        <td>
            <h1>JeMeSouviens</h1>
            <h2><small>spaced repetition in classroom.</small></h2>
            <ul>
                <li><a href="#game">Test the game</a></li>
                <li><a href="#cards">Open CARDS Dir</a></li>
                <li><a href="#pdf">PDF file</a></li>
                <li><a href="#markdown">Markdown editor</a></li>
                <li><a href="#print">Print cards</a></li>
                <li><a href="#upload">Download or upload cards</a></li>
            </ul>
        </td>
        <td style="width: 50px"></td>
        <td style="width: 200px">
            <div class="text-right"><img class="img-responsive" src="logo.png"></div>
        </td>
    </tr>
</table>










----

Based on the principle of flashcards, the project **JeMeSouviens** is made of 3 parts:
* a web interface to use the game (locally or online)
* a folder containing the cards
* software to manage cards (creation and sending to a site).

This help describes the card management software.  
A more complete description of the project is available by clicking on the button at the bottom of the interface.










<a id='game'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Test the game

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_10.jpeg">
</div>










<a id='cards'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Open CARDS Dir

* this action opens the folder containing the cards in your file browser  
(depends on your computer system)
* this folder contains some files at its root (*.php etc) that should not be deleted
* do not place your cards directly in the CARDS folder but in the **MyCards** subfolder  
(see the "upload cards" section).










<a id='pdf'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## PDF file

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_30.jpeg">
</div>

* cards are always made of 2 images:
     * one to view the question; its name must necessarily end with **-q**  
     (for example my-card-q.jpeg)
     * one to display the response; its name must necessarily end with **-r**  
     (for example my-card-r.jpeg)
     * otherwise, the names and extensions of the 2 files must be identical
* the action "PDF file" allows to generate cards from a file *.pdf
     * every odd page will be transformed into question
     * each even page will be transformed in response
* after selecting a PDF file
     * you can choose the format of images (jpeg or png)
     * you can select their width
     * you can request the creation of a subfolder to place the cards  
     (otherwise they will be next to the PDF file).










<a id='markdown'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Markdown editor

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_40.jpeg">
</div>

* This editor allows to create cards using the syntax [Markdown](https://en.wikipedia.org/wiki/Markdown)
* page breaks are obtained by **----** (4 signs **-**)
* a button allows you to create a PDF file from the opened Markdown file.

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_41.jpeg">
</div>










<a id='print'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Print cards

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_45.jpeg">
</div>

* this action is available through the "Tools" menu
* after selecting a folder containing cards, a PDF file of the cards will be created
* you can simply print it on both sides to obtain cutting cards.









<a id='upload'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Download or upload cards

* the goal is to be able to manage the cards of a site by http (or https).  
By default, this is the address of the instance of JeMeSouviens 
of the proposed Leo DROUYN College, 
but you can use another one
* even without a user account, you can download the cards of the site with the button located on the top right.  
This also makes it possible to update them

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_50.jpeg">
</div>

* if you have a user account on the site, you will be able to send your cards there
    * the subfolders of the site to which you have access are available in a drop-down list
    * each of these subfolders will correspond to a subfolder of the same name located in the local folder **MyCards**
    * it is in these local subfolders that you must create and organize your cards
    * you can also place the source files (PDF etc)
* 4 actions allow you to update your files.  
So that several users can manage the same subfolder, 
files present on the site but not in your local folder are treated separately
    * **compare: ** allows you to list the status of files 
    (existing only at the site or in local, more recent on site or local).  
    No files are downloaded or sent by this action.  
    It allows you to check the status of the different files before synchronizing
    * **synchronize: **
        * more recent files on the site are downloaded
        * more recent files locally are sent
        * files that only exist locally are sent
    * **download files from the site: ** 
    all files existing on the site but not in your local subfolder will be downloaded.  
    To do if you are more than one user (to recover files from other users) 
    or if you have changed computers and want to recover your files
    * **delete files from the site: ** 
    all files existing on the site but not in your local subfolder will be deleted from the site.  
    To be done after the previous action if you are working on the selected subfolder together, 
    in order not to delete files from other users.  
    Allows you to delete files that you have deleted locally from the site




