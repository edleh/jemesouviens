
<table style="width: 100%">
    <tr>
        <td>
            <h1>JeMeSouviens</h1>
            <h2><small>répétition espacée en classe.</small></h2>
            <ul>
                <li><a href="#game">Tester le jeu</a></li>
                <li><a href="#cards">Ouvrir le dossier CARDS</a></li>
                <li><a href="#pdf">Fichier PDF</a></li>
                <li><a href="#markdown">Éditeur Markdown</a></li>
                <li><a href="#print">Imprimer des cartes</a></li>
                <li><a href="#upload">Télécharger ou poster les cartes</a></li>
            </ul>
        </td>
        <td style="width: 50px"></td>
        <td style="width: 200px">
            <div class="text-right"><img class="img-responsive" src="logo.png"></div>
        </td>
    </tr>
</table>










----

Basé sur le principe des flashcards, le projet **JeMeSouviens** est fait de 3 parties :
* une interface web pour utiliser le jeu (localement ou en ligne)
* un dossier contenant les cartes
* un logiciel permettant de gérer les cartes (création et envoi sur un site).

Cette aide décrit le logiciel de gestion des cartes.  
Une descrition plus complète du projet est disponible en cliquant sur le bouton situé en bas de l'interface.










<a id='game'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Tester le jeu

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_10.jpeg">
</div>










<a id='cards'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Ouvrir le dossier CARDS

* cette action ouvre le dossier contenant les cartes dans votre navigateur de fichier  
(dépend du système de votre ordinateur)
* ce dossier contient quelques fichiers à sa racine (*.php etc) qu'il ne faut pas supprimer
* ne placez pas vos cartes directement dans le dossier CARDS mais dans le sous-dossier **MesCartes**  
(voir la partie "poster les cartes").










<a id='pdf'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Fichier PDF

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_30.jpeg">
</div>

* les cartes sont toujours faites de 2 images :
    * une pour afficher la question ; son nom doit obligatoirement se terminer par **-q**  
    (par exemple ma-carte-q.jpeg)
    * une pour afficher la réponse ; son nom doit obligatoirement se terminer par **-r**  
    (par exemple ma-carte-r.jpeg)
    * à part cela, les noms et les extensions des 2 fichiers doivent être identiques
* l'action "Fichier PDF" permet de générer des cartes à partir d'un fichier *.pdf
    * chaque page impaire sera transformée en question
    * chaque page paire sera transformée en réponse
* après avoir sélectionné un fichier PDF
    * vous pouvez choisir le format des images (jpeg ou png)
    * vous pouvez sélectionner leur largeur
    * vous pouvez demander la création d'un sous-dossier pour y placer les cartes  
    (sinon elles seront à côté du fichier PDF).










<a id='markdown'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Éditeur Markdown

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_40.jpeg">
</div>

* cet éditeur permet de créer des cartes en utilisant la syntaxe [Markdown](https://fr.wikipedia.org/wiki/Markdown)
* les sauts de pages sont obtenus par **----** (4 signes **-**)
* un bouton permet de créer un fichier PDF à partir du fichier Markdown ouvert.

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_41.jpeg">
</div>










<a id='print'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Imprimer des cartes

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_45.jpeg">
</div>

* cette action est disponible par le menu "Outils"
* après avoir sélectionné un dossier contenant des cartes, un fichier PDF de celles-ci sera créé
* il vous suffira de l'imprimer en recto-verso pour obtenir des cartes à découper.










<a id='upload'></a><div class="text-right">[<h2>⏶</h2>](#)</div>

----

## Télécharger ou poster les cartes

* le but est de pouvoir gérer les cartes d'un site par http (ou https).  
Par défaut, c'est l'adresse de l'instance de JeMeSouviens 
du collège Léo DROUYN qui est proposée, 
mais vous pouvez en utiliser une autre
* même sans compte utilisateur, vous pouvez télécharger les cartes du site avec le bouton situé en haut à droite.  
Cela permet aussi de les mettre à jour

<div class="figure">
    <img class="img-fluid" src="images/JMS-logiciel_50.jpeg">
</div>

* si vous avez un compte utilisateur sur le site, vous pourrez y envoyer vos cartes
    * les sous-dossiers du site auxquels vous avez accès sont disponibles dans une liste déroulante
    * chacun de ces sous-dossiers correspondra à un sous-dossier du même nom situé dans le dossier local **MesCartes**
    * c'est dans ces sous-dossiers locaux que vous devez créer et organiser vos cartes
    * vous pouvez aussi y placer les fichiers sources (PDF etc)
* 4 actions vous permettent de mettre à jour vos fichiers.  
Pour que plusieurs utilisateurs puissent gérer un même sous-dossier, 
les fichiers présents sur le site mais pas dans votre dossier local sont traités à part
    * **comparer :** permet de lister l'état des fichiers 
    (existant seulement sur le site ou en local, plus récent sur le site ou en local).  
    Aucun fichier n'est téléchargé ou envoyé par cette action.  
    Elle permet de vérifier l'état des différents fichiers avant de synchroniser
    * **synchroniser :**
        * les fichiers plus récents sur le site sont téléchargés
        * les fichiers plus récents en local sont envoyés
        * les fichiers n'existant qu'en local sont envoyés
    * **télécharger les fichiers du site :** 
    tous les fichiers existant sur le site mais pas dans votre sous-dossier local seront téléchargés.  
    À faire si vous êtes plusieurs utilisateurs (pour récupérer les fichiers des autres utilisateurs) 
    ou si vous avez changé d'ordinateur et voulez récupérer vos fichiers
    * **supprimer les fichiers du site :** 
    tous les fichiers existant sur le site mais pas dans votre sous-dossier local seront supprimés du site.  
    À faire après l'action précédente si vous travaillez à plusieurs sur le sous-dossier sélectionné, 
    afin de ne pas supprimer les fichiers des autres utilisateurs.  
    Permet de supprimer du site les fichiers que vous avez supprimés en local




