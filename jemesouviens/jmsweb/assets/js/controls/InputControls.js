/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    INPUT MANAGEMENT (KEYBOARD, MOUSE, TOUCHSCREEN, GAMEPAD)
    THE DICTIONARY inputState RETURNS THE STATUS OF ACTIONS
    --------------------------------------------------------------
    GESTION DES ENTRÉES (CLAVIER, SOURIS, ÉCRAN TACTILE, MANETTE)
    LE DICTIONNAIRE inputState RETOURNE L'ÉTAT DES ACTIONS
    --------------------------------------------------------------
*/





InputControls = function(domElement) {

    this.domElement = (domElement !== undefined) ? domElement: document;

    this.enabled = false;

    let controlX = controlY = viewHalfX = viewHalfY = 0;

    this.inputState = {
        SHOW_ANSWER: false, 
        RELOAD: false, 
        DO_FALSE: false, 
        DO_TRUE: false, 
        };

    this.handleResize = function() {
        if (this.domElement === document) {
            viewHalfX = window.innerWidth / 2;
            viewHalfY = window.innerHeight / 2;
            }
        else {
            viewHalfX = this.domElement.offsetWidth / 2;
            viewHalfY = this.domElement.offsetHeight / 2;
            }
        //console.log('handleResize: ' + viewHalfX + ', ' + viewHalfY);
        };





    /*
    *****************************************
    KEYBOARD
    ----------
    CLAVIER
    http://asquare.net/javascript/tests/KeyCode.html
    *****************************************
    */

    this.onKeyDown = function(event) {
        if (this.enabled === false)
            return;
        event.stopPropagation();
        switch(event.keyCode) {
            case 32: /*space*/      this.inputState.SHOW_ANSWER = true; break;
            case 37: /*←*/
            case 88: /*X*/          this.inputState.DO_FALSE = true; break;
            case 39: /*→*/
            case 86: /*V*/          this.inputState.DO_TRUE = true; break;

            case 27: /*esc*/        this.inputState.RELOAD = true; break;
            }
        updateControls();
        };

    this.onKeyUp = function(event) {
        if (this.enabled === false)
            return;
        event.stopPropagation();
        };





    /*
    *****************************************
    MOUSE
    ----------
    SOURIS
    *****************************************
    */

    this.onMouseDown = function(event) {
        if (this.enabled === false)
            return;
        if (this.domElement !== document)
            this.domElement.focus();
        event.preventDefault();
        event.stopPropagation();
        switch (event.button) {
            case 0: {
                if (this.domElement === document)
                    controlX = event.pageX - viewHalfX;
                else
                    controlX = event.pageX - this.domElement.offsetLeft - viewHalfX;
                let deltaX = 2 * controlX / viewHalfX;
                if (GAME.state === 'QUESTION') {
                    this.inputState.SHOW_ANSWER = true;
                    if (deltaX > 0.5)
                        this.domElement.style.cursor = "url('assets/images/cursor-yes.png'), pointer";
                    else if (deltaX < -0.5)
                        this.domElement.style.cursor = "url('assets/images/cursor-no.png'), pointer";
                    else
                        this.domElement.style.cursor = "auto";
                    }
                else if (GAME.state === 'RESPONSE') {
                    if (deltaX > 0.5)
                        this.inputState.DO_TRUE = true;
                    else if (deltaX < -0.5)
                        this.inputState.DO_FALSE = true;
                    }
                break;
                }
            }
        updateControls();
        };

    this.onMouseMove = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        event.stopPropagation();
        if (GAME.state !== 'RESPONSE')
            return;
        if (this.domElement === document)
            controlX = event.pageX - viewHalfX;
        else
            controlX = event.pageX - this.domElement.offsetLeft - viewHalfX;
        let deltaX = 2 * controlX / viewHalfX;
        if (deltaX > 0.5)
            this.domElement.style.cursor = "url('assets/images/cursor-yes.png'), pointer";
        else if (deltaX < -0.5)
            this.domElement.style.cursor = "url('assets/images/cursor-no.png'), pointer";
        else
            this.domElement.style.cursor = "auto";
        };

    this.onMouseUp = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        event.stopPropagation();
        };






    /*
    *****************************************
    TOUCHPAD
    ----------
    ÉCRAN TACTILE
    *****************************************
    */

    this.onTouchStart = function(event) {
        if (this.enabled === false)
            return;
        if (this.domElement !== document)
            this.domElement.focus();
        event.preventDefault();
        event.stopPropagation();
        switch (event.touches.length) {
            case 1: {
                if (this.domElement === document)
                    controlX = event.touches[0].pageX - viewHalfX;
                else
                    controlX = event.touches[0].pageX - this.domElement.offsetLeft - viewHalfX;
                let deltaX = 2 * controlX / viewHalfX;
                if (GAME.state === 'QUESTION') {
                    this.inputState.SHOW_ANSWER = true;
                    if (deltaX > 0.5)
                        this.domElement.style.cursor = "url('assets/images/cursor-yes.png'), pointer";
                    else if (deltaX < -0.5)
                        this.domElement.style.cursor = "url('assets/images/cursor-no.png'), pointer";
                    else
                        this.domElement.style.cursor = "auto";
                    }
                else if (GAME.state === 'RESPONSE') {
                    if (deltaX > 0.5)
                        this.inputState.DO_TRUE = true;
                    else if (deltaX < -0.5)
                        this.inputState.DO_FALSE = true;
                    }
                break;
                }
            }
        updateControls();
        };

    this.onTouchMove = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        event.stopPropagation();
        if (GAME.state !== 'RESPONSE')
            return;
        if (this.domElement === document)
            controlX = event.touches[0].pageX - viewHalfX;
        else
            controlX = event.touches[0].pageX - this.domElement.offsetLeft - viewHalfX;
        let deltaX = 2 * controlX / viewHalfX;
        if (deltaX > 0.5)
            this.domElement.style.cursor = "url('assets/images/cursor-yes.png'), pointer";
        else if (deltaX < -0.5)
            this.domElement.style.cursor = "url('assets/images/cursor-no.png'), pointer";
        else
            this.domElement.style.cursor = "auto";
        };

    this.onTouchEnd = function(event) {
        if (this.enabled === false)
            return;
        event.preventDefault();
        event.stopPropagation();
        };





    /*
    *****************************************
    GAMEPAD
    ----------
    MANETTE
    *****************************************
    */

    this.canGame = function() {
        return 'getGamepads' in navigator;
        }

    function findGamepad(id) {
        let gamepads = navigator.getGamepads();
        if (gamepads.length > 0) {
            if (id < gamepads.length)
                return gamepads[id];
            }
        }

    this.update = function() {
        if (this.enabled === false)
            return false;

        let gamepad = findGamepad(0);
        if (gamepad !== undefined && gamepad !== null) {
            //this.inputState.PLAYER_ADVANCE = - gamepad.axes[1];
            //this.inputState.PLAYER_TURN = - gamepad.axes[0];
            //console.log('gamepad :' + gamepad + ' --- ' + gamepad.buttons.length);
            for (let i=0; i < gamepad.buttons.length; i++) {
                if (gamepad.buttons[i].pressed) {
                    switch (i) {
                        case 0: {
                            if (GAME.state === 'QUESTION')
                                this.inputState.SHOW_ANSWER = true;
                            else if (GAME.state === 'RESPONSE')
                                this.inputState.DO_TRUE = true;
                            break;
                            }
                        case 1: {
                            if (GAME.state === 'QUESTION')
                                this.inputState.SHOW_ANSWER = true;
                            else if (GAME.state === 'RESPONSE')
                                this.inputState.DO_FALSE = true;
                            break;
                            }
                        default: {
                            if (GAME.state === 'QUESTION')
                                this.inputState.SHOW_ANSWER = true;
                            }
                        }
                    }
                }
            return true;
            }
        else
            return false;
        };





    /*
    *****************************************
    INSTALLATION
    ----------
    MISE EN PLACE
    *****************************************
    */

    let _onKeyDown = bind(this, this.onKeyDown);
    window.addEventListener('keydown', _onKeyDown, false);
    let _onKeyUp = bind(this, this.onKeyUp);
    window.addEventListener('keyup', _onKeyUp, false);

    let _onMouseDown = bind(this, this.onMouseDown);
    this.domElement.addEventListener('mousedown', _onMouseDown, false);
    let _onMouseMove = bind(this, this.onMouseMove);
    this.domElement.addEventListener('mousemove', _onMouseMove, false);
    let _onMouseUp = bind(this, this.onMouseUp);
    this.domElement.addEventListener('mouseup', _onMouseUp, false);

    let _onTouchStart = bind(this, this.onTouchStart);
    this.domElement.addEventListener('touchstart', _onTouchStart, false);
    let _onTouchMove = bind(this, this.onTouchMove);
    this.domElement.addEventListener('touchmove', _onTouchMove, false);
    let _onTouchEnd = bind(this, this.onTouchEnd);
    this.domElement.addEventListener('touchend', _onTouchEnd, false);


    function bind(scope, fn) {
        return function() {
            fn.apply(scope, arguments);
            };
        }


    this.dispose = function() {
        window.removeEventListener('keydown', _onKeyDown, false);
        window.removeEventListener('keyup', _onKeyUp, false);

        this.domElement.removeEventListener('mousedown', _onMouseDown, false);
        this.domElement.removeEventListener('mousemove', _onMouseMove, false);
        this.domElement.removeEventListener('mouseup', _onMouseUp, false);

        this.domElement.removeEventListener('touchstart', _onTouchStart, false);
        this.domElement.removeEventListener('touchmove', _onTouchMove, false);
        this.domElement.removeEventListener('touchend', _onTouchEnd, false);
        };



    this.handleResize();
    };

