/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    PLACEMENT AND MANAGEMENT OF THE GAME
    --------------------------------------------------------------
    MISE EN PLACE ET GESTION DU JEU
    --------------------------------------------------------------
*/





// GAME VARIABLES
// ----------
// VARIABLES DU JEU
let container, controls;
let TIMER = 0;
let INFOS = {};
let BUTTONS = {};





function initInterface() {
    // setting up the interface
    // ----------
    // mise en place de l'interface
    BUTTONS.answer = document.getElementById('answer');
    BUTTONS.true = document.getElementById('true');
    BUTTONS.false = document.getElementById('false');
    BUTTONS.answer.setAttribute('title', tr('Show answer'));
    BUTTONS.true.setAttribute('title', tr('The answer was correct'));
    BUTTONS.false.setAttribute('title', tr('The answer was false'));
    document.getElementById('back').setAttribute('title', tr('Go back'));
    document.getElementById('reload').setAttribute('title', tr('Restart'));

    INFOS.centerdiv = document.getElementById('infos-center');
    INFOS.leftdiv = document.getElementById('infos-left');
    INFOS.rightdiv = document.getElementById('infos-right');
    INFOS.rightdiv.setAttribute('title', tr('Click to change'));
    INFOS.info = tr('LOADING...');
    INFOS.other = '<p></p><p>' + LANGUAGES[PARAMS.lang] + '</p>';

    container = document.getElementById('container');

    controls = new InputControls(container);
    controls.enabled = true;
    window.addEventListener('resize', onWindowResize, false);

    GAME.path = PARAMS.path;
    GAME.what = PARAMS.what;
    loadGameFromJson(reinitGame);
    }




function goBack() {
    launchPage('index');
    }

function ChangeLang() {
    let i = LANGUAGES.LIST.indexOf(PARAMS.lang);
    i += 1;
    PARAMS.lang = LANGUAGES.LIST[i];
    if (LANGUAGES.LIST.indexOf(PARAMS.lang) < 0)
        PARAMS.lang = LANGUAGES.LIST[0];
    //console.log('ChangeLang: ' + PARAMS.lang);
    launchPage('game');
    }

function updateInfos() {
    // display of game infos
    // ----------
    // affichage des infos du jeu
    let message = '<br />';
    message += '<p>' + tr('Selected:') + ' ' + GAME.cards.length + '</p>';
    message += '<p>' + tr('In game:') + ' ' + GAME.inGame.length + '</p>'
    INFOS.leftdiv.innerHTML = message;
    INFOS.centerdiv.innerHTML = '<b>' + INFOS.info + '</b>';
    INFOS.rightdiv.innerHTML = INFOS.other;
    }


function reinitGame() {
    INFOS.info = tr('LOADING...');
    updateInfos();
    GAME.inGame = [];
    for (i in GAME.cards)
        GAME.inGame.push([GAME.cards[i][0], GAME.cards[i][1], GAME.cards[i][2]]);
    GAME.inGame = shuffle(GAME.inGame);
    INFOS.info = '';
    newCard();
    }





function showAnswer() {
    if (GAME.state !== 'QUESTION')
        return;
    GAME.state = 'RESPONSE';
    let image = CARDS_DIR + GAME.card[0] + '/' + GAME.card[1] + '-r' + GAME.card[2];
    //console.log(image);
    image = '<img id="image" class="img-responsive" src="' + image + '">';
    container.innerHTML = image;
    imageResize();
    BUTTONS.answer.disabled = true;
    BUTTONS.true.disabled = false;
    BUTTONS.false.disabled = false;
    }

function doTrue() {
    if (GAME.state !== 'RESPONSE')
        return;
    newCard();
    }

function doFalse() {
    if (GAME.state !== 'RESPONSE')
        return;
    GAME.inGame.push(GAME.card);
    GAME.inGame.push(GAME.card);
    GAME.inGame = shuffle(GAME.inGame);
    newCard();
    }

function newCard() {
    updateInfos();
    GAME.card = GAME.inGame.shift();
    if (GAME.card === undefined) {
        GAME.state = 'FINISHED';
        let text = '';
        text += '<br />';
        text += '<div class="row">';
        text += '<div class="col-sm-3"></div>';
        text += '<div class="col-sm-6">';
        text += '<div class="jumbotron"';
        text += '<br /><h1 style="font-size:50px">' + tr('FINISHED!') + '</h1>';
        text += '</div>';
        text += '<div class="col-sm-3"></div>';
        text += '</div>';
        text += '</div>';
        container.innerHTML = text;
        }
    else {
        GAME.state = 'QUESTION';
        let image = CARDS_DIR + GAME.card[0] + '/' + GAME.card[1] + '-q' + GAME.card[2];
        //console.log(image);
        image = '<img id="image" class="img-responsive" src="' + image + '">';
        container.innerHTML = image;
        imageResize();
        BUTTONS.answer.disabled = false;
        }
    BUTTONS.true.disabled = true;
    BUTTONS.false.disabled = true;
    container.style.cursor = "auto";
    }





function updateControls() {
    if (controls.enabled === false)
        return false;

    let interact = false;
    //console.log(controls.inputState);
    if (controls.inputState.RELOAD === true) {
        controls.inputState.RELOAD = false;
        interact = true;
        reinitGame();
        }
    else if (controls.inputState.SHOW_ANSWER === true) {
        controls.inputState.SHOW_ANSWER = false;
        interact = true;
        showAnswer();
        }
    else if (controls.inputState.DO_TRUE === true) {
        controls.inputState.DO_TRUE = false;
        interact = true;
        doTrue();
        }
    else if (controls.inputState.DO_FALSE === true) {
        controls.inputState.DO_FALSE = false;
        interact = true;
        doFalse();
        }
    return interact;
    }

function loop() {
    TIMER += 0.1;
    if (TIMER < 0.5)
        return;
    else if (TIMER > 5)
        TIMER = 0;
    else if (controls.update() === true) {
        INFOS.info = parseInt(10 * TIMER) / 10;
        INFOS.centerdiv.innerHTML = '<b>' + INFOS.info + '</b>';
        if (updateControls() === true)
            TIMER = 0;
        }
    }

function imageResize() {
    let maxWidth = window.innerWidth - 10,
        maxHeight = window.innerHeight - 100;
    document.getElementById('image').style.maxWidth = maxWidth + 'px';
    document.getElementById('image').style.maxHeight = maxHeight + 'px';
    }

function onWindowResize() {
    // the display is adapted to the dimensions of the window
    // ----------
    // on adapte l'affichage aux dimensions de la fenêtre
    controls.handleResize();
    if (GAME.state == 'QUESTION' || GAME.state == 'RESPONSE')
        imageResize();
    }

function init(event) {
    initInterface();
    if (controls.canGame()) {
        setInterval(loop, 100);
        }
    }

window.addEventListener('load', init, false);

