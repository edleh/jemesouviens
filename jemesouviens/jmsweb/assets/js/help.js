/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    HELP OF THE GAME
    --------------------------------------------------------------
    AIDE DU JEU
    --------------------------------------------------------------
*/





// GAME VARIABLES
// ----------
// VARIABLES DU JEU
let container;
let INFOS = {};
let BUTTONS = {};





function initInterface() {
    // setting up the interface
    // ----------
    // mise en place de l'interface
    document.getElementById('back').setAttribute('title', tr('Go back'));

    INFOS.rightdiv = document.getElementById('infos-right');
    INFOS.rightdiv.setAttribute('title', tr('Click to change'));
    INFOS.other = '<p></p><p>' + LANGUAGES[PARAMS.lang] + '</p>';

    container = document.getElementById('container');
    window.addEventListener('resize', onWindowResize, false);
    updateInfos();
    }





function goBack() {
    launchPage('index');
    }

function ChangeLang() {
    let i = LANGUAGES.LIST.indexOf(PARAMS.lang);
    i += 1;
    PARAMS.lang = LANGUAGES.LIST[i];
    if (LANGUAGES.LIST.indexOf(PARAMS.lang) < 0)
        PARAMS.lang = LANGUAGES.LIST[0];
    //console.log('ChangeLang: ' + PARAMS.lang);
    launchPage('help');
    }

function updateInfos() {
    // display of game help
    // ----------
    // affichage de l'aide du jeu
    INFOS.rightdiv.innerHTML = INFOS.other;
    let text = '';
    if (window.innerWidth < 600) {
        const liBegin = '<li>';
        const liMiddle = '<br />&nbsp;&nbsp;';
        const liEnd = '</li>';
        }
    else {
        const liBegin = '<li><table><tr><td style="width: 300px">';
        const liMiddle = '</td><td style="width: 50px"></td><td>';
        const liEnd = '</td></tr></table></li>';
        }


    text += '<br /><br />';
    text += '<h2 id="utilisation">' + tr('USE') + '</h2>';

    text += '<ul>';
    text += '<li>' + tr('select a folder and click the <strong>Start</strong> button.') + '<br />';
    text += tr('You can also  take cards at random') + '</li>';
    text += '</ul>';
    text += '<div class="figure">';
    text += '<img src="assets/images/JMS-game_01.jpeg" />';
    text += '</div>';

    text += '<ul>';
    text += '<li>' + tr('when a question is displayed:');
    text += '<ul>';
    text += '<li>' + tr('a click on the screen will display the answer');
    text += '<li>' + tr('you can also use the <strong>Show answer</strong> button on the toolbar') + '</li>';
    text += '</ul></li>';
    text += '</ul>';
    text += '<div class="figure">';
    text += '<img src="assets/images/JMS-game_02.jpeg" />';
    text += '</div>';

    text += '<ul>';
    text += '<li>' + tr('once the response is displayed:');
    text += '<ul>';
    text += '<li>' + tr('if the proposed answer was right, just click to the right of the screen');
    text += '<li>' + tr('you can also use the <strong>The answer was correct</strong> button') + '</li>';
    text += '</ul></li>';
    text += '</ul>';
    text += '<div class="figure">';
    text += '<img src="assets/images/JMS-game_03.jpeg" />';
    text += '</div>';

    text += '<ul>';
    text += '<li>' + tr('if not:');
    text += '<ul>';
    text += '<li>' + tr('if the proposed answer was false, just click to the left of the screen');
    text += '<li>' + tr('you can also use the <strong>The answer was false</strong> button') + '</li>';
    text += '</ul></li>';
    text += '</ul>';
    text += '<div class="figure">';
    text += '<img src="assets/images/JMS-game_04.jpeg" />';
    text += '</div>';


    text += '<br /><br />';
    text += '<h2>' + tr('OTHER CONTROLLERS') + '</h2>';
    text += '<ul>';

    text += '<li>' + tr('KEYBOARD') + '<ul>';
    text += liBegin + tr('show answer:') + liMiddle + tr('space') + liEnd;
    text += liBegin + tr('the answer was correct:') + liMiddle + 'V, →' + liEnd;
    text += liBegin + tr('the answer was false:') + liMiddle + 'X, ←' + liEnd;
    text += liBegin + tr('restart:') + liMiddle + tr('esc') + liEnd;
    text += '</ul></li>';

    text += '<li>' + tr('TOUCHSCREEN') + '<ul>';
    text += '<li>' + tr('same use as with a mouse') + '</li>';
    text += '</ul></li>';

    text += '<li>' + tr('GAMEPAD') + '<ul>';
    text += liBegin + tr('show answer:') + liMiddle + tr('any button') + liEnd;
    text += liBegin + tr('the answer was correct:') + liMiddle + tr('button 1') + liEnd;
    text += liBegin + tr('the answer was false:') + liMiddle + tr('button 2') + liEnd;
    text += '</ul></li>';

    text += '</ul>';
    text += '<br /><br />';


    container.innerHTML = text;
    }





function init(event) {
    initInterface();
    }

function onWindowResize() {
    // the display is adapted to the dimensions of the window
    // ----------
    // on adapte l'affichage aux dimensions de la fenêtre
    updateInfos();
    }

window.addEventListener('load', init, false);

