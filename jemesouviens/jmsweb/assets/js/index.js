/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    PLACEMENT AND MANAGEMENT OF THE GAME
    --------------------------------------------------------------
    MISE EN PLACE ET GESTION DU JEU
    --------------------------------------------------------------
*/





// GAME VARIABLES
// ----------
// VARIABLES DU JEU
let container;
let INFOS = {};





function initInterface() {
    // setting up the interface
    // ----------
    // mise en place de l'interface
    document.getElementById('help').setAttribute('title', tr('Help of the game'));
    document.getElementById('about').setAttribute('title', tr('Project web page'));
    document.getElementById('reset').setAttribute('title', tr('Reset to default settings'));
    document.getElementById('start').setAttribute('title', tr('Start'));
    document.getElementById('random').setAttribute('title', tr('Take cards at random'));
    document.getElementById('randomNB').setAttribute('title', tr('Number of cards taken at random'));
    document.getElementById('randomNB').value = PARAMS.randomNB;

    INFOS.rightdiv = document.getElementById('infos-right');
    INFOS.rightdiv.setAttribute('title', tr('Click to change'));
    INFOS.other = '<p></p><p>' + LANGUAGES[PARAMS.lang] + '</p>';
    updateInfos();

    container = document.getElementById('container');

    GAME.path = PARAMS.path;
    loadGameFromJson(updateSelect);
    }




function resetParams() {
    localStorage.clear();
    sessionStorage.clear();
    window.location = 'index.html';
    }

function startGame() {
    PARAMS.path = GAME.path;
    PARAMS.what = 'ALL';
    launchPage('game');
    }

function randomGame() {
    PARAMS.path = GAME.path;
    PARAMS.what = 'RANDOM';
    PARAMS.randomNB = parseInt(document.getElementById('randomNB').value);
    launchPage('game');
    }

function goHelp() {
    PARAMS.path = GAME.path;
    launchPage('help');
    }


function ChangeLang() {
    let i = LANGUAGES.LIST.indexOf(PARAMS.lang);
    i += 1;
    PARAMS.lang = LANGUAGES.LIST[i];
    if (LANGUAGES.LIST.indexOf(PARAMS.lang) < 0)
        PARAMS.lang = LANGUAGES.LIST[0];
    //console.log('ChangeLang: ' + PARAMS.lang);
    launchPage('index');
    }

function updateInfos() {
    // display of game infos
    // ----------
    // affichage des infos du jeu
    INFOS.rightdiv.innerHTML = INFOS.other;
    }


function updateSelect(SUBDIRS) {
    // display of game infos
    // ----------
    // affichage des infos du jeu

    let text = tr('Current folder: @ cards');
    text = text.replace('@', GAME.cards.length);
    document.getElementById('select-nb').innerHTML = text;



    text = '<li class="breadcrumb-item"><a path="" class="path" href="#"><span class="oi oi-home" aria-hidden="true"></span></a></li>';
    //console.log(GAME.path);
    let temp = GAME.path.split('/');
    //console.log(temp);
    let path = '';
    for (i in temp)
        if (temp[i] != '') {
            if (path == '')
                path = temp[i];
            else
                path += '/' + temp[i];
            text += '<li class="breadcrumb-item"><a path="' + path + '" class="path" href="#">' + temp[i] + '</a></li>';
            }
    document.getElementById('select-actual').innerHTML = text;

    //console.log(SUBDIRS);
    if (SUBDIRS.length < 1) {
        text = tr('No subdirectories.');
        document.getElementById('select-comment').innerHTML = text;
        document.getElementById('select-subdirs').innerHTML = '';
        }
    else if (SUBDIRS[0] == '') {
        text = tr('No subdirectories.');
        document.getElementById('select-comment').innerHTML = text;
        document.getElementById('select-subdirs').innerHTML = '';
        }
    else {
        text = tr('Available subdirectories:');
        document.getElementById('select-comment').innerHTML = text;
        text = '';
        for (i in SUBDIRS)
            text += '<button type="button" class="btn btn-light subdir">' + SUBDIRS[i] + '</button>';
        document.getElementById('select-subdirs').innerHTML = text;
        }

    [].forEach.call(document.querySelectorAll('.path'), function(el) {
        el.addEventListener('click', function() {
            GAME.path = this.getAttribute('path');
            loadGameFromJson(updateSelect);
            })
        });

    [].forEach.call(document.querySelectorAll('.subdir'), function(el) {
        el.addEventListener('click', function() {
            //alert(this.innerHTML);
            if (GAME.path == '')
                GAME.path = this.textContent;
            else
                GAME.path += '/' + this.textContent;
            loadGameFromJson(updateSelect);
            })
        });
    }





function init(event) {
    initInterface();
    }

window.addEventListener('load', init, false);

