/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


/*
    --------------------------------------------------------------
    SCRIPT LOADING SYSTEM NEEDED
    BASED ON FILE PARAMETERS
    --------------------------------------------------------------
    SYSTÈME DE CHARGEMENT DES SCRIPTS NÉCESSAIRES
    EN FONCTION DES PARAMÈTRES PASSÉS AU FICHIER
    --------------------------------------------------------------
*/





// GLOBAL VARIABLES (USED IN THE GAME)
// ----------
// VARIABLES GLOBALES (UTILISÉES DANS LE JEU)
const LANGUAGES = {
    'LIST': ['fr', 'en', ], 
    'fr': 'Français', 
    'en': 'English', 
    'de': 'Deutsch', 
    'es': 'Español', 
    'lt': 'Latina', 
    };

const CARDS_DIR = '../CARDS';
let database = undefined;

let PARAMS = {};
let SCORES = {};

let GAME = {
    'path': '', 
    'state': 'NOTHING', 
    'card': ['', '', ''], 
    'cards': [], 
    'inGame': [], 
    'what': 'ALL', 
    };



// USEFUL FUNCTIONS
// ----------
// FONCTIONS UTILES

function getJSON(url, callback) {
    /*
    Vanilla JavaScript $.getJSON version with callback
    https://codepad.co/snippet/kkEqeYaR
    */
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                let res = xhr.responseText;
                // Executes your callback with the
                // response already parsed into JSON
                callback(JSON.parse(res));
                } 
            else {
                // Server responded with some error
                console.error(xhr.statusText);
                }
            }
        };
    
    // What to do if there's an error with the request
    xhr.onerror = function (e) {
        console.error(xhr.statusText);
        };
    
    // Send the request to the server
    xhr.send(null);
    }

function loadGameFromJson(callback) {
    INFOS.info = tr('LOADING...');
    updateInfos();
    if (database == undefined) {
        console.log('TAFFY init');
        database = TAFFY([]);
        if (window.location.protocol == 'file:') {
            for (i in cards_database.CARDS)
                database.insert({
                    'path': cards_database.CARDS[i][0], 
                    'baseName': cards_database.CARDS[i][1], 
                    'ext': cards_database.CARDS[i][2]});
            loadCards(callback);
            }
        else {
            const filename = CARDS_DIR + '/_tools/find_cards.php';
            console.log(filename);
            getJSON(filename, function(data) {
                for (i in data.CARDS)
                    database.insert({
                        'path': data.CARDS[i][0], 
                        'baseName': data.CARDS[i][1], 
                        'ext': data.CARDS[i][2]});
                if (data.EXISTS === false)
                    GAME.path = '';
                loadCards(callback);
                });
            }
        }
    else
        loadCards(callback);
    }

function loadCards(callback) {
    //alert(GAME.what);
    let r = database({path: {left: '/' + GAME.path}});
    console.log('TAFFY ok : ' + r.count());
    r = r.order('path').get();
    GAME.cards = [];
    let SUBDIRS = [];
    for (i in r) {
        GAME.cards.push([
            r[i].path, 
            r[i].baseName, 
            r[i].ext]);
        let what;
        if (GAME.path == '')
            what = r[i].path.substring(GAME.path.length + 1);
        else
            what = r[i].path.substring(GAME.path.length + 2);
        //console.log(what);
        if (what.indexOf('/') > -1)
            what = what.substring(0, what.indexOf('/'));
        if (what != '')
            if (what != '_tools')
                if (SUBDIRS.indexOf(what) < 0)
                    SUBDIRS.push(what);
        }
    //console.log(GAME.cards[0]);
    //console.log(SUBDIRS);
    if (GAME.what === 'RANDOM') {
        const cardsNB = GAME.cards.length;
        if (cardsNB < PARAMS.randomNB) {
            let randomIndex;
            for (j=0; j<PARAMS.randomNB-cardsNB; j++) {
                randomIndex = Math.floor(Math.random() * cardsNB);
                GAME.cards.push(GAME.cards[randomIndex]);
                }
            }
        else if (cardsNB > PARAMS.randomNB + 1) {
            GAME.cards = shuffle(GAME.cards);
            for (j=0; j<cardsNB-PARAMS.randomNB; j++)
                GAME.cards.shift();
            }
        }
    callback(SUBDIRS);
    }

function shuffle(array) {
    // https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
    let currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
        }
    return array;
    }




function extractHtmlFileName() {
    /*
    http://befused.com/javascript/get-filename-url
    */
    const url = window.location.pathname;
    let fileName = url.substring(url.lastIndexOf('/')+1);
    fileName = fileName.split('.')[0];
    return fileName;
    }

function launchPage(who) {
    /*
    launch the page with the parameters
    ----------
    lance la page en indiquant les paramètres
    */
    let page = who + '.html';
    if (localStorage) {
        let json = JSON.stringify(PARAMS);
        localStorage.setItem('JMS', json);
        json = JSON.stringify(SCORES);
        sessionStorage.setItem('JMS', json);
        }
    else {
        page += '?lang=' +  PARAMS.lang;
        page += '&path=' +  PARAMS.path;
        page += '&what=' +  PARAMS.what;
        page = page.replace(/\s/g, '');
        }
    window.location = page;
    }





// MAIN FUNCTION
// ----------
// FONCTION PRINCIPALE
function main() {
    /*
    we retrieve the parameters, fill in the list INCLUDES
    and run the include function
    ----------
    on récupère les paramètres, on rempli la liste INCLUDES
    et on lance la fonction include
    */

    function include(urls, callback) {
        /*
        http://sametmax.com/include-require-import-en-javascript
        */

        for (i in urls) {
            let url = urls[i];
            /* on crée une balise<script type="text/javascript"></script> */
            let script = document.createElement('script');
            script.type = 'text/javascript';

            /* On fait pointer la balise sur le script qu'on veut charger
            avec en prime un timestamp pour éviter les problèmes de cache
            */
            script.src = url + '?' + (new Date().getTime());

            /* On dit d'exécuter cette fonction une fois que le dernier script est chargé */
            if (i == urls.length - 1)
                if (callback) {
                    script.onreadystatechange = callback;
                    script.onload = script.onreadystatechange;
                    }

            /* On rajoute la balise script dans le head, ce qui démarre le téléchargement */
            document.getElementsByTagName('head')[0].appendChild(script);
            }
        };

    let FILE_NAME = extractHtmlFileName();
    if (FILE_NAME == '' || FILE_NAME == undefined)
        FILE_NAME = 'index';
    let INCLUDES = [];

    if (localStorage) {
        let json = localStorage.getItem('JMS');
        if (json != null)
            PARAMS = JSON.parse(json);
        /*
         * json = sessionStorage.getItem('JMS');
        if (json != null)
            SCORES = JSON.parse(json);
        */
        }
    else {
        //retrieving page settings
        //----------
        //récupération des paramètres de la page
        //http://www.helary.net/recuperer-appel-parametres-javascript
        let t = window.location.search.substring(1).split('&');
        for (i in t) {
            let x = t[i].split('=');
            PARAMS[x[0]] = x[1];
            }
        }
    //console.log(PARAMS);
    //console.log(SCORES);

    // we start by finding the language of the interface
    // ----------
    // on commence par trouver la langue de l'interface
    if (PARAMS.lang == undefined)
        PARAMS.lang = navigator.language;
    if (LANGUAGES.LIST.indexOf(PARAMS.lang) < 0)
        PARAMS.lang = LANGUAGES.LIST[0];
    if (PARAMS.path == undefined)
        PARAMS.path = '';
    if (PARAMS.what == undefined)
        PARAMS.what = 'ALL';
    if (PARAMS.randomNB == undefined)
        PARAMS.randomNB = 10;

    INCLUDES.push('assets/js/translations/' + PARAMS.lang + '.js');
    if (FILE_NAME == 'game')
        INCLUDES.push('assets/js/controls/InputControls.js');
    if (FILE_NAME != 'help')
        if (window.location.protocol == 'file:')
            INCLUDES.push(CARDS_DIR + '/_tools/cards.js');

    if (INCLUDES.length < 1)
        include(['assets/js/' + FILE_NAME + '.js']);
    else {
        include(
            INCLUDES, 
            function() {
                include(['assets/js/' + FILE_NAME + '.js']);
                }
            )
        }
    }

main();

