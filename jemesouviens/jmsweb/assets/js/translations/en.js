/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


const TRANSLATIONS = {
    'Current folder: @ cards': '', 
    'No subdirectories.': '', 
    'Available subdirectories:': '', 

    'Help of the game': '', 
    'Project web page': '', 
    'Reset to default settings': '', 
    'Start': '', 
    'Take cards at random': '', 
    'Number of cards taken at random': '', 

    'LOADING...': '', 
    'LOADED !!!': '', 

    'Go back': '', 
    'Restart': '', 
    'Show answer': '', 
    'The answer was correct': '', 
    'The answer was false': '', 
    'FINISHED!': '', 

    'Selected:': '', 
    'In game:': '', 

    'Click to change': '', 

    'USE': '', 
    'select a folder and click the <strong>Start</strong> button.': '', 
    'You can also  take cards at random': '', 
    'when a question is displayed:': '', 
    'a click on the screen will display the answer': '', 
    'you can also use the <strong>Show answer</strong> button on the toolbar': '', 
    'once the response is displayed:': '', 
    'if not:': '', 
    'if the proposed answer was right, just click to the right of the screen': '', 
    'if the proposed answer was false, just click to the left of the screen': '', 
    'you can also use the <strong>The answer was correct</strong> button': '', 
    'you can also use the <strong>The answer was false</strong> button': '', 

    'OTHER CONTROLLERS': '', 
    'show answer:': '', 
    'the answer was correct:': '', 
    'the answer was false:': '', 
    'restart:': '', 
    'KEYBOARD': '', 
    'space': '', 
    'esc': '', 
    'TOUCHSCREEN': '', 
    'same use as with a mouse': '', 
    'GAMEPAD': '', 
    'any button': '', 
    'button 1': '', 
    'button 2': '', 

    '': '', 
    };


function tr(text) {
    let result = TRANSLATIONS[text.trim()];
    if (result === undefined)
        result = text;
    else if (result == '')
        result = text;
    return result;
    }

