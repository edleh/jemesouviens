/**
#-----------------------------------------------------------------
# This file is a part of JeMeSouviens project.
# Copyright:    (C) 2008-2020 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/


const TRANSLATIONS = {
    'Current folder: @ cards': 'Dossier actuel : @ cartes', 
    'No subdirectories.': 'Pas de sous-répertoires.', 
    'Available subdirectories:': 'Sous-répertoires disponibles :', 

    'Help of the game': 'Aide du jeu', 
    'Project web page': 'Page web du projet', 
    'Reset to default settings': 'Remettre les réglages par défaut', 
    'Start': 'Commencer', 
    'Take cards at random': 'Prendre des cartes au hasard', 
    'Number of cards taken at random': 'Nombre de cartes prises au hasard', 

    'LOADING...': 'CHARGEMENT...', 
    'LOADED !!!': 'CHARGÉ !!!', 

    'Go back': 'Retour', 
    'Restart': 'Recommencer', 
    'Show answer': 'Afficher la réponse', 
    'The answer was correct': 'La réponse était juste', 
    'The answer was false': 'La réponse était fausse', 
    'FINISHED!': 'TERMINÉ !', 

    'Selected:': 'Sélection :', 
    'In game:': 'Dans le jeu :', 

    'Click to change': 'Cliquer pour changer', 

    'USE': 'UTILISATION', 
    'select a folder and click the <strong>Start</strong> button.': 
        'on sélectionne un dossier et on clique sur le bouton <strong>Commencer</strong>.', 
    'You can also  take cards at random': 
        'On peut aussi prendre des cartes au hasard', 
    'when a question is displayed:': "lorsqu'une question est affichée :", 
    'a click on the screen will display the answer': 
        "un clic sur l'écran affichera la réponse", 
    'you can also use the <strong>Show answer</strong> button on the toolbar': 
        "on peut aussi utiliser le bouton <strong>Afficher la réponse</strong> de la barre d'outils", 
    'once the response is displayed:': 'une fois la réponse affichée :', 
    'if not:': 'sinon :', 
    'if the proposed answer was right, just click to the right of the screen': 
        "si la réponse proposée était juste, il suffit de cliquer à droite de l'écran", 
    'if the proposed answer was false, just click to the left of the screen': 
        "si la réponse proposée était fausse, il suffit de cliquer à gauche de l'écran", 
    'you can also use the <strong>The answer was correct</strong> button': 
        'on peut aussi utiliser le bouton <strong>La réponse était juste</strong>', 
    'you can also use the <strong>The answer was false</strong> button': 
        'on peut aussi utiliser le bouton <strong>La réponse était fausse</strong>', 

    'OTHER CONTROLLERS': 'AUTRES CONTRÔLEURS', 
    'show answer:': 'afficher la réponse :', 
    'the answer was correct:': 'la réponse était juste :', 
    'the answer was false:': 'la réponse était fausse :', 
    'restart:': 'recommencer :', 
    'KEYBOARD': 'CLAVIER', 
    'space': 'espace', 
    'esc': 'échap', 
    'TOUCHSCREEN': 'ÉCRAN TACTILE', 
    'same use as with a mouse': "même utilisation qu'avec une souris", 
    'GAMEPAD': 'MANETTE', 
    'any button': "n'importe quel bouton", 
    'button 1': 'bouton 1', 
    'button 2': 'bouton 2', 

    '': '', 
    };


function tr(text) {
    let result = TRANSLATIONS[text.trim()];
    if (result === undefined)
        result = text;
    else if (result == '')
        result = text;
    return result;
    }

